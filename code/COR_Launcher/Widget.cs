﻿using FluentFTP;
using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace COR_Launcher
{
    public partial class Widget : MetroForm
    {
        [DllImport("user32.dll", SetLastError = false)]
        static extern IntPtr GetDesktopWindow();

        public enum SystemMetric : int
        {
            SM_CXSCREEN = 0,  // 0x00
            SM_CYSCREEN = 1,  // 0x01
            SM_CXVSCROLL = 2,  // 0x02
            SM_CYHSCROLL = 3,  // 0x03
            SM_CYCAPTION = 4,  // 0x04
            SM_CXBORDER = 5,  // 0x05
            SM_CYBORDER = 6,  // 0x06
            SM_CXDLGFRAME = 7,  // 0x07
            SM_CXFIXEDFRAME = 7,  // 0x07
            SM_CYDLGFRAME = 8,  // 0x08
            SM_CYFIXEDFRAME = 8,  // 0x08
            SM_CYVTHUMB = 9,  // 0x09
            SM_CXHTHUMB = 10, // 0x0A
            SM_CXICON = 11, // 0x0B
            SM_CYICON = 12, // 0x0C
            SM_CXCURSOR = 13, // 0x0D
            SM_CYCURSOR = 14, // 0x0E
            SM_CYMENU = 15, // 0x0F
            SM_CXFULLSCREEN = 16, // 0x10
            SM_CYFULLSCREEN = 17, // 0x11
            SM_CYKANJIWINDOW = 18, // 0x12
            SM_MOUSEPRESENT = 19, // 0x13
            SM_CYVSCROLL = 20, // 0x14
            SM_CXHSCROLL = 21, // 0x15
            SM_DEBUG = 22, // 0x16
            SM_SWAPBUTTON = 23, // 0x17
            SM_CXMIN = 28, // 0x1C
            SM_CYMIN = 29, // 0x1D
            SM_CXSIZE = 30, // 0x1E
            SM_CYSIZE = 31, // 0x1F
            SM_CXSIZEFRAME = 32, // 0x20
            SM_CXFRAME = 32, // 0x20
            SM_CYSIZEFRAME = 33, // 0x21
            SM_CYFRAME = 33, // 0x21
            SM_CXMINTRACK = 34, // 0x22
            SM_CYMINTRACK = 35, // 0x23
            SM_CXDOUBLECLK = 36, // 0x24
            SM_CYDOUBLECLK = 37, // 0x25
            SM_CXICONSPACING = 38, // 0x26
            SM_CYICONSPACING = 39, // 0x27
            SM_MENUDROPALIGNMENT = 40, // 0x28
            SM_PENWINDOWS = 41, // 0x29
            SM_DBCSENABLED = 42, // 0x2A
            SM_CMOUSEBUTTONS = 43, // 0x2B
            SM_SECURE = 44, // 0x2C
            SM_CXEDGE = 45, // 0x2D
            SM_CYEDGE = 46, // 0x2E
            SM_CXMINSPACING = 47, // 0x2F
            SM_CYMINSPACING = 48, // 0x30
            SM_CXSMICON = 49, // 0x31
            SM_CYSMICON = 50, // 0x32
            SM_CYSMCAPTION = 51, // 0x33
            SM_CXSMSIZE = 52, // 0x34
            SM_CYSMSIZE = 53, // 0x35
            SM_CXMENUSIZE = 54, // 0x36
            SM_CYMENUSIZE = 55, // 0x37
            SM_ARRANGE = 56, // 0x38
            SM_CXMINIMIZED = 57, // 0x39
            SM_CYMINIMIZED = 58, // 0x3A
            SM_CXMAXTRACK = 59, // 0x3B
            SM_CYMAXTRACK = 60, // 0x3C
            SM_CXMAXIMIZED = 61, // 0x3D
            SM_CYMAXIMIZED = 62, // 0x3E
            SM_NETWORK = 63, // 0x3F
            SM_CLEANBOOT = 67, // 0x43
            SM_CXDRAG = 68, // 0x44
            SM_CYDRAG = 69, // 0x45
            SM_SHOWSOUNDS = 70, // 0x46
            SM_CXMENUCHECK = 71, // 0x47
            SM_CYMENUCHECK = 72, // 0x48
            SM_SLOWMACHINE = 73, // 0x49
            SM_MIDEASTENABLED = 74, // 0x4A
            SM_MOUSEWHEELPRESENT = 75, // 0x4B
            SM_XVIRTUALSCREEN = 76, // 0x4C
            SM_YVIRTUALSCREEN = 77, // 0x4D
            SM_CXVIRTUALSCREEN = 78, // 0x4E
            SM_CYVIRTUALSCREEN = 79, // 0x4F
            SM_CMONITORS = 80, // 0x50
            SM_SAMEDISPLAYFORMAT = 81, // 0x51
            SM_IMMENABLED = 82, // 0x52
            SM_CXFOCUSBORDER = 83, // 0x53
            SM_CYFOCUSBORDER = 84, // 0x54
            SM_TABLETPC = 86, // 0x56
            SM_MEDIACENTER = 87, // 0x57
            SM_STARTER = 88, // 0x58
            SM_SERVERR2 = 89, // 0x59
            SM_MOUSEHORIZONTALWHEELPRESENT = 91, // 0x5B
            SM_CXPADDEDBORDER = 92, // 0x5C
            SM_DIGITIZER = 94, // 0x5E
            SM_MAXIMUMTOUCHES = 95, // 0x5F

            SM_REMOTESESSION = 0x1000,
            SM_SHUTTINGDOWN = 0x2000,
            SM_REMOTECONTROL = 0x2001,

            SM_CONVERTIBLESLATEMODE = 0x2003,
            SM_SYSTEMDOCKED = 0x2004,
        }

        [DllImport("user32.dll")]
        static extern int GetSystemMetrics(SystemMetric smIndex);

        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Ansi)]
        static extern bool CloseHandle(IntPtr hHandle);

        private bool modalWindow = false;

        private int checkPeriod = /*30 **/ 60 * 1000 / 125;
        private int checkTimer = 0;

        public Widget()
        {
            InitializeComponent();
        }

        private void ProcessDirectory(FtpClient fc, string dir)
        {
            fc.SetWorkingDirectory(dir);

            FtpListItem[] ls = fc.GetListing();
            foreach (FtpListItem item in ls)
            {
                if (item.Type == FtpFileSystemObjectType.File)
                {
                    string windowizedFileName = item.FullName.Replace('/', '\\');
                    if (windowizedFileName[0] == '\\')
                        windowizedFileName = windowizedFileName.Substring(1);

                    FileInfo localFile = new FileInfo(SettingsHolder.BL_DB_PATH + windowizedFileName);

                    if (!localFile.Exists)
                    {
                        try
                        {
                            lSyncFile.Text = item.FullName;
                            fc.DownloadFile(SettingsHolder.BL_DB_PATH + windowizedFileName, item.FullName, true, FtpVerify.Retry);
                        }
                        catch (Exception e)
                        {
                            MessageBox.Show($"При обновлении файла {item.Name} возникли ошибки!\n\n{e.Message}", "Ошибка подключения", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        continue;
                    }

                    long remoteSize = fc.GetFileSize(item.FullName);
                    if (localFile.Length == remoteSize) continue;

                    try
                    {
                        File.Delete(SettingsHolder.BL_DB_PATH + windowizedFileName);
                    }
                    catch (Exception e)
                    {
                        // here is a chance when file is being used by another process so skip those files
                        continue;
                    }

                    try
                    {
                        lSyncFile.Text = item.FullName;
                        fc.DownloadFile(SettingsHolder.BL_DB_PATH + windowizedFileName, item.FullName, true, FtpVerify.Retry);
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show($"При обновлении файла {item.Name} возникли ошибки!\n\n{e.Message}", "Ошибка подключения", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        continue;
                    }

                    continue;
                }

                if (item.Type == FtpFileSystemObjectType.Directory)
                {
                    if (item.Name == "." || item.Name == ".." || item.Name == "/") continue;
                    string windowizedDirName = item.FullName.Replace('/', '\\');
                    if (windowizedDirName[0] == '\\')
                        windowizedDirName = windowizedDirName.Substring(1);

                    DirectoryInfo localDir = new DirectoryInfo(SettingsHolder.BL_DB_PATH + windowizedDirName);

                    if (!localDir.Exists)
                    {
                        Directory.CreateDirectory(localDir.FullName);
                    }

                    ProcessDirectory(fc, item.FullName);
                }

            }
        }

        private void Poll()
        {
            if (checkTimer > 0)
            {
                --checkTimer;
                return;
            }

            lSyncFile.Visible = true;
            metroLabel1.Text = "Синхронизация...";
            msNetworkStatus.Visible = true;

            FtpClient fc = new FtpClient("cor.com.ua", 10213, SettingsHolder.COR_USERNAME, SettingsHolder.COR_PASSWORD);

            try
            {
                fc.Connect();
            }
            catch (Exception e)
            {
                // ...
            }

            if (!fc.IsConnected)
            {
                MessageBox.Show("При подключении к серверу COR возникли ошибки!", "Ошибка подключения", MessageBoxButtons.OK, MessageBoxIcon.Error);
                checkTimer = checkPeriod;
                return;
            }

            ProcessDirectory(fc, ".");

            fc.Disconnect();

            lSyncFile.Text = "";
            lSyncFile.Visible = false;
            metroLabel1.Text = "Файлы метаданных синхронизированы!";
            msNetworkStatus.Visible = false;

            checkTimer = checkPeriod;
        }

        private void Widget_Load(object sender, EventArgs e)
        {
            IntPtr hDesktop = GetDesktopWindow();
            int screenHeight = GetSystemMetrics(SystemMetric.SM_CYFULLSCREEN);
            int screenWidth = GetSystemMetrics(SystemMetric.SM_CXFULLSCREEN);

            // our brand new cool metro ui window have problems with position, so...
            int heightFix = GetSystemMetrics(SystemMetric.SM_CYCAPTION);

            int realX = screenWidth - Width;
            int realY = screenHeight - Height + heightFix;

            SetDesktopBounds(realX, realY, Width, Height);

            Poll();
        }

        private void Widget_Deactivate(object sender, EventArgs e)
        {
            System.Threading.Thread.Sleep(125);

            if (modalWindow)
                return;

            Hide();
        }

        private void timerPoll_Tick(object sender, EventArgs e)
        {
            Poll();
        }

        private void tttQuit_Click(object sender, EventArgs e)
        {
            if (!Visible)
                Show();

            mtExit_Click(sender, e);
        }

        private void mtLaunchCitramonEx_Click(object sender, EventArgs e)
        {
            NativeLauncher launcher = new NativeLauncher();
            IntPtr eHandle = IntPtr.Zero;
            if (!launcher.RegisterEnterpriseExclusive(ref eHandle))
            {
                string exePath = SettingsHolder.BL_BIN + SettingsHolder.BL_EXE;
                string workingDir = SettingsHolder.BL_BIN;
                string args = $"{SettingsHolder.BL_EXE} enterprise /M /D\"{SettingsHolder.BL_DB_PATH}\"";
                string payloadPath = SettingsHolder.COR_DLL_PATH + SettingsHolder.COR_DLL;

                launcher.Launch(exePath, workingDir, args, ASCIIEncoding.ASCII.GetBytes(payloadPath), eHandle);
            }
            else
            {
                modalWindow = true;
                MetroFramework.MetroMessageBox.Show(this, "Цитрамон (монопольно) уже запущен на данном компьютере!", "COR Launcher", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                modalWindow = false;
            }
        }

        private void trayIcon_DoubleClick(object sender, EventArgs e)
        {
            if (!Visible)
            {
                Poll();
                Show();
            }
            else
            {
                Hide();
            }
        }

        private void mtLaunchConfig_Click(object sender, EventArgs e)
        {
            NativeLauncher launcher = new NativeLauncher();
            IntPtr eHandle = IntPtr.Zero;
            if (!launcher.RegisterConfig(ref eHandle))
            {
                string exePath = SettingsHolder.BL_BIN + SettingsHolder.BL_EXE;
                string workingDir = SettingsHolder.BL_BIN;
                string args = $"{SettingsHolder.BL_EXE} config /D\"{SettingsHolder.BL_DB_PATH}\"";
                string payloadPath = SettingsHolder.COR_DLL_PATH + SettingsHolder.COR_DLL;

                launcher.Launch(exePath, workingDir, args, ASCIIEncoding.ASCII.GetBytes(payloadPath), eHandle);
            }
            else
            {
                modalWindow = true;
                MetroFramework.MetroMessageBox.Show(this, "Конфигуратор уже запущен на данном компьютере!", "COR Launcher", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                modalWindow = false;
            }
        }

        private void mtMonitor_Click(object sender, EventArgs e)
        {
            NativeLauncher launcher = new NativeLauncher();
            IntPtr eHandle = IntPtr.Zero;
            if (!launcher.RegisterMonitor(ref eHandle))
            {
                string exePath = SettingsHolder.BL_BIN + SettingsHolder.BL_EXE;
                string workingDir = SettingsHolder.BL_BIN;
                string args = $"{SettingsHolder.BL_EXE} monitor /D\"{SettingsHolder.BL_DB_PATH}\"";
                string payloadPath = SettingsHolder.COR_DLL_PATH + SettingsHolder.COR_DLL;

                launcher.Launch(exePath, workingDir, args, ASCIIEncoding.ASCII.GetBytes(payloadPath), eHandle);
            }
            else
            {
                modalWindow = true;
                MetroFramework.MetroMessageBox.Show(this, "Монитор пользователей уже запущен для данного пользователя!", "COR Launcher", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                modalWindow = false;
                return;
            }
        }

        private void mtLaunchCitramon_Click(object sender, EventArgs e)
        {
            NativeLauncher launcher = new NativeLauncher();
            IntPtr eHandle = IntPtr.Zero;
            if (!launcher.RegisterEnterpriseExclusive(ref eHandle))
            {
                CloseHandle(eHandle);
            }
            else
            {
                modalWindow = true;
                MetroFramework.MetroMessageBox.Show(this, "На данном компьютере запущен Цитрамон в монопольном режиме! Запуск в разделенном режиме невозможен!", "COR Launcher", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                modalWindow = false;
                return;
            }

            if (!launcher.RegisterEnterprise(ref eHandle))
            {
                string exePath = SettingsHolder.BL_BIN + SettingsHolder.BL_EXE;
                string workingDir = SettingsHolder.BL_BIN;
                string args = $"{SettingsHolder.BL_EXE} enterprise /D\"{SettingsHolder.BL_DB_PATH}\"";
                string payloadPath = SettingsHolder.COR_DLL_PATH + SettingsHolder.COR_DLL;

                launcher.Launch(exePath, workingDir, args, ASCIIEncoding.ASCII.GetBytes(payloadPath), eHandle);
            }
            else
            {
                modalWindow = true;
                MetroFramework.MetroMessageBox.Show(this, "Цитрамон уже запущен для данного пользователя!", "COR Launcher", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                modalWindow = false;
            }
        }

        private void mtExit_Click(object sender, EventArgs e)
        {
            modalWindow = true;
            DialogResult result = MetroFramework.MetroMessageBox.Show(this, "Вы действительно хотите покинуть COR?",
                "Выход", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            modalWindow = false;

            if (result != DialogResult.Yes)
                return;

            Poll();

            Application.Exit();
        }
    }
}
