﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace COR_Launcher
{
    public static class SettingsHolder
    {

        public static Configuration config = null;

        public static string BL_BIN = @"C:\Program files (x86)\1Cv77\BIN\";
        public static string BL_EXE = "1cv7s.exe";
        public static string BL_DB_PATH = @"D:\1C_Bases\Citramon\";
        public static string COR_DLL_PATH = @"C:\Program files (x86)\1Cv77\BIN\";
        public static string COR_DLL = "COR_BL_77.dll";
        public static string COR_USERNAME = "Username";
        public static string COR_PASSWORD = "Password";

        public static void Init()
        {
            config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            string checker = String.Empty;

            if (!String.IsNullOrEmpty(checker = ConfigurationManager.AppSettings["BL_BIN"])) BL_BIN = checker;
            if (!String.IsNullOrEmpty(checker = ConfigurationManager.AppSettings["BL_EXE"])) BL_EXE = checker;
            if (!String.IsNullOrEmpty(checker = ConfigurationManager.AppSettings["BL_DB_PATH"])) BL_DB_PATH = checker;
            if (!String.IsNullOrEmpty(checker = ConfigurationManager.AppSettings["COR_DLL_PATH"])) COR_DLL_PATH = checker;
            if (!String.IsNullOrEmpty(checker = ConfigurationManager.AppSettings["COR_DLL"])) COR_DLL = checker;
            if (!String.IsNullOrEmpty(checker = ConfigurationManager.AppSettings["COR_USERNAME"])) COR_USERNAME = checker;
            if (!String.IsNullOrEmpty(checker = ConfigurationManager.AppSettings["COR_PASSWORD"])) COR_PASSWORD = checker;
        }

        public static void Save()
        {
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }

    }

}
