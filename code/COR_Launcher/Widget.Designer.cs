﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Windows.Forms;
using System.Data;
using System.Drawing;
using System.Configuration;
using MetroFramework.Forms;
using MetroFramework.Controls;
using MetroFramework.Components;

namespace COR_Launcher
{
    partial class Widget : MetroForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private MetroStyleManager metroStyleManager1;
		private MetroStyleExtender metroStyleExtender1;
		private MetroTile mtExit;
		private MetroProgressSpinner msNetworkStatus;
		private MetroTile mtLaunchCitramon;
		private MetroTile mtMonitor;
		private MetroTile mtLaunchConfig;
		private NotifyIcon trayIcon;
		private MetroTile mtLaunchCitramonEx;
		private MetroToolTip metroToolTip;
		private MetroContextMenu trayContextMenu;
		private ToolStripMenuItem tttStatus;
		private ToolStripSeparator tttSeparator1;
		private ToolStripMenuItem tttCitramonEx;
		private ToolStripMenuItem tttCitramon;
		private ToolStripMenuItem tttDesigner;
		private ToolStripMenuItem tttMonitor;
		private ToolStripSeparator toolStripSeparator2;
		private ToolStripMenuItem tttQuit;
		private ToolStripMenuItem toolStripMenuItem1;
		private ToolStripMenuItem toolStripMenuItem2;
		private Timer timerPoll;
		private MetroLabel metroLabel1;
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Widget));
            this.metroStyleManager1 = new MetroFramework.Components.MetroStyleManager(this.components);
            this.metroStyleExtender1 = new MetroFramework.Components.MetroStyleExtender(this.components);
            this.mtExit = new MetroFramework.Controls.MetroTile();
            this.msNetworkStatus = new MetroFramework.Controls.MetroProgressSpinner();
            this.mtLaunchCitramon = new MetroFramework.Controls.MetroTile();
            this.mtMonitor = new MetroFramework.Controls.MetroTile();
            this.mtLaunchConfig = new MetroFramework.Controls.MetroTile();
            this.trayIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.trayContextMenu = new MetroFramework.Controls.MetroContextMenu(this.components);
            this.tttStatus = new System.Windows.Forms.ToolStripMenuItem();
            this.tttSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tttCitramonEx = new System.Windows.Forms.ToolStripMenuItem();
            this.tttCitramon = new System.Windows.Forms.ToolStripMenuItem();
            this.tttDesigner = new System.Windows.Forms.ToolStripMenuItem();
            this.tttMonitor = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tttQuit = new System.Windows.Forms.ToolStripMenuItem();
            this.mtLaunchCitramonEx = new MetroFramework.Controls.MetroTile();
            this.metroToolTip = new MetroFramework.Components.MetroToolTip();
            this.timerPoll = new System.Windows.Forms.Timer(this.components);
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.lSyncFile = new MetroFramework.Controls.MetroLabel();
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager1)).BeginInit();
            this.trayContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroStyleManager1
            // 
            this.metroStyleManager1.Owner = this;
            this.metroStyleManager1.Style = MetroFramework.MetroColorStyle.Teal;
            // 
            // metroStyleExtender1
            // 
            this.metroStyleExtender1.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // mtExit
            // 
            this.mtExit.ActiveControl = null;
            this.mtExit.Location = new System.Drawing.Point(157, 181);
            this.mtExit.Name = "mtExit";
            this.mtExit.Size = new System.Drawing.Size(90, 90);
            this.mtExit.Style = MetroFramework.MetroColorStyle.Orange;
            this.mtExit.TabIndex = 1;
            this.mtExit.Text = "Выход";
            this.mtExit.TileImage = global::COR_Launcher.Properties.Resources.mtExit_TileImage;
            this.mtExit.TileImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.mtExit.UseSelectable = true;
            this.mtExit.UseStyleColors = true;
            this.mtExit.UseTileImage = true;
            this.mtExit.Click += new System.EventHandler(this.mtExit_Click);
            // 
            // msNetworkStatus
            // 
            this.msNetworkStatus.Location = new System.Drawing.Point(223, 38);
            this.msNetworkStatus.Maximum = 100;
            this.msNetworkStatus.Name = "msNetworkStatus";
            this.msNetworkStatus.Size = new System.Drawing.Size(24, 23);
            this.msNetworkStatus.TabIndex = 4;
            this.metroToolTip.SetToolTip(this.msNetworkStatus, "Индикатор обновления");
            this.msNetworkStatus.UseSelectable = true;
            this.msNetworkStatus.Value = 25;
            // 
            // mtLaunchCitramon
            // 
            this.mtLaunchCitramon.ActiveControl = null;
            this.mtLaunchCitramon.Location = new System.Drawing.Point(6, 117);
            this.mtLaunchCitramon.Name = "mtLaunchCitramon";
            this.mtLaunchCitramon.Size = new System.Drawing.Size(145, 58);
            this.mtLaunchCitramon.TabIndex = 6;
            this.mtLaunchCitramon.Text = "Цитрамон";
            this.mtLaunchCitramon.TileImage = global::COR_Launcher.Properties.Resources.mtLaunchCitramon_TileImage;
            this.mtLaunchCitramon.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.mtLaunchCitramon.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.mtLaunchCitramon.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.mtLaunchCitramon.UseSelectable = true;
            this.mtLaunchCitramon.UseStyleColors = true;
            this.mtLaunchCitramon.UseTileImage = true;
            this.mtLaunchCitramon.Click += new System.EventHandler(this.mtLaunchCitramon_Click);
            // 
            // mtMonitor
            // 
            this.mtMonitor.ActiveControl = null;
            this.mtMonitor.Location = new System.Drawing.Point(157, 117);
            this.mtMonitor.Name = "mtMonitor";
            this.mtMonitor.Size = new System.Drawing.Size(90, 58);
            this.mtMonitor.TabIndex = 7;
            this.mtMonitor.Text = "Монитор";
            this.mtMonitor.TileImage = global::COR_Launcher.Properties.Resources.mtMonitor_TileImage;
            this.mtMonitor.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.mtMonitor.UseSelectable = true;
            this.mtMonitor.UseTileImage = true;
            this.mtMonitor.Click += new System.EventHandler(this.mtMonitor_Click);
            // 
            // mtLaunchConfig
            // 
            this.mtLaunchConfig.ActiveControl = null;
            this.mtLaunchConfig.Location = new System.Drawing.Point(96, 181);
            this.mtLaunchConfig.Name = "mtLaunchConfig";
            this.mtLaunchConfig.Size = new System.Drawing.Size(55, 55);
            this.mtLaunchConfig.TabIndex = 8;
            this.mtLaunchConfig.TileImage = global::COR_Launcher.Properties.Resources.mtLaunchConfig_TileImage;
            this.mtLaunchConfig.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroToolTip.SetToolTip(this.mtLaunchConfig, "Конфигуратор");
            this.mtLaunchConfig.UseSelectable = true;
            this.mtLaunchConfig.UseTileImage = true;
            this.mtLaunchConfig.Click += new System.EventHandler(this.mtLaunchConfig_Click);
            // 
            // trayIcon
            // 
            this.trayIcon.ContextMenuStrip = this.trayContextMenu;
            this.trayIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("trayIcon.Icon")));
            this.trayIcon.Text = "COR";
            this.trayIcon.Visible = true;
            this.trayIcon.DoubleClick += new System.EventHandler(this.trayIcon_DoubleClick);
            // 
            // trayContextMenu
            // 
            this.trayContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tttStatus,
            this.tttSeparator1,
            this.tttCitramonEx,
            this.tttCitramon,
            this.tttDesigner,
            this.tttMonitor,
            this.toolStripSeparator2,
            this.tttQuit});
            this.trayContextMenu.Name = "trayContextMenu";
            this.trayContextMenu.Size = new System.Drawing.Size(214, 148);
            // 
            // tttStatus
            // 
            this.tttStatus.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.tttStatus.Image = global::COR_Launcher.Properties.Resources.tttStatus_Image;
            this.tttStatus.Name = "tttStatus";
            this.tttStatus.Size = new System.Drawing.Size(213, 22);
            this.tttStatus.Text = "Состояние";
            this.tttStatus.Click += new System.EventHandler(this.trayIcon_DoubleClick);
            // 
            // tttSeparator1
            // 
            this.tttSeparator1.Name = "tttSeparator1";
            this.tttSeparator1.Size = new System.Drawing.Size(210, 6);
            // 
            // tttCitramonEx
            // 
            this.tttCitramonEx.Image = global::COR_Launcher.Properties.Resources.tttCitramonEx_Image;
            this.tttCitramonEx.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tttCitramonEx.Name = "tttCitramonEx";
            this.tttCitramonEx.Size = new System.Drawing.Size(213, 22);
            this.tttCitramonEx.Text = "Цитрамон (монопольно)";
            this.tttCitramonEx.Click += new System.EventHandler(this.mtLaunchCitramonEx_Click);
            // 
            // tttCitramon
            // 
            this.tttCitramon.Image = global::COR_Launcher.Properties.Resources.tttCitramon_Image;
            this.tttCitramon.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tttCitramon.Name = "tttCitramon";
            this.tttCitramon.Size = new System.Drawing.Size(213, 22);
            this.tttCitramon.Text = "Цитрамон";
            this.tttCitramon.Click += new System.EventHandler(this.mtLaunchCitramon_Click);
            // 
            // tttDesigner
            // 
            this.tttDesigner.Image = global::COR_Launcher.Properties.Resources.tttDesigner_Image;
            this.tttDesigner.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tttDesigner.Name = "tttDesigner";
            this.tttDesigner.Size = new System.Drawing.Size(213, 22);
            this.tttDesigner.Text = "Конфигуратор";
            this.tttDesigner.Click += new System.EventHandler(this.mtLaunchConfig_Click);
            // 
            // tttMonitor
            // 
            this.tttMonitor.Image = global::COR_Launcher.Properties.Resources.tttMonitor_Image;
            this.tttMonitor.Name = "tttMonitor";
            this.tttMonitor.Size = new System.Drawing.Size(213, 22);
            this.tttMonitor.Text = "Монитор";
            this.tttMonitor.Click += new System.EventHandler(this.mtMonitor_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(210, 6);
            // 
            // tttQuit
            // 
            this.tttQuit.Image = global::COR_Launcher.Properties.Resources.tttQuit_Image;
            this.tttQuit.Name = "tttQuit";
            this.tttQuit.Size = new System.Drawing.Size(213, 22);
            this.tttQuit.Text = "Выход";
            this.tttQuit.Click += new System.EventHandler(this.mtExit_Click);
            // 
            // mtLaunchCitramonEx
            // 
            this.mtLaunchCitramonEx.ActiveControl = null;
            this.mtLaunchCitramonEx.Location = new System.Drawing.Point(6, 67);
            this.mtLaunchCitramonEx.Name = "mtLaunchCitramonEx";
            this.mtLaunchCitramonEx.Size = new System.Drawing.Size(241, 44);
            this.mtLaunchCitramonEx.TabIndex = 9;
            this.mtLaunchCitramonEx.Text = "Цитрамон (монопольно)";
            this.mtLaunchCitramonEx.TileImage = global::COR_Launcher.Properties.Resources.mtLaunchCitramonEx_TileImage;
            this.mtLaunchCitramonEx.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.mtLaunchCitramonEx.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.mtLaunchCitramonEx.UseSelectable = true;
            this.mtLaunchCitramonEx.UseStyleColors = true;
            this.mtLaunchCitramonEx.UseTileImage = true;
            this.mtLaunchCitramonEx.Click += new System.EventHandler(this.mtLaunchCitramonEx_Click);
            // 
            // metroToolTip
            // 
            this.metroToolTip.Style = MetroFramework.MetroColorStyle.Teal;
            this.metroToolTip.StyleManager = null;
            this.metroToolTip.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // timerPoll
            // 
            this.timerPoll.Enabled = true;
            this.timerPoll.Interval = 125;
            this.timerPoll.Tick += new System.EventHandler(this.timerPoll_Tick);
            // 
            // metroLabel1
            // 
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.Location = new System.Drawing.Point(6, 15);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(218, 42);
            this.metroLabel1.TabIndex = 11;
            this.metroLabel1.Text = "Синхронизация";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.metroLabel1.WrapToLine = true;
            // 
            // lSyncFile
            // 
            this.lSyncFile.Location = new System.Drawing.Point(6, 12);
            this.lSyncFile.Name = "lSyncFile";
            this.lSyncFile.Size = new System.Drawing.Size(241, 23);
            this.lSyncFile.TabIndex = 12;
            this.lSyncFile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Widget
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = MetroFramework.Forms.MetroFormBorderStyle.FixedSingle;
            this.ClientSize = new System.Drawing.Size(250, 274);
            this.ControlBox = false;
            this.Controls.Add(this.lSyncFile);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.mtLaunchCitramonEx);
            this.Controls.Add(this.mtLaunchConfig);
            this.Controls.Add(this.mtMonitor);
            this.Controls.Add(this.mtLaunchCitramon);
            this.Controls.Add(this.msNetworkStatus);
            this.Controls.Add(this.mtExit);
            this.DisplayHeader = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Movable = false;
            this.Name = "Widget";
            this.Padding = new System.Windows.Forms.Padding(0, 30, 0, 0);
            this.Resizable = false;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Style = MetroFramework.MetroColorStyle.Teal;
            this.Text = "COR";
            this.TopMost = true;
            this.Deactivate += new System.EventHandler(this.Widget_Deactivate);
            this.Load += new System.EventHandler(this.Widget_Load);
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager1)).EndInit();
            this.trayContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroLabel lSyncFile;
    }
}

