﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace COR_Launcher
{
    public class NativeLauncher
    {
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct STARTUPINFOA
        {
            public uint cb;
            public string lpReserved;
            public string lpDesktop;
            public string lpTitle;
            public uint dwX;
            public uint dwY;
            public uint dwXSize;
            public uint dwYSize;
            public uint dwXCountChars;
            public uint dwYCountChars;
            public uint dwFillAttribute;
            public uint dwFlags;
            public ushort wShowWindow;
            public ushort cbReserved2;
            public unsafe byte* lpReserved2;
            public unsafe byte* hStdInput;
            public unsafe byte* hStdOutput;
            public unsafe byte* hStdError;
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        internal struct PROCESS_INFORMATION
        {
            public IntPtr hProcess;
            public IntPtr hThread;
            public int dwProcessId;
            public int dwThreadId;
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct SECURITY_ATTRIBUTES
        {
            public int nLength;
            public unsafe byte* lpSecurityDescriptor;
            public int bInheritHandle;
        }

        [DllImport("kernel32.dll", SetLastError = true, EntryPoint = "GetStartupInfoA", CharSet = CharSet.Ansi)]
        public static extern void GetStartupInfo(out STARTUPINFOA lpStartupInfo);

        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Ansi)]
        static extern bool CreateProcessA(
           string lpApplicationName,
           string lpCommandLine,
           ref SECURITY_ATTRIBUTES lpProcessAttributes,
           ref SECURITY_ATTRIBUTES lpThreadAttributes,
           bool bInheritHandles,
           uint dwCreationFlags,
           IntPtr lpEnvironment,
           string lpCurrentDirectory,
           [In] ref STARTUPINFOA lpStartupInfo,
           out PROCESS_INFORMATION lpProcessInformation);

        [Flags]
        enum CreateProcessFlags : uint
        {
            DEBUG_PROCESS = 0x00000001,
            DEBUG_ONLY_THIS_PROCESS = 0x00000002,
            CREATE_SUSPENDED = 0x00000004,
            DETACHED_PROCESS = 0x00000008,
            CREATE_NEW_CONSOLE = 0x00000010,
            NORMAL_PRIORITY_CLASS = 0x00000020,
            IDLE_PRIORITY_CLASS = 0x00000040,
            HIGH_PRIORITY_CLASS = 0x00000080,
            REALTIME_PRIORITY_CLASS = 0x00000100,
            CREATE_NEW_PROCESS_GROUP = 0x00000200,
            CREATE_UNICODE_ENVIRONMENT = 0x00000400,
            CREATE_SEPARATE_WOW_VDM = 0x00000800,
            CREATE_SHARED_WOW_VDM = 0x00001000,
            CREATE_FORCEDOS = 0x00002000,
            BELOW_NORMAL_PRIORITY_CLASS = 0x00004000,
            ABOVE_NORMAL_PRIORITY_CLASS = 0x00008000,
            INHERIT_PARENT_AFFINITY = 0x00010000,
            INHERIT_CALLER_PRIORITY = 0x00020000,
            CREATE_PROTECTED_PROCESS = 0x00040000,
            EXTENDED_STARTUPINFO_PRESENT = 0x00080000,
            PROCESS_MODE_BACKGROUND_BEGIN = 0x00100000,
            PROCESS_MODE_BACKGROUND_END = 0x00200000,
            CREATE_BREAKAWAY_FROM_JOB = 0x01000000,
            CREATE_PRESERVE_CODE_AUTHZ_LEVEL = 0x02000000,
            CREATE_DEFAULT_ERROR_MODE = 0x04000000,
            CREATE_NO_WINDOW = 0x08000000,
            PROFILE_USER = 0x10000000,
            PROFILE_KERNEL = 0x20000000,
            PROFILE_SERVER = 0x40000000,
            CREATE_IGNORE_SYSTEM_DEFAULT = 0x80000000,
        }

        [Flags]
        public enum AllocationType
        {
            Commit = 0x1000,
            Reserve = 0x2000,
            Decommit = 0x4000,
            Release = 0x8000,
            Reset = 0x80000,
            Physical = 0x400000,
            TopDown = 0x100000,
            WriteWatch = 0x200000,
            LargePages = 0x20000000
        }

        [Flags]
        public enum MemoryProtection
        {
            Execute = 0x10,
            ExecuteRead = 0x20,
            ExecuteReadWrite = 0x40,
            ExecuteWriteCopy = 0x80,
            NoAccess = 0x01,
            ReadOnly = 0x02,
            ReadWrite = 0x04,
            WriteCopy = 0x08,
            GuardModifierflag = 0x100,
            NoCacheModifierflag = 0x200,
            WriteCombineModifierflag = 0x400
        }


        [DllImport("kernel32.dll", SetLastError = true, ExactSpelling = true, CharSet = CharSet.Ansi)]
        static extern IntPtr VirtualAllocEx(IntPtr hProcess, IntPtr lpAddress,
            uint dwSize, AllocationType flAllocationType, MemoryProtection flProtect);

        public enum CONTEXT_FLAGS : uint
        {
            CONTEXT_i386 = 0x10000,
            CONTEXT_i486 = 0x10000,   //  same as i386
            CONTEXT_CONTROL = CONTEXT_i386 | 0x01, // SS:SP, CS:IP, FLAGS, BP
            CONTEXT_INTEGER = CONTEXT_i386 | 0x02, // AX, BX, CX, DX, SI, DI
            CONTEXT_SEGMENTS = CONTEXT_i386 | 0x04, // DS, ES, FS, GS
            CONTEXT_FLOATING_POINT = CONTEXT_i386 | 0x08, // 387 state
            CONTEXT_DEBUG_REGISTERS = CONTEXT_i386 | 0x10, // DB 0-3,6,7
            CONTEXT_EXTENDED_REGISTERS = CONTEXT_i386 | 0x20, // cpu specific extensions
            CONTEXT_FULL = CONTEXT_CONTROL | CONTEXT_INTEGER | CONTEXT_SEGMENTS,
            CONTEXT_ALL = CONTEXT_CONTROL | CONTEXT_INTEGER | CONTEXT_SEGMENTS | CONTEXT_FLOATING_POINT | CONTEXT_DEBUG_REGISTERS | CONTEXT_EXTENDED_REGISTERS
        }


        [StructLayout(LayoutKind.Sequential)]
        public struct FLOATING_SAVE_AREA
        {
            public uint ControlWord;
            public uint StatusWord;
            public uint TagWord;
            public uint ErrorOffset;
            public uint ErrorSelector;
            public uint DataOffset;
            public uint DataSelector;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 80)]
            public byte[] RegisterArea;
            public uint Cr0NpxState;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct CONTEXT
        {
            public uint ContextFlags; //set this to an appropriate value 
                                      // Retrieved by CONTEXT_DEBUG_REGISTERS 
            public uint Dr0;
            public uint Dr1;
            public uint Dr2;
            public uint Dr3;
            public uint Dr6;
            public uint Dr7;
            // Retrieved by CONTEXT_FLOATING_POINT 
            public FLOATING_SAVE_AREA FloatSave;
            // Retrieved by CONTEXT_SEGMENTS 
            public uint SegGs;
            public uint SegFs;
            public uint SegEs;
            public uint SegDs;
            // Retrieved by CONTEXT_INTEGER 
            public uint Edi;
            public uint Esi;
            public uint Ebx;
            public uint Edx;
            public uint Ecx;
            public uint Eax;
            // Retrieved by CONTEXT_CONTROL 
            public uint Ebp;
            public uint Eip;
            public uint SegCs;
            public uint EFlags;
            public uint Esp;
            public uint SegSs;
            // Retrieved by CONTEXT_EXTENDED_REGISTERS 
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 512)]
            public byte[] ExtendedRegisters;
        }

        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Ansi)]
        static extern bool GetThreadContext(IntPtr hThread, ref CONTEXT lpContext);

        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Ansi)]
        public static extern bool WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, 
            byte[] lpBuffer, Int32 nSize, out IntPtr lpNumberOfBytesWritten);

        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Ansi)]
        public static extern bool SetThreadContext(IntPtr hThread, [In] ref CONTEXT lpContext);

        [DllImport("kernel32.dll")]
        static extern bool IsDebuggerPresent();

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Ansi)]
        [return: MarshalAs(UnmanagedType.U4)]
        private static extern uint MessageBoxA(IntPtr hwnd,
            [MarshalAs(UnmanagedType.LPTStr)]  String text,
            [MarshalAs(UnmanagedType.LPTStr)] String title,
            [MarshalAs(UnmanagedType.U4)] uint type);

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern uint ResumeThread(IntPtr hThread);

        [DllImport("kernel32", CharSet = CharSet.Ansi, ExactSpelling = true, SetLastError = true)]
        static extern IntPtr GetProcAddress(IntPtr hModule, string procName);

        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Ansi)]
        static extern IntPtr LoadLibraryA(string lpFileName);

        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Ansi)]
        static extern IntPtr CreateEventA(IntPtr lpEventAttributes, bool bManualReset, bool bInitialState, string lpName);

        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Ansi)]
        static extern bool CloseHandle(IntPtr hHandle);

        private const uint SHELLCODE_LENGTH = 22;

        public NativeLauncher()
        {

        }

        ~NativeLauncher()
        {

        }

        public void Launch(string exeFile, string workingDir, string arguments, byte[] payload, IntPtr eventHandle)
        {
            STARTUPINFOA StartupInfo = new STARTUPINFOA();
            PROCESS_INFORMATION ProcessInformation = new PROCESS_INFORMATION();

            SECURITY_ATTRIBUTES pSecurityAttributes = new SECURITY_ATTRIBUTES();
            SECURITY_ATTRIBUTES tSecurityAttributes = new SECURITY_ATTRIBUTES();

            StartupInfo.cb = (uint)Marshal.SizeOf(typeof(STARTUPINFOA));

            bool result = CreateProcessA(exeFile, arguments,
                ref pSecurityAttributes, ref tSecurityAttributes,
                false, (uint)(CreateProcessFlags.CREATE_SUSPENDED | CreateProcessFlags.DETACHED_PROCESS),
                IntPtr.Zero, workingDir, ref StartupInfo, out ProcessInformation);

            if (!result) return;

            IntPtr hProcess = ProcessInformation.hProcess;
            IntPtr hThread = ProcessInformation.hThread;

            IntPtr remote_dllString = VirtualAllocEx(hProcess, IntPtr.Zero, (uint)payload.Length + 1, AllocationType.Commit, MemoryProtection.ReadWrite);

            CONTEXT context = new CONTEXT();

            context.ContextFlags = (uint)CONTEXT_FLAGS.CONTEXT_CONTROL;
            GetThreadContext(hThread, ref context);

            unsafe
            {
                byte* retChar = (byte*)&context.Eip;
                byte* strChar = (byte*)&remote_dllString;

                IntPtr api = GetProcAddress(LoadLibraryA("kernel32.dll"), "LoadLibraryA");

                byte* apiChar = (byte*)&api;
                byte[] sc = {
                    0x68, retChar[0], retChar[1], retChar[2], retChar[3],
                    0x9C,
                    0x60,
                    0x68, strChar[0], strChar[1], strChar[2], strChar[3],
                    0xB8, apiChar[0], apiChar[1], apiChar[2], apiChar[3],
                    0xFF, 0xD0,
                    0x61,
                    0x9D,
                    0xC3
                };

                IntPtr remote_shellcode = VirtualAllocEx(hProcess, IntPtr.Zero,
                    (uint)sc.Length, AllocationType.Commit, MemoryProtection.ExecuteReadWrite);

                IntPtr bytesWritten = IntPtr.Zero;

                WriteProcessMemory(hProcess, remote_dllString, payload, payload.Length + 1, out bytesWritten);
                WriteProcessMemory(hProcess, remote_shellcode, sc, sc.Length, out bytesWritten);

                context.Eip = (uint)remote_shellcode;
                context.ContextFlags = (uint)CONTEXT_FLAGS.CONTEXT_CONTROL;
                SetThreadContext(hThread, ref context);
            }

            if (IsDebuggerPresent())
                MessageBoxA(IntPtr.Zero, "Нужно сделать Attach", "Отладка", 0);

            ResumeThread(hThread);

            HandleFreeOnRelease releaseManager = new HandleFreeOnRelease(hProcess, eventHandle);
            ThreadStart actionDelegate = new ThreadStart(() => { releaseManager.FreeHandle(); });
            Thread freeOnRelease = new Thread(actionDelegate);
            freeOnRelease.Start();
        }

	    public bool RegisterConfig(ref IntPtr hProcess)
        {
            IntPtr m_hStartEvent = CreateEventA(IntPtr.Zero, false, false, "Global\\COR_BL_Config");
            hProcess = IntPtr.Zero;

            if (m_hStartEvent == IntPtr.Zero)
            {
                CloseHandle(m_hStartEvent);
                return true;
            }

            if (Marshal.GetLastWin32Error() == 183L)
            {

                CloseHandle(m_hStartEvent);
                m_hStartEvent = IntPtr.Zero;
                return true;
            }

            hProcess = m_hStartEvent;

            return false;
        }

        public bool RegisterEnterprise(ref IntPtr hProcess)
        {
            IntPtr m_hStartEvent = CreateEventA(IntPtr.Zero, false, false, "Local\\COR_BL_Enterprise");
            hProcess = IntPtr.Zero;

            if (m_hStartEvent == IntPtr.Zero)
            {
                CloseHandle(m_hStartEvent);
                return true;
            }

            if (Marshal.GetLastWin32Error() == 183L)
            {
                CloseHandle(m_hStartEvent);
                m_hStartEvent = IntPtr.Zero;
                return true;
            }

            hProcess = m_hStartEvent;

            return false;
        }

        public bool RegisterEnterpriseExclusive(ref IntPtr hProcess)
        {
            IntPtr m_hStartEvent = CreateEventA(IntPtr.Zero, false, false, "Global\\COR_BL_EnterpriseEx");
            hProcess = IntPtr.Zero;

            if (m_hStartEvent == IntPtr.Zero)
            {
                CloseHandle(m_hStartEvent);
                return true;
            }

            if (Marshal.GetLastWin32Error() == 183L)
            {
                CloseHandle(m_hStartEvent);
                m_hStartEvent = IntPtr.Zero;
                return true;
            }

            hProcess = m_hStartEvent;

            return false;
        }

        public bool RegisterMonitor(ref IntPtr hProcess)
        {
            IntPtr m_hStartEvent = CreateEventA(IntPtr.Zero, false, false, "Local\\COR_BL_Monitor");
            hProcess = IntPtr.Zero;

            if (m_hStartEvent == IntPtr.Zero)
            {
                CloseHandle(m_hStartEvent);
                return true;
            }

            if (Marshal.GetLastWin32Error() == 183L)
            {
                CloseHandle(m_hStartEvent);
                m_hStartEvent = IntPtr.Zero;
                return true;
            }

            hProcess = m_hStartEvent;

            return false;
        }
    }

    public class HandleFreeOnRelease
    {
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern UInt32 WaitForSingleObject(IntPtr hHandle, UInt32 dwMilliseconds);

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool CloseHandle(IntPtr hHandle);

        public const UInt32 WAIT_FAILED = 0xFFFFFFFF;
        public const UInt32 INFINITE = 0xFFFFFFFF;
        public const UInt32 WAIT_ABANDONED = 0x00000080;
        public const UInt32 WAIT_OBJECT_0 = 0x00000000;
        public const UInt32 WAIT_TIMEOUT = 0x00000102;

        private IntPtr processHandle = IntPtr.Zero;
        private IntPtr eventHandle = IntPtr.Zero;

        public HandleFreeOnRelease(IntPtr pHandle, IntPtr eHandle)
        {
            processHandle = pHandle;
            eventHandle = eHandle;
        }

        public void FreeHandle()
        {
            WaitForSingleObject(processHandle, INFINITE);
            CloseHandle(eventHandle);
            CloseHandle(processHandle);
        }
    };
}
