#include "stdafx.h"
#include "Entry.h"

#include "DebugTools.hpp"
//#include "Networking.h"

#include "EventSubscription.hpp"

//#include "CORServer.hpp"

#include "GlobalModuleTools.hpp"
#include "StrMatchContext.hpp"
#include "FlagsChecker.hpp"

#include "StringConverter.hpp"
#include "RESTClient.hpp"
#include "JSONHolder.hpp"

bool CCORExtensions::LauncherPresent = false;
bool CCORExtensions::InitDone = false;

CApp7* CCORExtensions::MainApp;
CDBEngDDB7Service* CCORExtensions::DBService;

CCORExtensions initializer;

//////////////////////////////////////////////////////////////////////////
bool RegisterBlDesigner()
{
	HANDLE  m_hStartEvent = CreateEventA(NULL, FALSE, FALSE, "Global\\COR_BL_Config");

	if (m_hStartEvent == NULL)
	{
		CloseHandle(m_hStartEvent);
		return true;
	}

	if (GetLastError() == ERROR_ALREADY_EXISTS) {

		CloseHandle(m_hStartEvent);
		m_hStartEvent = NULL;
		return true;
	}
	
	CloseHandle(m_hStartEvent);
	return false;
}

//////////////////////////////////////////////////////////////////////////
CCORExtensions::CCORExtensions()
{
	CONSTRUCT_SINGLETON(CCORExtensions);

	hookingEngine = new Hooker();
	registrar = new Registrator();

	if (RegisterBlDesigner())
	{
		Log::Debug("COR Extensions: designer mode");
		appMode = AppMode::DESIGNER;
	}
	else
	{
		Log::Debug("COR Extensions: enterprise mode");
		appMode = AppMode::ENTERPRISE;

		registrar->Register<GlobalModuleTools>("GlobalModuleTools", "����������������������������");
		registrar->Register<StrMatch>("StrMatch", "��������������");
		registrar->Register<FlagsChecker>("SystemFlags", "��������������");
		registrar->Register<StringCoverter>("Strings", "������");
		registrar->Register<RESTClient>("RESTClient", "���������");
		registrar->Register<JSONHolder>("JSONObject", "������JSON");
		registrar->Register<EventSubscription>("Remoting", "���������������");

#ifdef BSL_DEBUG
		registrar->Register<BLDebugTools>("Debug", "�������");
#endif // BSL_DEBUG
	}

	curl_global_init(CURL_GLOBAL_ALL);

}

//////////////////////////////////////////////////////////////////////////
CCORExtensions::~CCORExtensions()
{
	delete hive;
	UnhookWindowsHookEx(COR.idleHookHandle);
	curl_global_cleanup();
}

LRESULT CALLBACK OnForeGroundIdle(int nCode, WPARAM wParam, LPARAM lParam)
{
	if (COR.InitDone)
	{
		COR.hive->UpdateCodeExecution();
		_sleep(25);
	}

	return ::CallNextHookEx(COR.idleHookHandle, nCode, wParam, lParam);
}

void CCORExtensions::AfterInit()
{
	theApp = new CCORExtensionsApp();

	hive = new Hive();

	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	HINSTANCE dll_instance = AfxGetInstanceHandle();

	COR.idleHookHandle = SetWindowsHookExA(WH_FOREGROUNDIDLE, (HOOKPROC)OnForeGroundIdle, dll_instance, 0);

	InitDone = true;
}
