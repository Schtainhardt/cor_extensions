#include "stdafx.h"
#include "Entry.h"
#include "Log.h"

#include <iostream>

Log* Log::globalLog = nullptr;
WORD Log::defaultColor;

Log::Log()
{
	globalLog = nullptr;

	//if (globalLog != nullptr)
	//{
	//	return;
	//}

	InitializeCriticalSection(&logProtector);

#ifdef BSL_DEBUG
	AllocConsole();

	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	freopen("CONOUT$", "w", stdout);

	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO csbi;

	GetConsoleScreenBufferInfo(hStdOut, &csbi);
#endif

	defaultColor = 0x07; //csbi.wAttributes;
	globalLog = this;
}

Log::~Log()
{
	DeleteCriticalSection(&logProtector);
}

void Log::SetColor(MessageColor newColor)
{
#ifdef BSL_DEBUG
	WORD _newColor = newColor;

	switch (newColor)
	{
	case DEFAULT:
		_newColor = defaultColor;
		break;
	default:
		break;
	}

	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdOut, _newColor);
#endif
}

void Log::RestoreColor()
{
	SetColor(DEFAULT);
}

void Log::WriteText(const char* message)
{

}

void Log::WriteText(const char* message, va_list args)
{
	while (*message != '\0')
	{
		if (*message == '$') {
			void* i = va_arg(args, void*);
			std::cout << "0x" << i;
		}
		else if (*message == '%')
		{
			const char* i = va_arg(args, const char*);
			if (i != nullptr)
				std::cout << i;
		}
		else if (*message == '#')
		{
			int i = va_arg(args, int);
			std::cout << i;
		}
		else if (*message == '@')
		{
			++message;
			MessageColor msgColor = (MessageColor)*message;
			globalLog->SetColor(msgColor);
		}
		else
		{
			std::cout << *message;
		}

		++message;
	}

	std::cout << std::endl;
}

void Log::DebugFmt(const char* message, ...)
{

	if (globalLog == nullptr)
	{
		globalLog = (Log*)malloc(sizeof(Log));
		globalLog = new(globalLog) Log();
	}

#ifdef BSL_DEBUG
	EnterCriticalSection(&globalLog->logProtector);
	std::cout << "[";
	globalLog->SetColor(bMAGENTA);
	std::cout << "DEBUG";
	globalLog->RestoreColor();
	std::cout << "]\t";

	va_list args;
	va_start(args, message);
	globalLog->WriteText(message, args);
	va_end(args);

	LeaveCriticalSection(&globalLog->logProtector);
#endif
}

void Log::Debug(const char* message)
{
	if (globalLog == nullptr)
	{
		globalLog = (Log*)malloc(sizeof(Log));
		globalLog = new(globalLog) Log();
	}

#ifdef BSL_DEBUG
	EnterCriticalSection(&globalLog->logProtector);
	std::cout << "[";
	globalLog->SetColor(bMAGENTA);
	std::cout << "DEBUG";
	globalLog->RestoreColor();
	std::cout << "]\t" << message << std::endl;
	LeaveCriticalSection(&globalLog->logProtector);
#endif // BSL_DEBUG
}

void Log::InfoFmt(const char* message, ...)
{
	if (globalLog == nullptr)
	{
		globalLog = (Log*)malloc(sizeof(Log));
		globalLog = new(globalLog) Log();
	}

#ifdef BSL_DEBUG
	EnterCriticalSection(&globalLog->logProtector);

	std::cout << "[";
	globalLog->SetColor(bCYAN);
	std::cout << "INFO ";
	globalLog->RestoreColor();
	std::cout << "]\t";

	va_list args;
	va_start(args, message);
	globalLog->WriteText(message, args);
	va_end(args);

	LeaveCriticalSection(&globalLog->logProtector);

#endif // BSL_DEBUG
}

void Log::Info(const char* message)
{
	if (globalLog == nullptr)
	{
		globalLog = (Log*)malloc(sizeof(Log));
		globalLog = new(globalLog) Log();
	}

#ifdef BSL_DEBUG
	EnterCriticalSection(&globalLog->logProtector);

	std::cout << "[";
	globalLog->SetColor(bCYAN);
	std::cout << "INFO ";
	globalLog->RestoreColor();
	std::cout << "]\t" << message << std::endl;
	LeaveCriticalSection(&globalLog->logProtector);

#endif // BSL_DEBUG
}

void Log::WarningFmt(const char* message, ...)
{
	if (globalLog == nullptr)
	{
		globalLog = (Log*)malloc(sizeof(Log));
		globalLog = new(globalLog) Log();
	}

#ifdef BSL_DEBUG
	EnterCriticalSection(&globalLog->logProtector);

	std::cout << "[";
	globalLog->SetColor(bYELLOW);
	std::cout << "WARN ";
	globalLog->RestoreColor();
	std::cout << "]\t";

	va_list args;
	va_start(args, message);
	globalLog->WriteText(message, args);
	va_end(args);

	LeaveCriticalSection(&globalLog->logProtector);

#endif // BSL_DEBUG
}

void Log::Warning(const char* message)
{
	if (globalLog == nullptr)
	{
		globalLog = (Log*)malloc(sizeof(Log));
		globalLog = new(globalLog) Log();
	}

#ifdef BSL_DEBUG
	EnterCriticalSection(&globalLog->logProtector);

	std::cout << "[";
	globalLog->SetColor(bYELLOW);
	std::cout << "WARN ";
	globalLog->RestoreColor();
	std::cout << "]\t" << message << std::endl;
	LeaveCriticalSection(&globalLog->logProtector);

#endif // BSL_DEBUG
}

void Log::ErrorFmt(const char* message, ...)
{
	if (globalLog == nullptr)
	{
		globalLog = (Log*)malloc(sizeof(Log));
		globalLog = new(globalLog) Log();
	}

#ifdef BSL_DEBUG
	EnterCriticalSection(&globalLog->logProtector);

	std::cout << "[";
	globalLog->SetColor(bRED);
	std::cout << "ERROR";
	globalLog->RestoreColor();
	std::cout << "]\t";

	va_list args;
	va_start(args, message);
	globalLog->WriteText(message, args);
	va_end(args);

	LeaveCriticalSection(&globalLog->logProtector);

#endif // BSL_DEBUG
}

void Log::Error(const char* message)
{
	if (globalLog == nullptr)
	{
		globalLog = (Log*)malloc(sizeof(Log));
		globalLog = new(globalLog) Log();
	}

#ifdef BSL_DEBUG
	EnterCriticalSection(&globalLog->logProtector);
	std::cout << "[";
	globalLog->SetColor(bRED);
	std::cout << "ERROR";
	globalLog->RestoreColor();
	std::cout << "]\t" << message << std::endl;
	LeaveCriticalSection(&globalLog->logProtector);

#endif // BSL_DEBUG
}