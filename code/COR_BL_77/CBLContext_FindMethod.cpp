#include "stdafx.h"
#include "CBLContext_FindMethod.h"

#include "Entry.h"
#include "HookTools.h"

//////////////////////////////////////////////////////////////////////////
// CBLContext::FindMethod
Hook_FindMethod::FindMethodFunc Hook_FindMethod::orig_FindMethod;

Hook_FindMethod::Hook_FindMethod()
{

	HOOK_MESSAGE;

	HANDLE processHandle = GetCurrentProcess();
	HMODULE moduleHandle = GetModuleHandleA(module_name);
	FindMethodFunc registrar = (FindMethodFunc)GetProcAddress(moduleHandle, mangled_func);

	orig_FindMethod = (FindMethodFunc)HookFunctionCdecl<7>(processHandle, registrar, hk_FindMethod);
}

int __stdcall Hook_FindMethod::hk_FindMethod(char const* methodName)
{
	Log::DebugFmt("Looking for \t @\x0b % @\x07", methodName);
	return orig_FindMethod(methodName);
}