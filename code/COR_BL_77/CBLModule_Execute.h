#pragma once
#include "stdafx.h"

//////////////////////////////////////////////////////////////////////////
// CBLModule:: -> Execute
class Hook_Execute
{
public:
	const char* module_name = "blang.dll";
	const char* mangled_func = "?Execute@CBLModule@@QAEHXZ";

	Hook_Execute();

private:

	typedef int(__thiscall *ExecuteFunc)();
	static ExecuteFunc orig_Execute;
	static int __stdcall hk_Execute();

};