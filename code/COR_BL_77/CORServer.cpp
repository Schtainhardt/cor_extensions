#include "stdafx.h"

#if 0

#include "CORServer.hpp"
#include <string.h>

#include "EventManager.h"
#include "Networking.h"

IMPLEMENT_DYNCREATE(CORServer, CBLContext)

//////////////////////////////////////////////////////////////////////////
//! METHODS DEFINITION
struct CORServer::paramdefs CORServer::defFnNames[] = {

	// name_eng			name_rus				returns val		args count
	{ { "Connect",		"������������" },		TRUE,			4 },
	{ { "Disconnect",	"�����������" },		TRUE,			0 },
	{ { "CheckConnection", "������������������" }, FALSE,		0 },
	{ { NULL,			NULL },					NULL,			NULL }

};

//////////////////////////////////////////////////////////////////////////
//! FIELDS DEFINITION
struct CORServer::parampropdefs CORServer::defPropNames[] = {

	// name_eng			name_rus				readable		writeable
	{ { "Version",		"������" },				TRUE,			FALSE },
	{ { NULL,			NULL },					NULL,			NULL }

};

//////////////////////////////////////////////////////////////////////////
CORServer::CORServer()
{
	Log::DebugFmt("% @\x0F%@\x07 %", "\t-> CORServer rev.", "1.0", "[02 nov 17]");
}

//////////////////////////////////////////////////////////////////////////
BOOL CORServer::Connect(const char* User, const char* Company, const char* Unit, const char* Subunit)
{
	return NetworkMan.Connect(User, Company, Unit, Subunit);
}

void CORServer::CheckConnection()
{
	return NetworkMan.CheckConnection();
}

BOOL CORServer::Disconnect()
{
	return NetworkMan.Disconnect();
}

#endif