#include "stdafx.h"
#include "RESTClient.hpp"

#include "UTF_Tools.h"

#include "base64.hpp"

IMPLEMENT_DYNCREATE(RESTClient, CBLContext)

//////////////////////////////////////////////////////////////////////////
//! METHODS DEFINITION
struct RESTClient::paramdefs RESTClient::defFnNames[] = {

	// name_eng			name_rus				returns val		args count
	{ { "Get",			"��������" },			TRUE,			1 },
	{ { "Post",			"��������" },			TRUE,			2 },
	{ { "Patch",		"��������" },			TRUE,			2 },
	{ { "Delete",		"�������" },			TRUE,			1 },

	{ { "UseAuth",		"�����������" },		TRUE,			2 },

	{ { NULL,			NULL },					NULL,			NULL }

};

//////////////////////////////////////////////////////////////////////////
//! FIELDS DEFINITION
struct RESTClient::parampropdefs RESTClient::defPropNames[] = {

	// name_eng			name_rus				readable		writeable
	{ { "Version",		"������" },				TRUE,			FALSE },
	{ { "Data",			"������" },				TRUE,			FALSE },
	{ { NULL,			NULL },					NULL,			NULL }

};

//////////////////////////////////////////////////////////////////////////
RESTClient::RESTClient()
{
	Log::DebugFmt("% @\x0F%@\x07 %", "\t-> RESTClient rev.", "1.2", "[12 apr 18]");
	authToken = "";
}

//////////////////////////////////////////////////////////////////////////
RESTClient::~RESTClient()
{
}

struct MemChunk
{
	std::string readBuffer;
	__int64 length = 0;
	bool overflow = false;
};

//////////////////////////////////////////////////////////////////////////
size_t WriteCallback(char *contents, size_t size, size_t nmemb, void *userp)
{

	MemChunk* chunk = ((MemChunk*)userp);
	if (chunk->overflow)
		return size * nmemb;

	__int64 nextLength = chunk->length + size * nmemb;

	if (nextLength >= INT_MAX - 1)
	{
		chunk->overflow = true;
		return size * nmemb;
	}

	chunk->readBuffer.append((char*)contents, size * nmemb);
	return size * nmemb;
}

//////////////////////////////////////////////////////////////////////////
const char* RESTClient::GetImpl(const char* aHttpPath)
{
	CURL* hCurl = curl_easy_init();
 	MemChunk readBuffer;
 
 	curl_easy_setopt(hCurl, CURLOPT_URL, aHttpPath);
 	curl_easy_setopt(hCurl, CURLOPT_WRITEFUNCTION, WriteCallback);
 	curl_easy_setopt(hCurl, CURLOPT_WRITEDATA, &readBuffer);
 
 	curl_slist* headers = nullptr;
 	if (authToken != "")
 	{
 		headers = curl_slist_append(headers, authToken.c_str());
 		curl_easy_setopt(hCurl, CURLOPT_HTTPHEADER, headers);
 	}

	curl_easy_perform(hCurl);

	long http_code = 0;
	curl_easy_getinfo(hCurl, CURLINFO_RESPONSE_CODE, &http_code);
	if (http_code < 200 || http_code > 399)
	{
		curl_easy_cleanup(hCurl);
		std::string errMsg("\0");
		errMsg.append(std::to_string(http_code));
		errMsg.append(" - ��� ������ �������");
		
		if (!readBuffer.overflow)
			data = UTF_Tools::UTF8_To_ASCII(readBuffer.readBuffer);

		curl_easy_cleanup(hCurl);

		CBLModule::RaiseExtRuntimeError(errMsg.c_str(), 0);
		return 0;
	}

	if (readBuffer.overflow)
	{
		curl_easy_cleanup(hCurl);
		CBLModule::RaiseExtRuntimeError("����� ������� ��������� ���������� ������������ ������", 0);
		return 0;
	}

	curl_easy_cleanup(hCurl);

	std::string outBuffer = UTF_Tools::UTF8_To_ASCII(readBuffer.readBuffer);

	const char* returnValue = (const char*)bl_malloc(outBuffer.length()+1);
	memcpy((void*)returnValue, outBuffer.c_str(), outBuffer.length());
	return returnValue;
}

//////////////////////////////////////////////////////////////////////////
const char* RESTClient::PostImpl(const char* httpPath, const char* postBody)
{
	CURL* hCurl = curl_easy_init();

	MemChunk readBuffer;

	curl_easy_setopt(hCurl, CURLOPT_URL, httpPath);
	curl_easy_setopt(hCurl, CURLOPT_POST, 1L);
	std::string utf8Body = UTF_Tools::ASCII_To_UTF8(std::string(postBody));
	curl_easy_setopt(hCurl, CURLOPT_POSTFIELDS, utf8Body.c_str());
	curl_easy_setopt(hCurl, CURLOPT_WRITEFUNCTION, WriteCallback);
	curl_easy_setopt(hCurl, CURLOPT_WRITEDATA, &readBuffer);

	curl_slist* headers = nullptr;
	if (authToken != "")
	{
		headers = curl_slist_append(headers, authToken.c_str());
		curl_easy_setopt(hCurl, CURLOPT_HTTPHEADER, headers);
	}

	curl_easy_perform(hCurl);

	long http_code = 0;
	curl_easy_getinfo(hCurl, CURLINFO_RESPONSE_CODE, &http_code);
	if (http_code < 200 || http_code > 399)
	{
		curl_easy_cleanup(hCurl);
		std::string errMsg("\0");
		errMsg.append(std::to_string(http_code));
		errMsg.append(" - ��� ������ �������");

		if (!readBuffer.overflow)
			data = UTF_Tools::UTF8_To_ASCII(readBuffer.readBuffer);

		curl_easy_cleanup(hCurl);

		CBLModule::RaiseExtRuntimeError(errMsg.c_str(), 0);
		return 0;
	}

	if (readBuffer.overflow)
	{
		curl_easy_cleanup(hCurl);
		CBLModule::RaiseExtRuntimeError("����� ������� ��������� ���������� ������������ ������", 0);
		return 0;
	}

	curl_easy_cleanup(hCurl);

	std::string outBuffer = UTF_Tools::UTF8_To_ASCII(readBuffer.readBuffer);

	const char* returnValue = (const char*)bl_malloc(outBuffer.length()+1);
	memcpy((void*)returnValue, outBuffer.c_str(), outBuffer.length());
	return returnValue;
}

//////////////////////////////////////////////////////////////////////////
const char* RESTClient::PatchImpl(const char* httpPath, const char* patchBody)
{
	CURL* hCurl = curl_easy_init();

	MemChunk readBuffer;

	curl_easy_setopt(hCurl, CURLOPT_URL, httpPath);
	curl_easy_setopt(hCurl, CURLOPT_CUSTOMREQUEST, "PATCH");
	std::string utf8Body = UTF_Tools::ASCII_To_UTF8(std::string(patchBody));
	curl_easy_setopt(hCurl, CURLOPT_POSTFIELDS, utf8Body.c_str());
	curl_easy_setopt(hCurl, CURLOPT_WRITEFUNCTION, WriteCallback);
	curl_easy_setopt(hCurl, CURLOPT_WRITEDATA, &readBuffer);

	curl_slist* headers = nullptr;
	if (authToken != "")
	{
		headers = curl_slist_append(headers, authToken.c_str());
		curl_easy_setopt(hCurl, CURLOPT_HTTPHEADER, headers);
	}

	curl_easy_perform(hCurl);

	long http_code = 0;
	curl_easy_getinfo(hCurl, CURLINFO_RESPONSE_CODE, &http_code);
	if (http_code < 200 || http_code > 399)
	{
		curl_easy_cleanup(hCurl);
		std::string errMsg("\0");
		errMsg.append(std::to_string(http_code));
		errMsg.append(" - ��� ������ �������");

		if (!readBuffer.overflow)
			data = UTF_Tools::UTF8_To_ASCII(readBuffer.readBuffer);

		curl_easy_cleanup(hCurl);

		CBLModule::RaiseExtRuntimeError(errMsg.c_str(), 0);
		return 0;
	}

	if (readBuffer.overflow)
	{
		curl_easy_cleanup(hCurl);
		CBLModule::RaiseExtRuntimeError("����� ������� ��������� ���������� ������������ ������", 0);
		return 0;
	}

	curl_easy_cleanup(hCurl);

	std::string outBuffer = UTF_Tools::UTF8_To_ASCII(readBuffer.readBuffer);

	const char* returnValue = (const char*)bl_malloc(outBuffer.length()+1);
	memcpy((void*)returnValue, outBuffer.c_str(), outBuffer.length());
	return returnValue;
}

//////////////////////////////////////////////////////////////////////////
const char* RESTClient::DeleteImpl(const char* httpPath)
{
	CURL* hCurl = curl_easy_init();

	MemChunk readBuffer;

	curl_easy_setopt(hCurl, CURLOPT_URL, httpPath);
	curl_easy_setopt(hCurl, CURLOPT_CUSTOMREQUEST, "DELETE");
	curl_easy_setopt(hCurl, CURLOPT_WRITEFUNCTION, WriteCallback);
	curl_easy_setopt(hCurl, CURLOPT_WRITEDATA, &readBuffer);

	curl_slist* headers = nullptr;
	if (authToken != "")
	{
		headers = curl_slist_append(headers, authToken.c_str());
		curl_easy_setopt(hCurl, CURLOPT_HTTPHEADER, headers);
	}

	curl_easy_perform(hCurl);

	long http_code = 0;
	curl_easy_getinfo(hCurl, CURLINFO_RESPONSE_CODE, &http_code);
	if (http_code < 200 || http_code > 399)
	{
		curl_easy_cleanup(hCurl);
		std::string errMsg("\0");
		errMsg.append(std::to_string(http_code));
		errMsg.append(" - ��� ������ �������");

		if (!readBuffer.overflow)
			data = UTF_Tools::UTF8_To_ASCII(readBuffer.readBuffer);

		curl_easy_cleanup(hCurl);

		CBLModule::RaiseExtRuntimeError(errMsg.c_str(), 0);
		return 0;
	}

	if (readBuffer.overflow)
	{
		curl_easy_cleanup(hCurl);
		CBLModule::RaiseExtRuntimeError("����� ������� ��������� ���������� ������������ ������", 0);
		return 0;
	}

	curl_easy_cleanup(hCurl);

	std::string outBuffer = UTF_Tools::UTF8_To_ASCII(readBuffer.readBuffer);

	const char* returnValue = (const char*)bl_malloc(outBuffer.length()+1);
	memcpy((void*)returnValue, outBuffer.c_str(), outBuffer.length());
	return returnValue;
}

//////////////////////////////////////////////////////////////////////////
const char* RESTClient::UseAuthImpl(const char* username, const char* password)
{
	authUsername = UTF_Tools::ASCII_To_UTF8(username);
	authPassword = UTF_Tools::ASCII_To_UTF8(password);

	const std::string plainPair = authUsername + ":" + authPassword;
	std::string encodedPair;

	Base64::Encode(plainPair, &encodedPair);

	authToken = "Authorization: Basic " + std::string(encodedPair);

	const char* returnValue = (const char*)bl_malloc(encodedPair.length() + 1);
	memcpy((void*)returnValue, encodedPair.c_str(), encodedPair.length());
	return returnValue;
}
