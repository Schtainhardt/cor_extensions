#include "stdafx.h"
#include "Hive.h"

//////////////////////////////////////////////////////////////////////////
bool Hive::ExecuteRemotely(HiveMessage* msg, std::string& result)
{
	std::string moduleCode = std::string(msg->data, msg->dataLength);
	
	HiveRemodeCodeModule* hrcm = new HiveRemodeCodeModule(this, moduleCode);
	remoteModules.push(hrcm);
	while (!hrcm->IsDone())
		_sleep(125);

	result = hrcm->Result();
	return hrcm->Sucsess();
}

//////////////////////////////////////////////////////////////////////////
bool Hive::DoRemote()
{
	while (!remoteModules.empty())
	{
		remoteModules.front()->Execute();
		remoteModules.pop();
	}

	return true;
}