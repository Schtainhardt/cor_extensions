#pragma once
#include "stdafx.h"
#include "UTF_Tools.h"
#include "Entry.h"

#define BL_JSON_TYPE_ID 0x4A534F4E
#define BL_JSON_NEW_ELEM 0x7FFFFFFF
#define BL_STORED_MARKER 0xB6

class JSONHolder : public CBLContext
{

private:
	nlohmann::json jsonHolder;
	nlohmann::json::iterator jsonIterator = jsonHolder.begin();

	mutable std::string newElementName;

public:

	DECLARE_DYNCREATE(JSONHolder);

	JSONHolder();
	virtual ~JSONHolder();

	void __set_container(const nlohmann::json& cont);
	const nlohmann::json& __get_container() { return jsonHolder; };

	BOOL InitImpl(const char* data);
	BOOL ExistImpl(const char* key);
	long LengthImpl();
	void GetIdxImpl(long idx, CValue& rValue);
	BOOL RemoveImpl(CValue* idx);
	const char* DumpImpl();
	void GetByKeyImpl(const char* idx, CValue& rValue);
	void SetByKeyImpl(const char* idx, CValue* val, CValue& rValue);

	BOOL InitIteratorImpl();
	BOOL FetchImpl();
	const char* IteratorKeyImpl();
	void GetIteratorValueImpl(CValue& rValue);
	void SetIteratorValueImpl(CValue* val, CValue& rValue);

	//////////////////////////////////////////////////////////////////////////
	virtual int CallAsFunc(int iMethNum, class CValue& rValue, class CValue** ppValue)
	{
		switch (iMethNum)
		{
		case mLoad:
			rValue = CValue(InitImpl(ppValue[0]->m_String));
			break;
		case mExist:
			rValue = CValue(ExistImpl(ppValue[0]->m_String));
			break;
		case mLength:
			rValue = CValue(LengthImpl());
			break;
		case mGetIdx:
			GetIdxImpl(ppValue[0]->m_Number, rValue);
			break;
		case mRemove:
			rValue = CValue(RemoveImpl(ppValue[0]));
			break;
		case mDump:
		{
			const char* returnValue = DumpImpl();
			rValue = CValue(returnValue);
			delete[] returnValue;
			break;
		}
		case mGetByKey:
		{
			GetIdxImpl(ppValue[0]->m_Number, rValue);
			break;
		}
		case mSetByKey:
			SetByKeyImpl(ppValue[0]->m_String, ppValue[1], rValue);
			break;
		case mInitIterator:
			rValue = CValue((long)InitIteratorImpl());
			break;
		case mFetch:
			rValue = CValue((long)FetchImpl());
			break;
		case mIterKey:
		{
			const char* returnValue = IteratorKeyImpl();
			rValue = CValue(returnValue);
			delete[] returnValue;
			break;
		}
		case mGetIterVal:
			GetIteratorValueImpl(rValue);
			break;
		case mSetIterVal:
			SetIteratorValueImpl(ppValue[0], rValue);
			break;
		default:
			return 1;
		};

		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int CallAsProc(int iMethNum, class CValue** ppValue)
	{
		switch (iMethNum)
		{
		default:
			return 1;
		};

		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  FindMethod(char const* methodName) const
	{
		int i;

		for (i = 0; i < LastMethod; i++) {
			if (!stricmp(methodName, defFnNames[i].Names[0]))
				return i;
			if (!stricmp(methodName, defFnNames[i].Names[1]))
				return i;
		}

		return -1;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  FindProp(char const* propName) const
	{
		nlohmann::json::const_iterator it = jsonHolder.find(UTF_Tools::ASCII_To_UTF8(propName));
		if (it == jsonHolder.cend())
		{
			newElementName = std::string(UTF_Tools::ASCII_To_UTF8(propName));
			return BL_JSON_NEW_ELEM;
		}

		return std::distance(jsonHolder.cbegin(), it);
	}

	//////////////////////////////////////////////////////////////////////////
	virtual char const* GetCode(void) const
	{
		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int GetDestroyUnRefd(void) const
	{
		return 1;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual class CObjID GetID(void) const
	{
		return CObjID::CObjID();
	}

	//////////////////////////////////////////////////////////////////////////
	virtual char const* GetMethodName(int iMethodNum, int iMethodAlias) const
	{
		if (iMethodNum >= LastMethod)
		{
			return 0;
		}
		else
			return defFnNames[iMethodNum].Names[iMethodAlias];
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int GetNMethods(void) const
	{
		return LastMethod;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int GetNParams(int iMethodNum) const
	{
		if (iMethodNum >= LastMethod)
		{
			return -1;
		}
		else
			return defFnNames[iMethodNum].NumberOfParams;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  GetNProps(void) const
	{
		return jsonHolder.size();
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int GetParamDefValue(int iMethodNum, int iParamNum, class CValue& pDefValue) const
	{
		if (iMethodNum >= LastMethod)
		{
			return 1;
		}

		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual char const* GetPropName(int propNum, int propAlias) const
	{
		auto it = jsonHolder.cbegin();
		std::advance(it, propNum);
		return UTF_Tools::UTF8_To_ASCII(it.key()).c_str();
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int GetPropVal(int propNum, class CValue& rValue) const
	{

		if (propNum == BL_JSON_NEW_ELEM)
		{
			std::string errMsg("������� �������� �������������� ������� <");
			std::string keyName = UTF_Tools::UTF8_To_ASCII(newElementName);
			errMsg += keyName.substr(0, keyName.size() - 2);
			errMsg += ">";

			CBLModule::RaiseExtRuntimeError(errMsg.c_str(), 0);
			rValue = CValue(0l);
			return 0;
		}

		auto it = jsonHolder.cbegin();
		std::advance(it, propNum);

		nlohmann::json::value_t ty = it.value().type();

		switch (ty)
		{
		case nlohmann::json::value_t::array:
		case nlohmann::json::value_t::object:
		{
			JSONHolder* innerContext = (JSONHolder*)CBLContext::CreateInstance("������JSON");
			innerContext->__set_container(it.value());
			rValue.AssignContext(innerContext);
			break;
		}
		case nlohmann::json::value_t::string:
		{
			std::string returnValue(it.value().get<std::string>());
			returnValue = UTF_Tools::UTF8_To_ASCII(returnValue);

			CValue returnStoredVal;
			if (returnValue[0] == (char)BL_STORED_MARKER)
			{
				std::string boxedVal = returnValue.substr(1);
				returnStoredVal.LoadFromString(boxedVal.c_str(), 1);
				rValue = CValue(returnStoredVal);
				break;
			}

			char* retPtr = new char[returnValue.length()];
			memcpy((void*)retPtr, returnValue.c_str(), returnValue.length());
			rValue = CValue(retPtr);
			delete[] retPtr;
			break;
		}
		case nlohmann::json::value_t::boolean:
			rValue = CValue(it.value() == true ? 1 : 0);
			break;
		case nlohmann::json::value_t::number_integer:
		case nlohmann::json::value_t::number_unsigned:
			rValue = CValue((long)it.value());
			break;
		case nlohmann::json::value_t::number_float:
		{
			CNumeric num((long double)it.value().get<long double>());
			rValue = CValue(num);
			break;
		}
		case nlohmann::json::value_t::discarded:
		case nlohmann::json::value_t::null:
			rValue = CValue(0l);
			break;
		default:
			break;
		}

	}

	//////////////////////////////////////////////////////////////////////////
	virtual long GetTypeID(void) const
	{
		return 100; //BL_JSON_TYPE_ID;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual char const* GetTypeString(void) const
	{
		return "������JSON";
	}

	//////////////////////////////////////////////////////////////////////////
	virtual class CType GetValueType(void) const
	{
		return CType(100); //CType(BL_JSON_TYPE_ID);
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  HasRetVal(int iMethodNum) const
	{
		if (iMethodNum >= LastMethod)
		{
			return 0;
		}
		else
			return defFnNames[iMethodNum].HasReturnValue;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  IsExactValue(void) const
	{
		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  IsOleContext(void) const
	{
		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int IsPropReadable(int propNum) const
	{

		if (jsonHolder.size() - 1 < propNum)
			return FALSE;

		return TRUE;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  IsPropWritable(int propNum) const
	{
		return TRUE;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  IsSerializable(void)
	{
		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int SaveToString(CString& retVal)
	{
		retVal = "DJK";
		return 1;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int SetPropVal(int propNum, class CValue const & val)
	{

		if ( (propNum > jsonHolder.size() - 1) && (propNum != BL_JSON_NEW_ELEM))
			return 0;

		// * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		if (val.type == 2)	//string
		{
			if (propNum == BL_JSON_NEW_ELEM)
			{
				jsonHolder[newElementName] = UTF_Tools::ASCII_To_UTF8((const char*)val.m_String);
				newElementName = std::string("\0");
				return 0;
			}
			else 
			{
				auto it = jsonHolder.begin();
				std::advance(it, propNum);
				it.value() = std::string(UTF_Tools::ASCII_To_UTF8((const char*)val.m_String));
				return 0;
			}
		}

		// * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		else if (val.type == 1)	// kind of number
		{
			if (val.m_Number != val.m_Number.GetDouble())	// floating point number
			{
				if (propNum == BL_JSON_NEW_ELEM)
				{
					jsonHolder[newElementName] = val.m_Number.GetDouble();
					newElementName = std::string();
					return 0;
				}
				else
				{
					auto it = jsonHolder.begin();
					std::advance(it, propNum);
					it.value() = val.m_Number.GetDouble();
					return 0;
				}
			}

		// * * * * * * * * * * * * * * * * * * * * * * * * * * * *
			else	// number
			{
				if (propNum == BL_JSON_NEW_ELEM)
				{
					jsonHolder[newElementName] = (long)val.m_Number;
					newElementName = std::string("\0");
					return 0;
				}
				else
				{
					auto it = jsonHolder.begin();
					std::advance(it, propNum);
					it.value() = (long)val.m_Number;
					return 0;
				}
			}
		}
		// * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		else if ((val.type == 100) && (strcmp(val.m_Context->GetTypeString(), "������JSON") == 0))
		{
			JSONHolder* innerHolder = (JSONHolder*)val.m_Context;
			if (propNum == BL_JSON_NEW_ELEM)
			{
				jsonHolder[newElementName] = nlohmann::json(innerHolder->__get_container());
				newElementName = std::string("\0");
				return 0;
			}
			else
			{
				auto it = jsonHolder.begin();
				std::advance(it, propNum);
				it.value() = nlohmann::json(innerHolder->__get_container());
				return 0;
			}
		}
		else
		{
			char serializationBuffer[32768];
			CString serializationString(serializationBuffer, 32768);

			if (propNum == BL_JSON_NEW_ELEM)
			{
				std::string setVal = std::string("\0");
				setVal += (char)BL_STORED_MARKER;

				CValue* valCopy = new CValue(val);
				valCopy->SaveToString(serializationString);
				setVal += std::string((const char*)serializationString);

				jsonHolder[newElementName] = UTF_Tools::ASCII_To_UTF8(setVal);
				newElementName = std::string("\0");
				return 0;
			}
			else
			{
				auto it = jsonHolder.begin();
				std::advance(it, propNum);

				std::string setVal = std::string("\0");
				setVal += (char)BL_STORED_MARKER;

				CValue* valCopy = new CValue(val);
				valCopy->SaveToString(serializationString);
				setVal += std::string((const char*)serializationString);

				it.value() = UTF_Tools::ASCII_To_UTF8(setVal);
				return 0;
			}
		}

		//else
		//{
		//	CBLModule::RaiseExtRuntimeError("������������� �������� �� �������� ������, ������� ��� JSON-��������!", 0);
		//	return 0;
		//}

		Log::ErrorFmt("Unreachable place reached at function %", __FUNCTION__);
		DebugBreak();
		return 0;

	}

	//////////////////////////////////////////////////////////////////////////
	static struct paramdefs {
		char* Names[2];
		int HasReturnValue;
		int NumberOfParams;
	}  defFnNames[];

	//////////////////////////////////////////////////////////////////////////
	static struct parampropdefs {
		char* Names[2];
		BOOL IsReadable;
		BOOL IsWritable;
	}  defPropNames[];

	//////////////////////////////////////////////////////////////////////////
	enum {
		mLoad,
		mExist,
		mLength,
		mGetIdx,
		mRemove,
		mDump,
		mGetByKey,
		mSetByKey,

		mInitIterator,
		mFetch,
		mIterKey,
		mGetIterVal,
		mSetIterVal,

		LastMethod
	};

	//////////////////////////////////////////////////////////////////////////
	enum {
		Version,
		LastProp
	};

};