#include "stdafx.h"
#include "Global_OnLoadSourcePriv.h"

#include "HookTools.h"

//////////////////////////////////////////////////////////////////////////
// ::OnLoadSourcePriv

Hook_OnLoadSourcePriv::OnLoadSourcePrivFunc Hook_OnLoadSourcePriv::orig_OnLoadSourcePriv;


//////////////////////////////////////////////////////////////////////////
Hook_OnLoadSourcePriv::Hook_OnLoadSourcePriv()
{
	HOOK_MESSAGE;

	HANDLE processHandle = GetCurrentProcess();
	HMODULE moduleHandle = GetModuleHandleA(module_name);
	OnLoadSourcePrivFunc registrar = (OnLoadSourcePrivFunc)GetProcAddress(moduleHandle, mangled_func);

	orig_OnLoadSourcePriv = (OnLoadSourcePrivFunc)HookFunctionCdecl<7>(processHandle, registrar, hk_OnLoadSourcePriv);
}


//////////////////////////////////////////////////////////////////////////
int __cdecl Hook_OnLoadSourcePriv::hk_OnLoadSourcePriv(CString str1, CString& str2)
{
	Log::DebugFmt("OnLoadSourcePriv:\n\n @\x0b%@\x07\n@\x0b%@\x07\n", (LPCSTR)str1, (LPCSTR)str2);
	return orig_OnLoadSourcePriv(str1, str2);
}