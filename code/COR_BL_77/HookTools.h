#pragma once
#include <map>

//////////////////////////////////////////////////////////////////////////
// CDECL HOOK
template <int offset>
LPVOID HookFunctionCdecl(HANDLE CurrentProcess, PVOID TargetFunction, PVOID pHookFunction)
{
	DWORD OldProtection = 0;
	if (!VirtualProtectEx(CurrentProcess, TargetFunction, offset, PAGE_EXECUTE_READWRITE, &OldProtection)) return 0;

	char* DumpAddress = ((char*)TargetFunction);
	char* OriginalFuncAddress = ((char*)TargetFunction) + offset;

	char InstructionDump[offset];
	ZeroMemory(InstructionDump, sizeof(InstructionDump));
	memcpy(InstructionDump, DumpAddress, offset);

	LPVOID ReroutedFuncPtr = VirtualAlloc(NULL, offset+4, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	if (ReroutedFuncPtr == nullptr) return 0;
	ZeroMemory(ReroutedFuncPtr, offset+4);

	DWORD FinalAddress = (DWORD)pHookFunction - (DWORD)TargetFunction - 5;

	*DumpAddress++ = 0xE9;		// jmp
	*((DWORD*)DumpAddress) = (DWORD)FinalAddress;
	DumpAddress += 4;
	for (size_t i = 0; i < offset - 5; i++)
	{
		*DumpAddress++ = (UCHAR)0x90;
	}

	MemoryBarrier();

	UCHAR* pInstr = (UCHAR*)ReroutedFuncPtr;
	memcpy(pInstr, InstructionDump, offset);
	pInstr += offset;
	*pInstr++ = 0xE9;		// jmp

	*((DWORD*)pInstr) = (DWORD)OriginalFuncAddress - (DWORD)pInstr - sizeof(UCHAR) - sizeof(void*);

	return ReroutedFuncPtr;
}

//////////////////////////////////////////////////////////////////////////
// THISCALL HOOK
template <int skip, int offset>
LPVOID HookFunctionThiscall(HANDLE CurrentProcess, PVOID TargetFunction, PVOID pHookFunction, void** outOriginalAbsolute = nullptr)
{

	// rewinding
	char* StartPointer = (char*)TargetFunction + skip;

	// set r/w
	DWORD OldProtection = 0;
	if (!VirtualProtectEx(CurrentProcess, StartPointer, offset, PAGE_EXECUTE_READWRITE, &OldProtection)) return 0;

	// dump target call
	char* DumpAddress = ((char*)StartPointer);

	DWORD* OriginalCallAddress = (DWORD*)malloc(sizeof(DWORD));
	ZeroMemory(OriginalCallAddress, sizeof(DWORD));
	memcpy(OriginalCallAddress, DumpAddress+sizeof(UCHAR), sizeof(void*));

	// 29 + align
	const int shellCodeSize = 32;

	// alloc detour
	LPVOID ReroutedFuncPtr = VirtualAlloc(NULL, offset + shellCodeSize, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	if (ReroutedFuncPtr == nullptr) return 0;
	ZeroMemory(ReroutedFuncPtr, offset + shellCodeSize);

	// make writer
	char* DetourFuncWriter = (char*)ReroutedFuncPtr;

	// calc target function offset
	DWORD FinalAddress = (DWORD)pHookFunction - (DWORD)DetourFuncWriter - sizeof(void*) - 5*sizeof(UCHAR);

	// Detour asm start >>

	//#WARNING: `eax` discarded, may cause bugs!

	// eax pushed in original, cleaning the stack:
	*DetourFuncWriter++			= (UCHAR)0x58;				// pop eax

	// Now stack have no arguments in it, so dump rollback data into it:
	// __thiscall means that this-pointer stored in ecx so make backup:
	*DetourFuncWriter++			= (UCHAR)0x51;				// push ecx

	// eax for our __stdcall hook:
	*DetourFuncWriter++			= (UCHAR)0x50;				// push eax

	// another one if we should call original function
	*DetourFuncWriter++			= (UCHAR)0x50;				// push eax

	// call hook	(pops one of two pushed eax values):
	*DetourFuncWriter++			= (UCHAR)0xE8;				// call ...
	*((DWORD*)DetourFuncWriter)	= (DWORD)FinalAddress;		// ...  dword ptr [pHookFunction]
	DetourFuncWriter += 4;						// rewind

	// test hook result to 0x01h if we need to call original function
	*DetourFuncWriter++ = (UCHAR)0x85;				// test ...
	*DetourFuncWriter++ = (UCHAR)0xC0;				// ...  eax, eax

	// to restore ecx we should pop eax first	(safe, ZF is set):
	*DetourFuncWriter++ = (UCHAR)0x58;				// pop eax
	*DetourFuncWriter++ = (UCHAR)0x59;				// pop ecx

	// push arg to call original function:
	*DetourFuncWriter++ = (UCHAR)0x50;				// push eax

	// recalc original target offset
	DWORD TargetOffset = (DWORD)*OriginalCallAddress + (DWORD)StartPointer - (DWORD)DetourFuncWriter - 2*sizeof(UCHAR);

	// recalc abs
	DWORD AbsoluteOriginal = (DWORD)*OriginalCallAddress + (DWORD)StartPointer + sizeof(void*) + sizeof(UCHAR);

	// jump over call if eax==0		(original call not needed):
	*DetourFuncWriter++ = (UCHAR)0x74;			// jz ...
	*DetourFuncWriter++ = (UCHAR)0x07;			// $+7

	// call target
	*DetourFuncWriter++			= (UCHAR)0xE8;				// call ...
	*((DWORD*)DetourFuncWriter) = (DWORD)TargetOffset;			// ...  dword ptr [newOriginalOffset]
	DetourFuncWriter += 4;		// rewind

	// called function pops just one arg from stack so skip stack cleaning
	*DetourFuncWriter++ = (UCHAR)0xEB;			// jmp ...
	*DetourFuncWriter++ = (UCHAR)0x01;			// $+1		(near jump, not E9)

	// clean the stack if original code wasn't called
	*DetourFuncWriter++ = (UCHAR)0x58;				// pop eax

	// calc return to original code
	DWORD ReturnAddress = (DWORD)TargetFunction + skip - (DWORD)DetourFuncWriter;

	// jump back
	*DetourFuncWriter++ = (UCHAR)0xE9;	// jmp ...
	*((DWORD*)DetourFuncWriter) = (DWORD)ReturnAddress;			// ...  dword ptr [returnAddres]
	DetourFuncWriter += 4;		// rewind

	// << Detour asm end

	// Do detour

	DWORD RerouteOffset = 0-((DWORD)StartPointer + sizeof(void*) + sizeof(UCHAR) - (DWORD)ReroutedFuncPtr);

	*StartPointer++ = 0xE9;		// jmp ...
	*((DWORD*)StartPointer) = (DWORD)RerouteOffset;			// ...  dword ptr [ReroutedFuncPtr]
	StartPointer += 4; //rewind

	// Get abs:
	if (outOriginalAbsolute != nullptr)
	{
		*outOriginalAbsolute = (void*)AbsoluteOriginal;
	}

	return ReroutedFuncPtr;
}

//////////////////////////////////////////////////////////////////////////
// BRUTE FORCE EPILOG HOOK
//							use with caution
//							MUST inject with 9 bytes pad
template <int skip, int offset>
LPVOID HookFunctionEpilog(HANDLE CurrentProcess, PVOID TargetFunction, PVOID pHookFunction)
{
	// rewinding
	char* StartPointer = (char*)TargetFunction + skip;

	// set r/w
	DWORD OldProtection = 0;
	if (!VirtualProtectEx(CurrentProcess, StartPointer, offset, PAGE_EXECUTE_READWRITE, &OldProtection)) return 0;

	char* originalEpilog = (char*)malloc(offset);
	ZeroMemory(originalEpilog, offset);
	memcpy(originalEpilog, StartPointer, offset);

	for (size_t i = 0; i < offset; i++)
	{
		*StartPointer++ = 0x90;
	}

	StartPointer -= offset;

	*StartPointer++ = 0x60;		// push eax
	*StartPointer++ = 0xB8;		// mov eax
	*((DWORD*)StartPointer) = (DWORD)pHookFunction;	// ...  dword ptr [ReroutedFuncPtr]
	StartPointer += 4; //rewind
	*StartPointer++ = 0xFF;		// far call
	*StartPointer++ = 0xD0;		// eax
	*StartPointer++ = 0x61;		// pop eax

	memcpy(StartPointer, originalEpilog, offset);

	return TargetFunction;

}

//////////////////////////////////////////////////////////////////////////
// DIRECT HOOK
//			rewrites memory per se
template <int skip>
LPVOID HookFunctionDirectInject(HANDLE CurrentProcess, PVOID TargetFunction, unsigned int ShellcodeSize, void* pShellCode)
{
	// rewinding
	char* StartPointer = (char*)TargetFunction + skip;

	// set r/w
	DWORD OldProtection = 0;
	if (!VirtualProtectEx(CurrentProcess, StartPointer, ShellcodeSize, PAGE_EXECUTE_READWRITE, &OldProtection)) return 0;

	char* originalCode = (char*)malloc(ShellcodeSize);
	ZeroMemory(originalCode, ShellcodeSize);
	memcpy(originalCode, StartPointer, ShellcodeSize);

	memcpy(StartPointer, pShellCode, ShellcodeSize);

	return originalCode;

}

//////////////////////////////////////////////////////////////////////////
class HookExplorer
{
private:

	struct FunctionDefinition
	{
		void* pointer;
		const char* moduleName;
	};

	std::map<const char*, FunctionDefinition> vtable;

	int Rebuild(const char* moduleName, const char* functions[]);
public:
	HookExplorer();

	void RebuildVirtualTable();
	void DumpVirtualTable();
};

//////////////////////////////////////////////////////////////////////////
#define HOOK_MESSAGE Log::DebugFmt("Hook: @\x0b%@\x07", __FUNCTION__)