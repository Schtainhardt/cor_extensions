#include "stdafx.h"
#include "CBLContext_RegisterContextClass.h"

#include "Entry.h"
#include "HookTools.h"

//////////////////////////////////////////////////////////////////////////
// CBLContext::RegisterContextClass
Hook_RegisterContextClass::RegisterContextClassFunc Hook_RegisterContextClass::orig_RegisterContextClass;

Hook_RegisterContextClass::Hook_RegisterContextClass()
{

	HOOK_MESSAGE;

	HANDLE processHandle = GetCurrentProcess();
	HMODULE moduleHandle = GetModuleHandleA(module_name);
	RegisterContextClassFunc registrar = (RegisterContextClassFunc)GetProcAddress(moduleHandle, mangled_func);

	orig_RegisterContextClass = (RegisterContextClassFunc)HookFunctionCdecl<7>(processHandle, registrar, hk_RegisterContextClass);
}

void __cdecl Hook_RegisterContextClass::hk_RegisterContextClass(CRuntimeClass* pClass, char const* className, CType const &typeRef)
{
	Log::DebugFmt("Registering class @\x02{$}@\x07\t @\x0b % @\x07", pClass, className);
	orig_RegisterContextClass(pClass, className, typeRef);
}