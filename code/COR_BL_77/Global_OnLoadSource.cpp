#include "stdafx.h"
#include "Global_OnLoadSource.h"

#include "HookTools.h"

//////////////////////////////////////////////////////////////////////////
// ::OnLoadSource
Hook_OnLoadSource::OnLoadSourceFunc Hook_OnLoadSource::orig_OnLoadSource;

//////////////////////////////////////////////////////////////////////////
Hook_OnLoadSource::Hook_OnLoadSource()
{
	HOOK_MESSAGE;

	HANDLE processHandle = GetCurrentProcess();
	HMODULE moduleHandle = GetModuleHandleA(module_name);
	OnLoadSourceFunc registrar = (OnLoadSourceFunc)GetProcAddress(moduleHandle, mangled_func);

	orig_OnLoadSource = (OnLoadSourceFunc)HookFunctionCdecl<7>(processHandle, registrar, hk_OnLoadSource);
}

//////////////////////////////////////////////////////////////////////////
int __cdecl Hook_OnLoadSource::hk_OnLoadSource(CString str1)
{
	Log::DebugFmt("OnLoadSource:\n\n @\x0b%@\x07\n", (LPCSTR)str1);
	return orig_OnLoadSource(str1);
}

