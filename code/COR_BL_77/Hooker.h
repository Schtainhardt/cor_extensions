#pragma once
#include "stdafx.h"

#include "CBLContext_RegisterContextClass.h"
#include "CBLModule_LoadSource.h"
#include "CBLModule_AssignSource.h"
#include "Global_GetGlobalModule.h"
#include "CBLModule_Execute.h"
#include "CBLModule_Compile.h"
#include "CBLContext_CBLContext.h"
#include "CBLContext_CBLContext_destructor.h"
#include "CBLModule_EvalFunc.h"
#include "CBLModule_BatchExec.h"
#include "CApp_ctor.h"
#include "CApp_ProcessAddInEvents.h"
#include "CDBEngDDB7Service_ctor.h"

//#include <minhook/MinHook.h>

//////////////////////////////////////////////////////////////////////////
class Hooker
{
public:
	Hooker();
	~Hooker();

private:
	
	Hook_RegisterContextClass* hkr_RegisterContextClass = nullptr;
	Hook_LoadSource* hkr_LoadSource = nullptr;
	Hook_AssignSource* hkr_AssignSource = nullptr;
	Hook_GetGlobalModule* hkr_GetGlobalModule = nullptr;
	Hook_Execute* hkr_Execute = nullptr;
	Hook_Compile* hkr_Compile = nullptr;
	Hook_CBLContext* hkr_CBLContext = nullptr;
	Hook_CBLContext_d* hkr_CBLContext_d = nullptr;
	Hook_EvalExpr* hkr_EvalExpr = nullptr;
	Hook_ExecuteBatch* hkr_ExecuteBatch = nullptr;
	Hook_CApp* hkr_CApp = nullptr;
	Hook_ProcessAddInEvents* hkr_ProcessAddInEvents = nullptr;

public:
	Hook_GetGlobalModule* Get_GetGlobalModule() { return hkr_GetGlobalModule; }

};