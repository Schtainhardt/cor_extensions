#pragma once
#include "stdafx.h"

class RESTClient : public CBLContext
{
private:
	std::string authUsername;
	std::string authPassword;

	std::string authToken;

	std::string data;

public:

	DECLARE_DYNCREATE(RESTClient);

	RESTClient();
	virtual ~RESTClient();

	const char* GetImpl(const char* httpPath = nullptr);
	const char* PostImpl(const char* httpPath, const char* postBody = nullptr);
	const char* PatchImpl(const char* httpPath, const char* patchBody = nullptr);
	const char* DeleteImpl(const char* httpPath);

	const char* UseAuthImpl(const char* username, const char* password);

	//////////////////////////////////////////////////////////////////////////
	virtual int CallAsFunc(int iMethNum, class CValue& rValue, class CValue** ppValue)
	{
		switch (iMethNum)
		{
		case mGet:
		{
			const char* returnValue = GetImpl(ppValue[0]->m_String);
			rValue = CValue(returnValue);
			bl_free(returnValue);
			break;
		}
		case mPost:
		{
			const char* returnValue = PostImpl(ppValue[0]->m_String, ppValue[1]->m_String);
			rValue = CValue(returnValue);
			bl_free(returnValue);
			break;
		}
		case mPatch:
		{
			const char* returnValue = PatchImpl(ppValue[0]->m_String, ppValue[1]->m_String);
			rValue = CValue(returnValue);
			bl_free(returnValue);
			break;
		}
		case mDelete:
		{
			const char* returnValue = DeleteImpl(ppValue[0]->m_String);
			rValue = CValue(returnValue);
			bl_free(returnValue);
			break;
		}
		case mUseAuth:
		{
			const char* returnValue = UseAuthImpl(ppValue[0]->m_String, ppValue[1]->m_String);
			rValue = CValue(returnValue);
			bl_free(returnValue);
			break;
		}
		default:
			return 1;
		};

		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int CallAsProc(int iMethNum, class CValue** ppValue)
	{
		switch (iMethNum)
		{

		default:
			return 1;
		};

		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  FindMethod(char const* methodName) const
	{
		int i;

		for (i = 0; i < LastMethod; i++) {
			if (!stricmp(methodName, defFnNames[i].Names[0]))
				return i;
			if (!stricmp(methodName, defFnNames[i].Names[1]))
				return i;
		}

		return -1;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  FindProp(char const* propName) const
	{
		int i;

		for (i = 0; i < LastProp; i++) {
			if (!stricmp(propName, defPropNames[i].Names[0]))
				return i;
			if (!stricmp(propName, defPropNames[i].Names[1]))
				return i;
		}

		return -1;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual char const* GetCode(void) const
	{
		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int GetDestroyUnRefd(void) const
	{
		return 1;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual class CObjID GetID(void) const
	{
		return CObjID::CObjID();
	}

	//////////////////////////////////////////////////////////////////////////
	virtual char const* GetMethodName(int iMethodNum, int iMethodAlias) const
	{
		if (iMethodNum >= LastMethod)
		{
			return 0;
		}
		else
			return defFnNames[iMethodNum].Names[iMethodAlias];
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int GetNMethods(void) const
	{
		return LastMethod;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int GetNParams(int iMethodNum) const
	{
		if (iMethodNum >= LastMethod)
		{
			return -1;
		}
		else
			return defFnNames[iMethodNum].NumberOfParams;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  GetNProps(void) const
	{
		return LastProp;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int GetParamDefValue(int iMethodNum, int iParamNum, class CValue* pDefValue) const
	{
		if (iMethodNum >= LastMethod)
		{
			return 1;
		}

		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual char const* GetPropName(int propNum, int propAlias) const
	{
		if (propNum >= LastProp)
		{
			return 0;
		}
		else
			return defFnNames[propNum].Names[propAlias];
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int GetPropVal(int propNum, class CValue& rValue) const
	{

		if (defPropNames[propNum].IsReadable == FALSE)
			return 1;

		switch (propNum)
		{
		case Version:
			rValue = CValue("1.0.0");
			return 0;
		case pData:
			rValue = CValue(data.c_str());
			return 0;
		default:
			break;
		}

		return 1;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual long GetTypeID(void) const
	{
		return 100;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual char const* GetTypeString(void) const
	{
		return "���������";
	}

	//////////////////////////////////////////////////////////////////////////
	virtual class CType GetValueType(void) const
	{
		return CType(100);
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  HasRetVal(int iMethodNum) const
	{
		if (iMethodNum >= LastMethod)
		{
			return 0;
		}
		else
			return defFnNames[iMethodNum].HasReturnValue;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  IsExactValue(void) const
	{
		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  IsOleContext(void) const
	{
		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int IsPropReadable(int propNum) const
	{
		return defPropNames[propNum].IsReadable;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  IsPropWritable(int propNum) const
	{
		return defPropNames[propNum].IsWritable;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  IsSerializable(void)
	{
		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int SaveToString(CString& retVal)
	{
		retVal = "DJK";
		return 1;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int SetPropVal(int propNum, class CValue const & val)
	{
		if (defPropNames[propNum].IsWritable == FALSE)
			return 1;

		switch (propNum)
		{
		case Version:
			//imposible
			break;
		default:
			break;
		}

		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	static struct paramdefs {
		char* Names[2];
		int HasReturnValue;
		int NumberOfParams;
	}  defFnNames[];

	//////////////////////////////////////////////////////////////////////////
	static struct parampropdefs {
		char* Names[2];
		BOOL IsReadable;
		BOOL IsWritable;
	}  defPropNames[];

	//////////////////////////////////////////////////////////////////////////
	enum {
		mGet,
		mPost,
		mPatch,
		mDelete,

		mUseAuth,

		LastMethod
	};

	//////////////////////////////////////////////////////////////////////////
	enum {
		Version,
		pData,

		LastProp
	};

};