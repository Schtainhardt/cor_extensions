#pragma once
#include "stdafx.h"

//////////////////////////////////////////////////////////////////////////
// CBLContext::RegisterContextClass
class Hook_RegisterContextClass
{
public:
	const char* module_name = "bkend.dll";
	const char* mangled_func = "?RegisterContextClass@CBLContext@@SAXPAUCRuntimeClass@@PBDABVCType@@@Z";

	Hook_RegisterContextClass();

private:
	
	typedef void(__cdecl *RegisterContextClassFunc)(CRuntimeClass* pClass, char const* className, CType const &typeRef);
	static RegisterContextClassFunc orig_RegisterContextClass;
	static void __cdecl hk_RegisterContextClass(CRuntimeClass* pClass, char const* className, CType const &typeRef);


};