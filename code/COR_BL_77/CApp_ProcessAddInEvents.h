#pragma once
#include "stdafx.h"

//////////////////////////////////////////////////////////////////////////
// CApp:: -> ProcessAddInEvents
class Hook_ProcessAddInEvents
{
public:
	const char* module_name = "seven.dll";
	const char* mangled_func = "?OnIdle@CApp7@@UAEHJ@Z";

	Hook_ProcessAddInEvents();
	~Hook_ProcessAddInEvents();

private:

	static void __stdcall WriteAppPointerFunc();

};