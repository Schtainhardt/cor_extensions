#pragma once
#include "stdafx.h"
#include "Hooker.h"
#include "Registrator.h"
#include "AdditionalMenuHandler.h"
#include "SingletonTemplate.h"
#include "Hive.h"

enum AppMode
{
	ENTERPRISE,
	DESIGNER,
	UNKNOWN
};

class CCORExtensions : public Singleton<CCORExtensions>
{
private:
	Hooker* hookingEngine = nullptr;
	Registrator* registrar = nullptr;

	CCORExtensionsApp* theApp = nullptr;

	AppMode appMode = AppMode::UNKNOWN;

public:
	CCORExtensions();
	~CCORExtensions();

	void AfterInit();

	static bool LauncherPresent;

	static CApp7* MainApp;
	static CDBEngDDB7Service* DBService;
	static bool InitDone;

	Hive* hive = nullptr;

	HHOOK idleHookHandle = 0;

	AppMode GetAppMode() { return appMode; };
};

REGISTER_SINGLETON(CCORExtensions);
#define COR CCORExtensions::GetInstance()