#include "stdafx.h"
#include "CBLModule_EvalFunc.h"

#include "HookTools.h"

//////////////////////////////////////////////////////////////////////////
// CBLModule:: -> EvalExpr
Hook_EvalExpr::EvalExprFunc Hook_EvalExpr::orig_EvalExpr;
void* Hook_EvalExpr::EvalExprInternalCall;

Hook_EvalExpr::Hook_EvalExpr()
{
	HOOK_MESSAGE;

	HANDLE processHandle = GetCurrentProcess();
	HMODULE moduleHandle = GetModuleHandleA(module_name);
	EvalExprFunc registrar = (EvalExprFunc)GetProcAddress(moduleHandle, mangled_func);

	DWORD OldProtection = 0;
	if (!VirtualProtectEx(processHandle, (LPVOID)registrar, 0x2A, PAGE_EXECUTE_READWRITE, &OldProtection))
		Log::Error("Failed");

	char* writer = (char*)registrar + 0x21;
	*writer++ = 0xE8;
	*((DWORD*)writer) = (DWORD)hk_EvalExpr - (DWORD)writer - 4;

}

int __stdcall Hook_EvalExpr::hk_EvalExpr(const char* code, class CValue& retVal, class CValue** args)
{
	CBLModule7* self;
	__asm
	{
		mov self, ecx
	}

	return self->EvalExpr(code, retVal, args);
}