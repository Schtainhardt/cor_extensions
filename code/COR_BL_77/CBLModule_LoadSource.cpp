#include "stdafx.h"
#include "CBLModule_LoadSource.h"

#include "HookTools.h"


//////////////////////////////////////////////////////////////////////////
// CBLModule:: ->LoadSource

Hook_LoadSource::LoadSourceFunc Hook_LoadSource::orig_LoadSource;

Hook_LoadSource::Hook_LoadSource()
{
	HOOK_MESSAGE;

	HANDLE processHandle = GetCurrentProcess();
	HMODULE moduleHandle = GetModuleHandleA(module_name);
	LoadSourceFunc registrar = (LoadSourceFunc)GetProcAddress(moduleHandle, mangled_func);

	orig_LoadSource = (LoadSourceFunc)HookFunctionThiscall<8, 5>(processHandle, registrar, hk_LoadSource);

}

int __stdcall Hook_LoadSource::hk_LoadSource(const char* sourceCode)
{
	CBLModule* thisModule = nullptr;
	// __thiscall hack
	__asm
	{
		mov thisModule, ecx
	}

	Log::DebugFmt("Trying to load module @\x0b%@\x07", sourceCode);
	return 1;
}