#pragma once
#include "stdafx.h"

void* VirtualRealloc(void* lpAddress, unsigned long dwSize, unsigned long flProtect);

#ifdef EXPORT_MEMORY_MANAGER
extern "C" __declspec(dllexport) void* BL_Alloc(size_t memSize);
extern "C" __declspec(dllexport) void* BL_Realloc(void* ptr, size_t memSize);
extern "C" __declspec(dllexport) void BL_Free(void* ptr);
#else
void* BL_Alloc(size_t memSize);
void* BL_Realloc(void* ptr, size_t memSize);
void BL_Free(void* ptr);
#endif // EXPORT_MEMORY_MANAGER

#define bl_malloc(x) BL_Alloc(x)
#define bl_free(x) BL_Free((void*)x)

#define bl_new(x) (x*)BL_Alloc(sizeof(x))
#define bl_delete(x) BL_Free(x)

// I dunno why it works on debug O_o
#ifdef BSL_DEBUG
inline void* operator new (size_t size) { return BL_Alloc(size); }
inline void* operator new[](size_t size) { return BL_Alloc(size); }
inline void  operator delete  (void* ptr) { BL_Free(ptr); }
inline void  operator delete[](void* ptr) { BL_Free(ptr); }
#endif // BSL_DEBUG
