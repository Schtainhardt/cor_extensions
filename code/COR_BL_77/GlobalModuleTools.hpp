#pragma once
#include "stdafx.h"

#define IMPORT_1C __declspec(dllimport)

#include "type32.h"
#include "bkend.h"

#include "Log.h"

//////////////////////////////////////////////////////////////////////////
class GlobalModuleTools : public CBLContext
{
private:
	static CBLContext* ExtensionContext;
	static CBLModule7* ExtensionModule;

	int SwapPlainFile(const char* filePath);
	int SwapCryptedFile(const char* filePath);
	int SwapPlainText(const char* sourceCode);
	int SwapCryptedText(const char* sourceCode);

public:

	enum SwapMode
	{
		PLAIN_FILE,
		CRYPTED_FILE,
		PLAIN_TEXT,
		CRYPTED_TEXT,

		LAST_VAL
	};

	int SwapImpl(const char* sourceCode, SwapMode mode);
	int RevertImpl();

	DECLARE_DYNCREATE(GlobalModuleTools);

	GlobalModuleTools()
	{
		Log::DebugFmt("% @\x0F%@\x07 %", "\t-> Global Module Tools rev.", "1.0", "[22 sep 17]");
	};

	virtual ~GlobalModuleTools()
	{
	};


	//////////////////////////////////////////////////////////////////////////
	virtual int CallAsFunc(int iMethNum, class CValue& rValue, class CValue** ppValue)
	{

		const char* sourceCode;
		SwapMode mode;

		switch (iMethNum)
		{
		case methSwap:
			sourceCode = ppValue[0]->m_String;
			mode = (SwapMode)(long)ppValue[1]->m_Number;
			rValue = CValue(SwapImpl(sourceCode, mode));
			break;
		case methRevert:
			rValue = CValue(RevertImpl());
			break;
		default:
			return S_FALSE;
		};

		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int CallAsProc(int iMethNum, class CValue** ppValue)
	{
		const char* sourceCode;
		SwapMode mode;

		switch (iMethNum)
		{
		case methSwap:
			sourceCode = ppValue[0]->m_String;
			mode = (SwapMode)(long)ppValue[1]->m_Number;
			SwapImpl(sourceCode, mode);
			break;
		case methRevert:
			RevertImpl();
			break;
		default:
			return S_FALSE;
		};

		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int FindMethod(char const* methodName) const
	{
		int i;

		for (i = 0; i < LastMethod; i++) {
			if (!stricmp(methodName, defFnNames[i].Names[0]))
				return i;
			if (!stricmp(methodName, defFnNames[i].Names[1]))
				return i;
		}

		return -1;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  FindProp(char const* propName) const
	{
		int i;

		for (i = 0; i < LastProp; i++) {
			if (!stricmp(propName, defPropNames[i].Names[0]))
				return i;
			if (!stricmp(propName, defPropNames[i].Names[1]))
				return i;
		}

		return -1;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual char const* GetCode(void) const
	{
		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int GetDestroyUnRefd(void) const
	{
		return 1;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual class CObjID GetID(void) const
	{
		return CObjID::CObjID();
	}

	//////////////////////////////////////////////////////////////////////////
	virtual char const* GetMethodName(int iMethodNum, int iMethodAlias) const
	{
		if (iMethodNum >= LastMethod)
		{
			return 0;
		}
		else
			return defFnNames[iMethodNum].Names[iMethodAlias];
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int GetNMethods(void) const
	{
		return LastMethod;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int GetNParams(int iMethodNum) const
	{
		if (iMethodNum >= LastMethod)
		{
			return -1;
		}
		else
			return defFnNames[iMethodNum].NumberOfParams;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  GetNProps(void) const
	{
		return LastProp;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int GetParamDefValue(int iMethodNum, int iParamNum, class CValue* pDefValue) const
	{
		if (iMethodNum >= LastMethod)
		{
			return 1;
		}

		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual char const* GetPropName(int propNum, int propAlias) const
	{
		if (propNum >= LastProp)
		{
			return 0;
		}
		else
			return defFnNames[propNum].Names[propAlias];
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int GetPropVal(int propNum, class CValue& rValue) const
	{

		if (defPropNames[propNum].IsReadable == FALSE)
			return 1;

		switch (propNum)
		{
		case propVer:
			rValue = CValue("1.0.0");
			return 0;
		default:
			break;
		}

		return 1;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual long GetTypeID(void) const
	{
		return 100;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual char const* GetTypeString(void) const
	{
		return "����������������������������";
	}

	//////////////////////////////////////////////////////////////////////////
	virtual class CType GetValueType(void) const
	{
		return CType(100);
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  HasRetVal(int iMethodNum) const
	{
		if (iMethodNum >= LastMethod)
		{
			return 0;
		}
		else
			return defFnNames[iMethodNum].HasReturnValue;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  IsExactValue(void) const
	{
		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  IsOleContext(void) const
	{
		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int IsPropReadable(int propNum) const
	{
		return defPropNames[propNum].IsReadable;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  IsPropWritable(int propNum) const
	{
		return defPropNames[propNum].IsWritable;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  IsSerializable(void)
	{
		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int SaveToString(CString& retVal)
	{
		retVal = "DJK";
		return 1;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int SetPropVal(int propNum, class CValue const & val)
	{
		if (defPropNames[propNum].IsWritable == FALSE)
			return 1;

		switch (propNum)
		{
		case propVer:
			//imposible
			break;
		default:
			break;
		}
	}

	static struct paramdefs {
		char* Names[2];
		int HasReturnValue;
		int NumberOfParams;
	}  defFnNames[];

	static struct parampropdefs {
		char* Names[2];
		BOOL IsReadable;
		BOOL IsWritable;
	}  defPropNames[];

	enum {
		methSwap,
		methRevert,
		// def:
		LastMethod
	};

	enum {
		propVer,
		LastProp
	};

};