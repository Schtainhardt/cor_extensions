#include "stdafx.h"
#include "CApp_ProcessAddInEvents.h"

#include "HookTools.h"

#include "Entry.h"

//////////////////////////////////////////////////////////////////////////
// CApp:: -> ProcessAddInEvents

Hook_ProcessAddInEvents::Hook_ProcessAddInEvents()
{
	HOOK_MESSAGE;

	HANDLE processHandle = GetCurrentProcess();
	HMODULE moduleHandle = GetModuleHandleA(module_name);
	LPVOID registrar = GetProcAddress(moduleHandle, mangled_func);

	DWORD OldProtection = 0;
	if (!VirtualProtectEx(processHandle, (LPVOID)registrar, 0x016F, PAGE_EXECUTE_READWRITE, &OldProtection))
		Log::Error("Failed");

	char* writer = (char*)registrar +0x016B;
	*((DWORD*)writer) = (DWORD)WriteAppPointerFunc - (DWORD)writer - 4;
}

Hook_ProcessAddInEvents::~Hook_ProcessAddInEvents()
{

}

void __stdcall Hook_ProcessAddInEvents::WriteAppPointerFunc()
{
	CApp7* ca;
	__asm
	{
		mov ca, ecx
	}

	if (CCORExtensions::MainApp == nullptr && !CCORExtensions::InitDone)
	{
		CCORExtensions::MainApp = ca;
		COR.AfterInit();
	}

	ca->ProcessAddinEvents();
}