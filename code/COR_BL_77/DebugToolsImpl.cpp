#include "stdafx.h"
#include "DebugTools.hpp"

#include "Global_GetGlobalModule.h"

#include <fstream>
#include <sstream>
#include "UTF_Tools.h"
#include "JSONHolder.hpp"

//#include "ContextsManager.h"
//#include "Networking.h"

IMPLEMENT_DYNCREATE(BLDebugTools, CBLContext)

//////////////////////////////////////////////////////////////////////////
//! METHODS DEFINITION
struct BLDebugTools::paramdefs BLDebugTools::defFnNames[] = {

	// name_eng			name_rus				returns val		args count
	{ { "LogDebug",		"����������" },			FALSE,			1 },
	{ { "LogDebugFmt",	"����������������" },	FALSE,			1 },
	{ { "LogError",		"���������" },			FALSE,			1 },
	{ { "LogErrorFmt",		"���������������" },FALSE,			1 },
	{ { "LogWarning",	"�����������������" },	FALSE,			1 },
	{ { "LogWarningFmt",	"�����������������������" },FALSE,	1 },
	{ { "LogInfo",		"�������" },			FALSE,			1 },
	{ { "LogInfoFmt",		"�������������" },	FALSE,			1 },
	
	{ { "DumpVT",		"������" },				FALSE,			0 },

	{ { "TestContexts",	"������������������" },	FALSE,			1 },
	
	{ { "TestMutex",	"����������������" },	TRUE,			0 },

	{ { "CompileFile",	"�����������" },		TRUE,			1 },

	{ { NULL,			NULL },					NULL,			NULL }

};

//////////////////////////////////////////////////////////////////////////
//! FIELDS DEFINITION
struct BLDebugTools::parampropdefs BLDebugTools::defPropNames[] = {

	// name_eng			name_rus				readable		writeable
	{ { "Version",		"������" },				TRUE,			FALSE },
	{ { NULL,			NULL },					NULL,			NULL }

};

//////////////////////////////////////////////////////////////////////////
BLDebugTools::BLDebugTools()
{
	Log::DebugFmt("% @\x0F%@\x07 %", "\t-> BLDebugTools rev.", "1.0", "[11 sep 17]");
}

//////////////////////////////////////////////////////////////////////////
void BLDebugTools::LogDebugImpl(const char* msg)
{
	Log::Debug(msg);
}

//////////////////////////////////////////////////////////////////////////
void BLDebugTools::LogDebugFmtImpl(const char* msg)
{
	Log::DebugFmt(msg);
}

//////////////////////////////////////////////////////////////////////////
void BLDebugTools::LogErrorImpl(const char* msg)
{
	Log::Error(msg);
}

//////////////////////////////////////////////////////////////////////////
void BLDebugTools::LogErrorFmtImpl(const char* msg)
{
	Log::ErrorFmt(msg);
}

//////////////////////////////////////////////////////////////////////////
void BLDebugTools::LogWarningImpl(const char* msg)
{
	Log::Warning(msg);
}

//////////////////////////////////////////////////////////////////////////
void BLDebugTools::LogWarningFmtImpl(const char* msg)
{
	Log::WarningFmt(msg);
}

//////////////////////////////////////////////////////////////////////////
void BLDebugTools::LogInfoImpl(const char* msg)
{
	Log::Info(msg);
}

//////////////////////////////////////////////////////////////////////////
void BLDebugTools::LogInfoFmtImpl(const char* msg)
{
	Log::InfoFmt(msg);
}

//////////////////////////////////////////////////////////////////////////
void BLDebugTools::DumpVTImpl()
{
	HookExplorer* he = new HookExplorer();
	he->RebuildVirtualTable();
	he->DumpVirtualTable();
	delete he;
}

//////////////////////////////////////////////////////////////////////////
void BLDebugTools::TestContextsImpl(CValue* va)
{
// 	CDataBase7* cdbseven = CDataBase7::GetDefault();
// 	CDBEngDDB7Service* srv = (CDBEngDDB7Service*)cdbseven->GetDDB7Service();
// 
// 	CDB7SetInfo cdbssi;
// 	srv->WriteDB7SetInfo(cdbssi);
// 	std::string aaa(cdbssi.GetMyInfo().GetDBDescr());
// 
// 	int s = 22;
}

//////////////////////////////////////////////////////////////////////////
int BLDebugTools::TestMutexImpl()
{
	HANDLE m_hEvent = CreateEventA(NULL, TRUE, FALSE, "Global\\BslCorConfig");

	switch (GetLastError())
	{
		// app is already running
	case ERROR_ALREADY_EXISTS:
		CloseHandle(m_hEvent);
		return 1;
		break;
	case ERROR_SUCCESS:
		CloseHandle(m_hEvent);
		break;
	}

	return 0;
}

//////////////////////////////////////////////////////////////////////////
int BLDebugTools::Compile(const char* path)
{

	std::ifstream file("D:\\code.os");
	std::stringstream buffer;

	buffer << file.rdbuf();
	std::string str = buffer.str();

	CBLModule7* cblm = new CBLModule7(0, str.c_str());
	if (cblm->Compile() == 0) return FALSE;

	CCompiledModule* compiledModule = cblm->pIntInfo->pCompiledModule;

	std::string sPath(path);
	std::string sPathConstArray = sPath + ".ca";
	std::string sPathMethArray = sPath + ".ma";
	std::string sPathCode = sPath + ".ex";

	CFile theFile;

	theFile.Open(sPathCode.c_str(), CFile::modeCreate | CFile::modeReadWrite);
	CArchive ex_archivator(&theFile, CArchive::store);
	compiledModule->PCode.Serialize(ex_archivator);
	ex_archivator.Close();
	theFile.Close();

	nlohmann::json dataPacket;

	dataPacket["consts"] = nlohmann::json();
	int constsCount = compiledModule->ConstArray.GetSize();
	for (int i = 0; i < constsCount; ++i)
	{
		char serializationBuffer[32768];
		CString serializationString(serializationBuffer, 32768);

		std::string setVal = std::string("\0");

		CValue* valCopy = new CValue(compiledModule->ConstArray[i]->val);
		valCopy->SaveToString(serializationString);
		setVal += std::string((const char*)serializationString);

		dataPacket["consts"].push_back(setVal);
	}

	dataPacket["meths"] = nlohmann::json();

	int methsCount = compiledModule->ProcArray.GetSize();
	for (int i = 0; i < constsCount; ++i)
	{
		nlohmann::json dumper;
		dumper["BaseAddr"] = compiledModule->ProcArray[i]->BaseAddr;
		dumper["Flag10"] = compiledModule->ProcArray[i]->Flag10;
		dumper["Flag11"] = compiledModule->ProcArray[i]->Flag11;
		dumper["Flag12"] = compiledModule->ProcArray[i]->Flag12;
		dumper["Flag9"] = compiledModule->ProcArray[i]->Flag9;
		dumper["Flag8"] = (int)compiledModule->ProcArray[i]->Flag8;
		dumper["NParams"] = compiledModule->ProcArray[i]->NParams;
		dumper["pName"] = (const char*)compiledModule->ProcArray[i]->pName;
		dumper["Type"] = compiledModule->ProcArray[i]->Type;

		dumper["VarList"] = nlohmann::json();
		int varDescrCount = compiledModule->ProcArray[i]->VarList.GetSize();
		for (int v = 0; v < varDescrCount; ++v)
		{
			nlohmann::json varDescr;
			varDescr["ArraySize"] = compiledModule->ProcArray[i]->VarList[v]->ArraySize;
			varDescr["Flag1"] = compiledModule->ProcArray[i]->VarList[v]->Flag1;
			varDescr["Name"] = (const char*)compiledModule->ProcArray[i]->VarList[v]->Name;
			varDescr["Type"] = compiledModule->ProcArray[i]->VarList[v]->Type;
			varDescr["Type2"] = compiledModule->ProcArray[i]->VarList[v]->Type2;

			dumper["VarList"].push_back(varDescr);
		}

		dataPacket["meths"].push_back(dumper);
	}

	std::ofstream cfile(sPathConstArray);
	cfile << dataPacket.dump(4);
	cfile.close();

// 	theFile.Open(sPathMethArray.c_str(), CFile::modeCreate | CFile::modeReadWrite);
// 	CArchive ma_archivator(&theFile, CArchive::store);
// 	compiledModule->ProcArray.Serialize(ma_archivator);
// 	ma_archivator.Close();
// 	theFile.Close();

	return TRUE;
}
