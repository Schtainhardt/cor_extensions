#pragma once
#include "stdafx.h"

//////////////////////////////////////////////////////////////////////////
// Hive message types:
// * TARGET_	messages which are targeted to one client and require response
// * PUBLIC_	broadcast messages that require response
// * PNOTIFY_	broadcast messages that do not require response	(PublicNOTIFY)
// * CNOTIFY_	targeted messages that do not require response (ClientNOTIFY)
// * INTERNAL_	self-targeted messages

enum HiveMessageType
{
	//+========================+===========================+=====================================+
	//| Request		           | Response			       | Description		                 |
	//+========================+===========================+=====================================+
		TARGET_EXECUTE,			TARGET_EXECUTE_RESP,		// Remote code execution
								TARGET_EXECUTE_ERR,
	//+------------------------+---------------------------+-------------------------------------+
		TARGET_VFS_MOUNT,		TARGET_VFS_MOUNT_RESP,		// Mount remote folder
								TARGET_VFS_MOUNT_ERR,
	//+------------------------+---------------------------+-------------------------------------+
		TARGET_VFSC_REQ,        TARGET_VFSC_RESP,           // VFS credentials
	//+------------------------+---------------------------+-------------------------------------+
		PUBLIC_LIST,			PNOTIFY_ALIVE,				// Heartbeat
	//+------------------------+---------------------------+-------------------------------------+
		PUBLIC_RESET_LIST,									// List reset request for HB trackers
	//+------------------------+---------------------------+-------------------------------------+
		INTERNAL_SHUTDOWN,									// Internal shutdown loopback
	//+------------------------+---------------------------+-------------------------------------+
		NO_ACTION											// Default no action request
	//+------------------------+---------------------------+-------------------------------------+
};

//////////////////////////////////////////////////////////////////////////
// Hive roles:
//
enum HiveRole
{
	// BL instances
	HR_ENTERPRISE,
	HR_CONFIG,

	// bee
	HR_LOGGER,

	// server modules
	HR_MASTER,
	HR_EXCHANGER,
	HR_HB_INVOKER,
	HR_VFSC_BROKER,

	// client module
	HR_LAUNCHER
};

#pragma pack(push, 1)
#define HiveMessageHeadSize sizeof(HiveMessage)

//////////////////////////////////////////////////////////////////////////
// Message holder:
// 
struct HiveMessage
{
	unsigned int totalSize;
	unsigned int senderToken;
	unsigned int targetToken;
	HiveMessageType type;
	unsigned int dataLength;
	char* data;

	HiveMessage (unsigned int length = 0)
	{
		dataLength = length;
		totalSize = HiveMessageHeadSize + dataLength;
		type = NO_ACTION;

		if (length > 0)
			data = new char[length];
	}

	HiveMessage(std::vector<char>& vdata)
	{
		dataLength = vdata.size();
		totalSize = HiveMessageHeadSize + dataLength;
		type = NO_ACTION;

		if (dataLength > 0)
		{
			data = new char[dataLength];
			memcpy(data, vdata.data(), dataLength);
		}
	}

	HiveMessage(const char* vdata, unsigned int length)
	{
		dataLength = length;
		totalSize = HiveMessageHeadSize + dataLength;
		type = NO_ACTION;

		if (length > 0)
		{
			data = new char[length];
			memcpy(data, vdata, length);
		}
	}

	operator zmq::message_t()
	{
		char* rawMessage = new char[this->totalSize];
		memcpy(rawMessage, this, HiveMessageHeadSize);
		
		char* rawMessageDataPtr = HiveMessageHeadSize + rawMessage;
		memcpy(rawMessageDataPtr, this->data, this->dataLength);

		zmq::message_t msg(rawMessage, this->totalSize);

		//rawMessageDataPtr = nullptr;
		//delete[] rawMessage;

		return msg;
	}
};

//////////////////////////////////////////////////////////////////////////
// Alive [heartbeat]:
//
struct HiveMessageAlive
{
	unsigned int ProcessID;
	HiveRole Role;
};

//////////////////////////////////////////////////////////////////////////
// FS Mount
//
struct HiveVFSCredentials
{
	// custom send, see serverside
};

#pragma pack(pop)