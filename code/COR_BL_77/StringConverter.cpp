#include "stdafx.h"
#include "StringConverter.hpp"

#include "UTF_Tools.h"
#include "base64.hpp"

IMPLEMENT_DYNCREATE(StringCoverter, CBLContext)

//////////////////////////////////////////////////////////////////////////
//! METHODS DEFINITION
struct StringCoverter::paramdefs StringCoverter::defFnNames[] = {

	// name_eng			name_rus				returns val		args count
	{ { "AsciiToUTF8", "����������8" },			TRUE,			1 },
	{ { "UTF8ToAscii", "���8�������" },			TRUE,			1 },

	{ { "AsciiToUTF16", "����������16" },		TRUE,			1 },
	{ { "UTF16ToAscii", "���16�������" },		TRUE,			1 },

	{ { "UTF8ToUTF16",  "���8����16" },			TRUE,			1 },
	{ { "UTF16ToUTF8",  "���16����8" },			TRUE,			1 },

	{ { "UTF8ToURI",	"���8�������" },		TRUE,			1 },
	{ { "AsciiToURI",	"�������������" },		TRUE,			1 },
	{ { "Base64ToFloat","������������������" },	TRUE,			1 },

	{ { NULL,			NULL },					NULL,			NULL }

};

//////////////////////////////////////////////////////////////////////////
//! FIELDS DEFINITION
struct StringCoverter::parampropdefs StringCoverter::defPropNames[] = {

	// name_eng			name_rus				readable		writeable
	{ { "Version",		"������" },				TRUE,			FALSE },
	{ { NULL,			NULL },					NULL,			NULL }

};

//////////////////////////////////////////////////////////////////////////
StringCoverter::StringCoverter()
{
	Log::DebugFmt("% @\x0F%@\x07 %", "\t-> StringConverter rev.", "1.0", "[16 mar 18]");
}

const char* StringCoverter::Utf8ToAscii(const char* msg)
{
	std::string returnString = UTF_Tools::UTF8_To_ASCII(std::string(msg));
	char* returnValue = new char[returnString.length()];
	ZeroMemory(returnValue, returnString.length() * sizeof(char));
	memcpy(returnValue, returnString.c_str(), returnString.length());
	return returnValue;
}

const char* StringCoverter::AsciiToUtf8(const char* msg)
{
	std::string returnString = UTF_Tools::ASCII_To_UTF8(msg);
	char* returnValue = new char[returnString.length()+1];
	ZeroMemory(returnValue, returnString.length() * sizeof(char) +1);
	memcpy(returnValue, returnString.c_str(), returnString.length());
	return returnValue;
}

const wchar_t* StringCoverter::Utf8ToUtf16(const char* msg)
{
	std::wstring returnString = UTF_Tools::UTF8_To_UTF16(std::string(msg));
	wchar_t* returnValue = new wchar_t[returnString.length()];
	ZeroMemory(returnValue, returnString.length() * sizeof(wchar_t));
	memcpy(returnValue, returnString.c_str(), returnString.length());
	return returnValue;
}

const char* StringCoverter::Utf16ToUtf8(const wchar_t* msg)
{
	std::string returnString = UTF_Tools::UTF16_To_UTF8(std::wstring(msg));
	char* returnValue = new char[returnString.length()];
	ZeroMemory(returnValue, returnString.length() * sizeof(char));
	memcpy(returnValue, returnString.c_str(), returnString.length());
	return returnValue;
}

const wchar_t* StringCoverter::AsciiToUtf16(const char* msg)
{
	std::wstring returnString = UTF_Tools::ASCII_To_UTF16(std::string(msg));
	wchar_t* returnValue = new wchar_t[returnString.length()];
	ZeroMemory(returnValue, returnString.length() * sizeof(wchar_t));
	memcpy(returnValue, returnString.c_str(), returnString.length());
	return returnValue;
}

const char* StringCoverter::Utf16ToAscii(const wchar_t* msg)
{
	std::string returnString = UTF_Tools::UTF16_To_ASCII(std::wstring(msg));
	char* returnValue = new char[returnString.length()];
	ZeroMemory(returnValue, returnString.length() * sizeof(char));
	memcpy(returnValue, returnString.c_str(), returnString.length());
	return returnValue;
}

const char* StringCoverter::AsciiToUri(const char* msg)
{
	std::string returnString = UTF_Tools::UTF8_To_URL( UTF_Tools::ASCII_To_UTF8(msg) );
	char* returnValue = new char[returnString.length()+1];
	ZeroMemory(returnValue, returnString.length() * sizeof(char) +1);
	memcpy(returnValue, returnString.c_str(), returnString.length());
	return returnValue;
}

const char* StringCoverter::Utf8ToUri(const char* msg)
{
	std::string returnString = UTF_Tools::UTF8_To_URL(std::string(msg));
	char* returnValue = new char[returnString.length()];
	ZeroMemory(returnValue, returnString.length() * sizeof(char));
	memcpy(returnValue, returnString.c_str(), returnString.length());
	return returnValue;
}

//////////////////////////////////////////////////////////////////////////
long StringCoverter::Base64ToInt64(const char* msg)
{
	long long* val = new long long[1];
	ZeroMemory(val, sizeof(long long));
	Base64::Decode(msg, strlen(msg), (char*)val, sizeof(long long));

	int valLow = *((int*)val);
	int valHigh = *((int*)val+sizeof(int));

	int retvalL = valLow;
	retvalL = valLow & 0xFF;
	retvalL = (retvalL << 8) | ((valLow >> 8) & 0xFF);
	retvalL = (retvalL << 8) | ((valLow >> 16) & 0xFF);
	retvalL = (retvalL << 8) | ((valLow >> 24) & 0xFF);

	int retvalH = valHigh;
	retvalH = valHigh & 0xFF;
	retvalH = (retvalH << 8) | ((valHigh >> 8) & 0xFF);
	retvalH = (retvalH << 8) | ((valHigh >> 16) & 0xFF);
	retvalH = (retvalH << 8) | ((valHigh >> 24) & 0xFF);

	return retvalL + retvalH;

}
