#include "stdafx.h"
#include "MemManager.h"

void* VirtualRealloc(void* lpAddress, unsigned long dwSize, unsigned long flProtect)
{
	DWORD dBytesOld;
	LPVOID lpAddressNew;
	MEMORY_BASIC_INFORMATION mbiStatus;

	//Check size of current array in bytes, quit if no bytes available.
	if (!VirtualQuery(lpAddress, &mbiStatus, sizeof(mbiStatus))) 
		return NULL;

	dBytesOld = mbiStatus.RegionSize;

	if (!dBytesOld) 
		return NULL;

	//Create a new array, quit if failed to create.
	lpAddressNew = VirtualAlloc(NULL, dwSize, MEM_COMMIT, flProtect);

	if (!lpAddressNew) 
		return NULL;

	VirtualQuery(lpAddressNew, &mbiStatus, sizeof(mbiStatus));

	//Copy old array onto new array, making sure not to exceed any boundries.
	if (dBytesOld > mbiStatus.RegionSize) 
		dBytesOld = mbiStatus.RegionSize;

	CopyMemory(lpAddressNew, (void*)lpAddress, dBytesOld);

	//Free old array and return address of new array.
	VirtualFree((void*)lpAddress, 0, MEM_RELEASE);

	return lpAddressNew;
}

#ifdef EXPORT_MEMORY_MANAGER
extern "C" __declspec(dllexport) void* BL_Alloc(size_t memSize)
{
	Log::DebugFmt("[@\x03A ALLOCATOR @\x07]\tRequested\t\t@\x0A#@\x07 bytes", memSize);
	//return VirtualAlloc(0, memSize, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	void* rv = malloc(memSize);
	ZeroMemory(rv, memSize);
	return rv;
}

extern "C" __declspec(dllexport) void* BL_Realloc(void* ptr, size_t memSize)
{
	Log::DebugFmt("[@\x03A ALLOCATOR @\x07]\Reallocated\t\t@\x0A$\t> #@\x07 bytes", ptr, memSize);
	//return VirtualRealloc(ptr, memSize, PAGE_EXECUTE_READWRITE);
	return realloc(ptr, memSize);
}

extern "C" __declspec(dllexport) void BL_Free(void* ptr)
{
	Log::DebugFmt("[@\x03A ALLOCATOR @\x07]\tFreeing block at\t@\x0A$\x07", ptr);
	//VirtualFree(ptr, 0, MEM_RELEASE);
	free(ptr);
}
#else
void* BL_Alloc(size_t memSize)
{
	//Log::DebugFmt("[@\x03A ALLOCATOR @\x07]\tRequested\t\t@\x0A#@\x07 bytes", memSize);
	//return VirtualAlloc(0, memSize, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	void* rv = malloc(memSize);
	ZeroMemory(rv, memSize);
	return rv;
}

void* BL_Realloc(void* ptr, size_t memSize)
{
	//Log::DebugFmt("[@\x03A ALLOCATOR @\x07]\tReallocated\t\t@\x0A$\t> #@\x07 bytes", ptr, memSize);
	//return VirtualRealloc(ptr, memSize, PAGE_EXECUTE_READWRITE);
	return realloc(ptr, memSize);
}

void BL_Free(void* ptr)
{
	//Log::DebugFmt("[@\x03A ALLOCATOR @\x07]\tFreeing block at\t@\x0A$\x07", ptr);
	//VirtualFree(ptr, 0, MEM_RELEASE);
	free(ptr);
}
#endif // EXPORT_MEMORY_MANAGER