#pragma once
#include "stdafx.h"

//////////////////////////////////////////////////////////////////////////
// ::OnLoadSourcePriv
class Hook_OnLoadSourcePriv
{
public:
	const char* module_name = "blang.dll";
	const char* mangled_func = "?OnLoadSourcePriv@@YAHPAVCString@@AAV1@@Z";

	Hook_OnLoadSourcePriv();

private:

	typedef int(__cdecl *OnLoadSourcePrivFunc)(CString str1, CString& str2);
	static OnLoadSourcePrivFunc orig_OnLoadSourcePriv;
	static int __cdecl hk_OnLoadSourcePriv(CString str1, CString& str2);

};