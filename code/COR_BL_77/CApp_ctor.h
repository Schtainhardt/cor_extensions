#pragma once
#include "stdafx.h"

//////////////////////////////////////////////////////////////////////////
// CApp:: -> ctor

// NOTICE:
// This hook works only with some kind of DLL loader!

class Hook_CApp
{
public:
	const char* module_name = "seven.dll";
	const char* mangled_func = "??0CApp7@@QAE@XZ";

	Hook_CApp();
	~Hook_CApp();

private:

	static int __stdcall WriteAppPointerFunc();

};