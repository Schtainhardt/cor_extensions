#include "stdafx.h"
#include "UTF_Tools.h"

#include <sstream>
#include <iomanip>
#include <iostream>
#include <string>
#include <locale>
#include <codecvt>

//////////////////////////////////////////////////////////////////////////
std::string UTF_Tools::UTF8_To_ASCII(std::string utf8String)
{
	//std::string res;
	int result_u = 0;
	int result_c = 0;

	result_u = MultiByteToWideChar(CP_UTF8, 0, utf8String.c_str(), -1, 0, 0);

	if (!result_u)
		return 0;

	std::wstring ures(result_u+1, L'\0');

	if (!MultiByteToWideChar(CP_UTF8, 0, utf8String.c_str(), -1, const_cast<wchar_t*>(ures.c_str()), result_u))
	{
		return 0;
	}

	result_c = WideCharToMultiByte(CP_THREAD_ACP, 0, const_cast<wchar_t*>(ures.c_str()), -1, 0, 0, 0, 0);

	if (!result_c)
	{
		return 0;
	}

	std::string cres(result_c+1, '\0');

	if (!WideCharToMultiByte(CP_THREAD_ACP, 0, const_cast<wchar_t*>(ures.c_str()), -1, const_cast<char*>(cres.c_str()), result_c, 0, 0))
	{
		return 0;
	}

	//res.append(cres);
		
	return cres;
}

//////////////////////////////////////////////////////////////////////////
std::string UTF_Tools::ASCII_To_UTF8(std::string asciiString)
{

 	int size = MultiByteToWideChar(CP_THREAD_ACP, MB_PRECOMPOSED, asciiString.c_str(),
 		asciiString.length(), nullptr, 0);
 
	std::wstring utf16_str(size + 1, L'\0');
 
 	MultiByteToWideChar(CP_THREAD_ACP, MB_PRECOMPOSED, asciiString.c_str(),
 		asciiString.length(), const_cast<wchar_t*>(utf16_str.c_str()), size);
 
 	int utf8_size = WideCharToMultiByte(CP_UTF8, 0, const_cast<wchar_t*>(utf16_str.c_str()),
		size, nullptr, 0,
 		nullptr, nullptr);
 
 	std::string utf8_str(utf8_size, '\0');
 	WideCharToMultiByte(CP_UTF8, 0, const_cast<wchar_t*>(utf16_str.c_str()),
		size, const_cast<char*>(utf8_str.c_str()), utf8_size,
 		nullptr, nullptr);

	return utf8_str;
}

std::string UTF_Tools::UTF16_To_ASCII(std::wstring utf16String)
{
	return UTF8_To_ASCII(UTF16_To_UTF8(utf16String));
}

std::wstring UTF_Tools::ASCII_To_UTF16(std::string asciiString)
{
	return UTF8_To_UTF16(ASCII_To_UTF8(asciiString));
}

std::wstring UTF_Tools::UTF8_To_UTF16(std::string utf8String)
{

	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> converter;
	std::wstring wstr = converter.from_bytes(utf8String);

	return wstr;
}

std::string UTF_Tools::UTF16_To_UTF8(std::wstring utf16String)
{
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> converter;
	std::string dest = converter.to_bytes(utf16String);
	return dest;
}

std::string UTF_Tools::UTF8_To_URL(std::string utfString)
{
	std::stringstream urlEncoded;
	urlEncoded << std::hex << std::setfill('0');

	for (std::string::const_iterator it = utfString.begin(); it != utfString.end(); ++it) {
		urlEncoded << '%' << std::setw(2) << static_cast<unsigned int>(static_cast<unsigned char>(*it));
	}

	return urlEncoded.str();
}