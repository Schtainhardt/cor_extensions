#include "stdafx.h"

#include "Networking.h"

#include "EventManager.h"
#include "Log.h"

#include "NetworkingHelper.h"

#include "json.hpp"
using json = nlohmann::json;
//////////////////////////////////////////////////////////////////////////
Networking* Networking::p_instance;

#define GENERIC_CATCH(ret) catch (std::exception const& e) \
{ \
	Log::ErrorFmt("Networking: Error on % :\n %", __FUNCTION__, e.what()); \
	return ret; \
}

enum RpcResp
{
	mlrOK,
	mlrERR_NO_ID,
	mlrERR_BANNED,
	mlrERR_GENERAL
};

//////////////////////////////////////////////////////////////////////////
std::string stringXor(std::string value, std::string key)
{
	std::string retval(value);
	long unsigned int klen = key.length();
	long unsigned int vlen = value.length();
	unsigned long int k = 0;
	unsigned long int v = 0;
	for (; v < vlen; v++) {
		retval[v] = value[v] ^ key[k];
		k = (++k < klen ? k : 0);
	}
	return retval;
}

//////////////////////////////////////////////////////////////////////////
Networking::Networking()
{
	// #WARNING: NO! Causes race condition in std::thread
	// Connect();

	//serversList.push_back("127.0.0.1");

	clientKey = KeyGenerator::GenerateKey();

	EventMan;	// lazy initialization
}

//////////////////////////////////////////////////////////////////////////
inline void Networking::_CheckConnection()
{
	int triesRemain = 5;
	
	if (clientRPC != nullptr)
		connected = (clientRPC->get_connection_state() == rpc::client::connection_state::connected);
	else
		connected = false;

	while (!connected && --triesRemain > 0) 
	{
		_Connect(); 
		Sleep(150);
	}

	if (!connected) throw std::exception("Not connected");
}

//////////////////////////////////////////////////////////////////////////
bool Networking::_Connect()
{
	try
	{
		if (clientRPC != nullptr)
		{
			connected = (clientRPC->get_connection_state() == rpc::client::connection_state::connected);
			if (connected)
				return true;
			else
				delete clientRPC;
		}

		clientRPC = new rpc::client("cor.com.ua", 10416);
		clientRPC->set_timeout(10000);
		connected = (clientRPC->get_connection_state() == rpc::client::connection_state::connected);
		return connected;
	}
	catch (std::exception& e)
	{
		connected = false;
		Log::ErrorFmt("Networking: Error on connecting to server % :\n %", "127.0.0.1", e.what());
		return false;
	}
	
	return true;
}

//////////////////////////////////////////////////////////////////////////
bool Networking::_GetToken()
{
	try
	{
		_CheckConnection();
		token = clientRPC->call("GetToken").as<std::string>();
	}
	GENERIC_CATCH(false)

	return true;
}

//////////////////////////////////////////////////////////////////////////
bool Networking::_ServerLogin(std::string userName, std::string Company, std::string Unit, std::string Subunit)
{
	try
	{
		_CheckConnection();

		json req;
		req["token"] = token;
		req["id"] = clientKey;
		req["clientInfoJson"] = json();
		req["clientInfoJson"]["name"] = userName;
		req["clientInfoJson"]["company"] = Company;
		req["clientInfoJson"]["unit"] = Unit;
		req["clientInfoJson"]["subunit"] = Subunit;

		std::string loginResult = clientRPC->call("ServerLogin", req.dump()).as<std::string>();

		json resp = json::parse(loginResult);
		Log::DebugFmt("Login result: %", loginResult.c_str());

		if (resp["messageType"].get<std::string>() == "error")
		{
			Log::ErrorFmt("Error on %: %", __FUNCTION__, resp["message"].get<std::string>().c_str());
			Disconnect();
			return false;
		}

		if (resp["messageType"].get<std::string>() == "resp")
		{
			RpcResp msgResp = (RpcResp)resp["result"].get<int>();
			if (msgResp != mlrOK)
			{
				Log::ErrorFmt("Error after %: code #", __FUNCTION__, (int)msgResp);
				Disconnect();
				return false;
			}
		}

		Log::DebugFmt("It seems that we've connected to server");
	}
	GENERIC_CATCH(false)

	return true;
}

//////////////////////////////////////////////////////////////////////////
void Networking::CheckConnection()
{
	_CheckConnection();
}

//////////////////////////////////////////////////////////////////////////
bool Networking::Connect(std::string userName, std::string Company, std::string Unit, std::string Subunit)
{
	if (!_GetToken()) return false;
	if (!_ServerLogin(userName, Company, Unit, Subunit)) return false;

	return true;
}

//////////////////////////////////////////////////////////////////////////
bool Networking::Disconnect()
{
	try
	{
		_CheckConnection();

		json req;
		req["connectionId"] = clientKey;
		json resp = json::parse(clientRPC->call("ServerLogout", req.dump()).as<std::string>());
		if (resp["messageType"].get<std::string>() == "error")
		{
			Log::ErrorFmt("Error on %: code %", __FUNCTION__, resp["message"].get<std::string>());
			clientRPC = nullptr;
			return false;
		}
		clientRPC = nullptr;
	}
	GENERIC_CATCH(false)

	return true;
}

//////////////////////////////////////////////////////////////////////////
bool Networking::MessagingLogin()
{
	try
	{
		_CheckConnection();

		json req;
		req["connectionId"] = clientKey;
		json resp = json::parse(clientRPC->call("MessagingLogin", req.dump()).as<std::string>());
		if (resp["messageType"].get<std::string>() == "error")
		{
			Log::ErrorFmt("Error on %: %", __FUNCTION__, resp["message"].get<std::string>().c_str());
			Disconnect();
			return false;
		}

		if (resp["messageType"].get<std::string>() == "resp")
		{
			RpcResp nmsgResp = (RpcResp)resp["result"].get<int>();
			if (nmsgResp != mlrOK)
			{
				Log::ErrorFmt("Error after %: code #", __FUNCTION__, (int)nmsgResp);
				Disconnect();
				return false;
			}
		}
	}
	GENERIC_CATCH(false)

	return true;
}

//////////////////////////////////////////////////////////////////////////
bool Networking::MessagingLogout()
{
	try
	{
		_CheckConnection();

		json req;
		req["connectionId"] = clientKey;
		json resp = json::parse(clientRPC->call("MessagingLogout", req.dump()).as<std::string>());
		if (resp["messageType"].get<std::string>() == "error")
		{
			Log::ErrorFmt("Error on %: %", __FUNCTION__, resp["message"].get<std::string>().c_str());
			Disconnect();
			return false;
		}

		if (resp["messageType"].get<std::string>() == "resp")
		{
			RpcResp nmsgResp = (RpcResp)resp["result"].get<int>();
			if (nmsgResp != mlrOK)
			{
				Log::ErrorFmt("Error after %: code #", __FUNCTION__, (int)nmsgResp);
				Disconnect();
				return false;
			}
		}
	}
	GENERIC_CATCH(false)

	return true;
}

//////////////////////////////////////////////////////////////////////////
bool Networking::MessagingSub(std::string channelName)
{
	try
	{
		_CheckConnection();

		json req;
		req["connectionId"] = clientKey;
		req["channelName"] = channelName;
		json resp = json::parse(clientRPC->call("MessagingSub", req.dump()).as<std::string>());
		if (resp["messageType"].get<std::string>() == "error")
		{
			Log::ErrorFmt("Error on %: %", __FUNCTION__, resp["message"].get<std::string>().c_str());
			Disconnect();
			return false;
		}

		if (resp["messageType"].get<std::string>() == "resp")
		{
			RpcResp nmsgResp = (RpcResp)resp["result"].get<int>();
			if (nmsgResp != mlrOK)
			{
				Log::ErrorFmt("Error after %: code #", __FUNCTION__, (int)nmsgResp);
				Disconnect();
				return false;
			}
		}
	}
	GENERIC_CATCH(false)

	return true;
}

//////////////////////////////////////////////////////////////////////////
bool Networking::MessagingLeave(std::string channelName)
{
	try
	{
		_CheckConnection();

		json req;
		req["connectionId"] = clientKey;
		req["channelName"] = channelName;
		json resp = json::parse(clientRPC->call("MessagingLeave", req.dump()).as<std::string>());
		if (resp["messageType"].get<std::string>() == "error")
		{
			Log::ErrorFmt("Error on %: %", __FUNCTION__, resp["message"].get<std::string>().c_str());
			Disconnect();
			return false;
		}

		if (resp["messageType"].get<std::string>() == "resp")
		{
			RpcResp nmsgResp = (RpcResp)resp["result"].get<int>();
			if (nmsgResp != mlrOK)
			{
				Log::ErrorFmt("Error after %: code #", __FUNCTION__, (int)nmsgResp);
				Disconnect();
				return false;
			}
		}
	}
	GENERIC_CATCH(false)

	return true;
}

//////////////////////////////////////////////////////////////////////////
bool Networking::MessagingSend(std::string user, std::string channel, std::string text)
{
	try
	{
		_CheckConnection();

		json req;
		req["connectionId"] = clientKey;
		req["msg"] = json();
		req["msg"]["channel"] = channel;
		req["msg"]["name"] = user;
		req["msg"]["text"] = text;
		json resp = json::parse(clientRPC->call("SendMessage", req.dump()).as<std::string>());

		if (resp["messageType"].get<std::string>() == "error")
		{
			Log::ErrorFmt("Error on %: %", __FUNCTION__, resp["message"].get<std::string>().c_str());
			Disconnect();
			return false;
		}

		if (resp["messageType"].get<std::string>() == "resp")
		{
			RpcResp nmsgResp = (RpcResp)resp["result"].get<int>();
			if (nmsgResp != mlrOK)
			{
				Log::ErrorFmt("Error after %: code #", __FUNCTION__, (int)nmsgResp);
				Disconnect();
				return false;
			}
		}
	}
	GENERIC_CATCH(false)

	return true;
}

//////////////////////////////////////////////////////////////////////////
bool Networking::MessagingCheckInbox(MessagePacket** outMessagePacket)
{
	try
	{
		_CheckConnection();

		*outMessagePacket = nullptr;

		json req;
		req["connectionId"] = clientKey;
		json resp = json::parse(clientRPC->call("CheckInbox", req.dump()).as<std::string>());

		if (resp["messageType"].get<std::string>() == "error")
		{
			Log::ErrorFmt("Error on % (CheckInbox): %", __FUNCTION__, resp["message"].get<std::string>().c_str());
			Disconnect();
			return false;
		}

		if (resp["messageType"].get<std::string>() == "resp")
		{
			RpcResp nmsgResp = (RpcResp)resp["result"].get<int>();
			if (nmsgResp != mlrOK)
			{
				Log::ErrorFmt("Error after % (CheckInbox): code #", __FUNCTION__, (int)nmsgResp);
				Disconnect();
				return false;
			}
		}

		if (resp["messageType"].get<std::string>() == "msgArray")
		{
			json msgArray = resp["messages"];
			for (auto& msgPack : msgArray)
			{
				MessagePacket msgPacket;
				msgPacket.channel = msgPack["channel"].get<std::string>();
				msgPacket.name = msgPack["name"].get<std::string>();
				msgPacket.text = msgPack["text"].get<std::string>();
				inbox.emplace_back(msgPacket);
			}
		}

		if (inbox.size() > 0)
		{
			auto it = inbox.begin();
			*outMessagePacket = new MessagePacket(*it);
			inbox.erase(it);
			return inbox.size() > 0;
		}
	}
	GENERIC_CATCH(false)

	return false;
}