#include "stdafx.h"

#include "CBLContext_CBLContext.h"

#include "Entry.h"
#include "HookTools.h"

#pragma warning( once : 4309)

//////////////////////////////////////////////////////////////////////////
// CBLContext::CBLContext
Hook_CBLContext::Hook_CBLContext()
{

	HOOK_MESSAGE;

	HANDLE processHandle = GetCurrentProcess();
	HMODULE moduleHandle = GetModuleHandleA(module_name);
	void* registrar = GetProcAddress(moduleHandle, mangled_func);

	char* innerShellcode = (char*)malloc(32);
	ZeroMemory(innerShellcode, 32);

	*innerShellcode++ = 0x60;	// pushad
	*innerShellcode++ = 0x51;	// push ecx

	*innerShellcode++ = 0xB8;	// mov eax
	*((DWORD*)innerShellcode) = (DWORD)hk_CBLContext;
	innerShellcode += 4;
	*innerShellcode++ = 0xFF;	// call
	*innerShellcode++ = 0xD0;	// eax

	*innerShellcode++ = 0x61;	// popad

	*innerShellcode++ = 0xB8;	// mov eax
	*((DWORD*)innerShellcode) = (DWORD)registrar+0x07;
	innerShellcode += 4;

 	*innerShellcode++ = 0x6A;	// hardcoded binary dump of original, if u mind
 	*innerShellcode++ = 0xFF;
 	*innerShellcode++ = 0x68;
 	*innerShellcode++ = 0x43;
 	*innerShellcode++ = 0xB6;
 	*innerShellcode++ = 0x1E;
 	*innerShellcode++ = 0x20;

	*innerShellcode++ = 0xFF;	// jmp
	*innerShellcode++ = 0xE0;	// eax

	innerShellcode -= 24;

	char* shellCode = (char*)malloc(7);
	ZeroMemory(shellCode, 7);

	*shellCode++ = 0xB8;
	*((DWORD*)shellCode) = (DWORD)innerShellcode;	// ...  dword ptr [ReroutedFuncPtr]
	shellCode += 4; //rewind
	*shellCode++ = 0xFF;
	*shellCode++ = 0xE0;
	shellCode -= 7;
	// <<<<

	backupData = (char*)malloc(7);
	ZeroMemory(backupData, 7);
	memcpy(backupData, registrar, 7);

	registrarBackupAddr = registrar;

	LPVOID res_CBLContext_pre = HookFunctionDirectInject<0>(processHandle, registrar, 7, shellCode);
}

Hook_CBLContext::~Hook_CBLContext()
{
	memcpy(registrarBackupAddr, backupData, 7);
}

void __stdcall Hook_CBLContext::hk_CBLContext(CBLContext* thisContext)
{

	// === !!! WARNING !!! ===
	// mind that object is not initialized yet! It's pre-constructor trap!

	//ContextHandle hContext = ContextsMan.RegisterContext(thisContext);

}
