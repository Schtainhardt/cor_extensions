#include "stdafx.h"
#include "CDBEngDDB7Service_ctor.h"

#include "HookTools.h"

#include "Entry.h"

//////////////////////////////////////////////////////////////////////////
// CDBEngDDB7Service:: -> ctor

Hook_CDBEngDDB7Service::Hook_CDBEngDDB7Service()
{
	HOOK_MESSAGE;

	HANDLE processHandle = GetCurrentProcess();
	HMODULE moduleHandle = GetModuleHandleA(module_name);
	LPVOID registrar = GetProcAddress(moduleHandle, mangled_func);

	DWORD OldProtection = 0;
	if (!VirtualProtectEx(processHandle, (LPVOID)((DWORD)registrar-11), 13, PAGE_EXECUTE_READWRITE, &OldProtection))
		Log::Error("Failed");

	char* writer = (char*)registrar - 9;
	*writer++ = 0xE8;
	*((DWORD*)writer) = (DWORD)WriteAppPointerFunc - (DWORD)writer - 4;
	writer += 4;
	*writer++ = 0x6A;
	*writer++ = 0xFF;
	*writer++ = 0xEB;
	*writer++ = 0x02;
	*writer++ = 0xEB;
	*writer++ = 0xF5;
}

Hook_CDBEngDDB7Service::~Hook_CDBEngDDB7Service()
{

}

int __stdcall Hook_CDBEngDDB7Service::WriteAppPointerFunc()
{
	CDBEngDDB7Service* ca;
	__asm
	{
		mov ca, ecx
	}
	CCORExtensions::DBService = ca;
}