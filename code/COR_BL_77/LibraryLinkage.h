#pragma once

#define CURL_STATICLIB
#define ZMQ_STATIC

//#define BSL_DEV

#pragma comment(lib, "Iphlpapi.lib")
#pragma comment(lib, "shlwapi.lib")

#pragma comment(lib, "bkend.lib")
#pragma comment(lib, "blang.lib")
#pragma comment(lib, "seven.lib")
#pragma comment(lib, "type32.lib")
#pragma comment(lib, "frame.lib")

#ifdef BSL_DEV
#pragma comment(lib, "libminhook.lib")
#endif // BSL_DEV

#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "libcurl.lib")

#pragma comment(lib, "rpc.lib")
#pragma comment(lib, "libzmq.lib")

//#pragma comment(lib, "mscoree.lib")