#pragma once
#include "stdafx.h"

//////////////////////////////////////////////////////////////////////////
// CBLContext::FindMethod
class Hook_FindMethod
{
public:
	const char* module_name = "bkend.dll";
	const char* mangled_func = "?FindMethod@CBLContext@@UBEHPBD@Z";

	Hook_FindMethod();

private:
	
	typedef int(__thiscall *FindMethodFunc)(char const* methodName);
	static FindMethodFunc orig_FindMethod;
	static int __stdcall hk_FindMethod(char const* methodName);


};