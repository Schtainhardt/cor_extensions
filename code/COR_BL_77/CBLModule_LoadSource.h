#pragma once
#include "stdafx.h"

//////////////////////////////////////////////////////////////////////////
// CBLModule:: -> LoadSource
class Hook_LoadSource
{
public:
	const char* module_name = "blang.dll";
	const char* mangled_func = "?LoadSource@CBLModule@@QAEHPBD@Z";

	Hook_LoadSource();

private:

	typedef int(__thiscall *LoadSourceFunc)(const char* sourceCode);
	static LoadSourceFunc orig_LoadSource;
	static int __stdcall hk_LoadSource(const char* sourceCode);

};