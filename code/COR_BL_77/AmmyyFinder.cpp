#include "stdafx.h"
#include "AmmyyFinder.h"

#if _WIN32_WINNT > 0x501
struct SCHandle
{
private:
	SC_HANDLE hValue;

public:
	SCHandle(SC_HANDLE Value) : hValue(Value) {};

	~SCHandle()
	{
		if (hValue != NULL)
			CloseServiceHandle(hValue);
	}

	operator SC_HANDLE()
	{
		return hValue;
	}

	bool operator!() const
	{
		return (hValue == NULL);
	}
};

std::string AmmyyFinder::GetServiceBinaryName(const std::string& ServiceName)
{
	SCHandle hSCManager(OpenSCManager(NULL, NULL, SC_MANAGER_CONNECT));

	if (!hSCManager)
		return "";

	SCHandle hService(OpenService(hSCManager, ServiceName.c_str(), SERVICE_QUERY_CONFIG));
	if (!hService)
		return "";

	std::vector<BYTE> buffer;
	DWORD dwBytesNeeded = sizeof(QUERY_SERVICE_CONFIGW);
	LPQUERY_SERVICE_CONFIG pConfig;

	do
	{
		buffer.resize(dwBytesNeeded);
		pConfig = (LPQUERY_SERVICE_CONFIG)&buffer[0];

		if (QueryServiceConfig(hService, pConfig, buffer.size(), &dwBytesNeeded))
			return pConfig->lpBinaryPathName;

	} while (GetLastError() == ERROR_INSUFFICIENT_BUFFER);

	return "";
}

#include <regex>
#include <iterator>
std::string AmmyyFinder::GetAmmyyBinary()
{
	std::stringstream ss;
	std::string res = GetServiceBinaryName("AmmyyAdmin");

	std::regex rgx("\"([^\"]*)\"");

	std::sregex_iterator current(res.begin(), res.end(), rgx);
	std::sregex_iterator end;

	if (current == end)
		return "";

	auto a = *current;
	std::string b = a.str();

	return b;
}
#endif