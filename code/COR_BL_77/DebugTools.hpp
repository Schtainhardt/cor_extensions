#pragma once
#include "stdafx.h"

#include <iostream>

#include "HookTools.h"

class BLDebugTools : public CBLContext
{
private:

	CBLContext* ExtensionContext;
	CBLModule7* ExtensionModule;

public:

	DECLARE_DYNCREATE(BLDebugTools);

	BLDebugTools();
	virtual ~BLDebugTools() {};

	void LogDebugImpl(const char* msg);
	void LogDebugFmtImpl(const char* msg);
	void LogErrorImpl(const char* msg);
	void LogErrorFmtImpl(const char* msg);
	void LogWarningImpl(const char* msg);
	void LogWarningFmtImpl(const char* msg);
	void LogInfoImpl(const char* msg);
	void LogInfoFmtImpl(const char* msg);

	void DumpVTImpl();

	void TestContextsImpl(CValue* va);

	int TestMutexImpl();

	int Compile(const char* path);

	//////////////////////////////////////////////////////////////////////////
	virtual int CallAsFunc(int iMethNum, class CValue& rValue, class CValue** ppValue)
	{
		switch (iMethNum)
		{
		case TestMutex:
			rValue = CValue(TestMutexImpl());
			break;
		case mCompile:
			rValue = CValue(Compile(ppValue[0]->m_String));
		default:
			return 1;
		};

		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int CallAsProc(int iMethNum, class CValue** ppValue)
	{
		switch (iMethNum)
		{
		case LogDebug:
		{
			LogDebugImpl(ppValue[0]->m_String);
			break;
		}
		case LogDebugFmt:
		{
			LogDebugFmtImpl(ppValue[0]->m_String);
			break;
		}
		case LogError:
		{
			LogErrorImpl(ppValue[0]->m_String);
			break;
		}
		case LogErrorFmt:
		{
			LogErrorFmtImpl(ppValue[0]->m_String);
			break;
		}
		case LogWarning:
		{
			LogWarningImpl(ppValue[0]->m_String);
			break;
		}
		case LogWarningFmt:
		{
			LogWarningFmtImpl(ppValue[0]->m_String);
			break;
		}
		case LogInfo:
		{
			LogInfoImpl(ppValue[0]->m_String);
			break;
		}
		case LogInfoFmt:
		{
			LogInfoFmtImpl(ppValue[0]->m_String);
			break;
		}
		case DumpVT:
		{
			DumpVTImpl();
			break;
		}
		case TestContexts:
		{
			TestContextsImpl(ppValue[0]);
			break;
		}
		default:
			return 1;
		};

		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  FindMethod(char const* methodName) const
	{
		int i;

		for (i = 0; i < LastMethod; i++) {
			if (!stricmp(methodName, defFnNames[i].Names[0]))
				return i;
			if (!stricmp(methodName, defFnNames[i].Names[1]))
				return i;
		}

		return -1;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  FindProp(char const* propName) const
	{
		int i;

		for (i = 0; i < LastProp; i++) {
			if (!stricmp(propName, defPropNames[i].Names[0]))
				return i;
			if (!stricmp(propName, defPropNames[i].Names[1]))
				return i;
		}

		return -1;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual char const* GetCode(void) const
	{
		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int GetDestroyUnRefd(void) const
	{
		return 1;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual class CObjID GetID(void) const
	{
		return CObjID::CObjID();
	}

	//////////////////////////////////////////////////////////////////////////
	virtual char const* GetMethodName(int iMethodNum, int iMethodAlias) const
	{
		if (iMethodNum >= LastMethod)
		{
			return 0;
		}
		else
			return defFnNames[iMethodNum].Names[iMethodAlias];
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int GetNMethods(void) const
	{
		return LastMethod;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int GetNParams(int iMethodNum) const
	{
		if (iMethodNum >= LastMethod)
		{
			return -1;
		}
		else
			return defFnNames[iMethodNum].NumberOfParams;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  GetNProps(void) const
	{
		return LastProp;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int GetParamDefValue(int iMethodNum, int iParamNum, class CValue* pDefValue) const
	{
		if (iMethodNum >= LastMethod)
		{
			return 1;
		}

		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual char const* GetPropName(int propNum, int propAlias) const
	{
		if (propNum >= LastProp)
		{
			return 0;
		}
		else
			return defFnNames[propNum].Names[propAlias];
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int GetPropVal(int propNum, class CValue& rValue) const
	{

		if (defPropNames[propNum].IsReadable == FALSE)
			return 1;

		switch (propNum)
		{
		case Version:
			rValue = CValue("1.0.0");
			return 0;
		default:
			break;
		}

		return 1;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual long GetTypeID(void) const
	{
		return 100;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual char const* GetTypeString(void) const
	{
		return "�������������������.�������";
	}

	//////////////////////////////////////////////////////////////////////////
	virtual class CType GetValueType(void) const
	{
		return CType(100);
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  HasRetVal(int iMethodNum) const
	{
		if (iMethodNum >= LastMethod)
		{
			return 0;
		}
		else
			return defFnNames[iMethodNum].HasReturnValue;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  IsExactValue(void) const
	{
		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  IsOleContext(void) const
	{
		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int IsPropReadable(int propNum) const
	{
		return defPropNames[propNum].IsReadable;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  IsPropWritable(int propNum) const
	{
		return defPropNames[propNum].IsWritable;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  IsSerializable(void)
	{
		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int SaveToString(CString& retVal)
	{
		retVal = "DJK";
		return 1;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int SetPropVal(int propNum, class CValue const & val)
	{
		if (defPropNames[propNum].IsWritable == FALSE)
			return 1;

		switch (propNum)
		{
		case Version:
			//imposible
			break;
		default:
			break;
		}
	}

	//////////////////////////////////////////////////////////////////////////
	static struct paramdefs {
		char* Names[2];
		int HasReturnValue;
		int NumberOfParams;
	}  defFnNames[];

	//////////////////////////////////////////////////////////////////////////
	static struct parampropdefs {
		char* Names[2];
		BOOL IsReadable;
		BOOL IsWritable;
	}  defPropNames[];

	//////////////////////////////////////////////////////////////////////////
	enum {
		LogDebug,
		LogDebugFmt,
		LogError,
		LogErrorFmt,
		LogWarning,
		LogWarningFmt,
		LogInfo,
		LogInfoFmt,

		DumpVT,

		TestContexts,

		TestMutex,

		mCompile,

		LastMethod
	};

	//////////////////////////////////////////////////////////////////////////
	enum {
		Version,
		LastProp
	};

};