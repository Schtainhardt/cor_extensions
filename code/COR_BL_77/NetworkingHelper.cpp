#include "stdafx.h"
#include "NetworkingHelper.h"

std::string KeyGenerator::wstring2string(const std::wstring& wstr)
{
	using convert_typeX = std::codecvt_utf8<wchar_t>;
	std::wstring_convert<convert_typeX, wchar_t> converterX;

	return converterX.to_bytes(wstr);
}

std::string KeyGenerator::guid2string(GUID *guid)
{
	char guid_string[37]; // 32 hex chars + 4 hyphens + null terminator
	snprintf(
		guid_string, sizeof(guid_string) / sizeof(guid_string[0]),
		"%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x",
		guid->Data1, guid->Data2, guid->Data3,
		guid->Data4[0], guid->Data4[1], guid->Data4[2],
		guid->Data4[3], guid->Data4[4], guid->Data4[5],
		guid->Data4[6], guid->Data4[7]);
	return guid_string;
}

std::string KeyGenerator::GenerateKey()
{
	// Init dumper
	SMBIOS smbiosDumper = SMBIOS::getInstance();

	PWCHAR type3Id = NULL;
	PWCHAR type3Ver = NULL;

	GUID type2Guid = { 0 };
	PWCHAR type2Id = NULL;

	PWCHAR type1Version = NULL;
	DWORD type1SecVersion = NULL;
	PWCHAR type1Release = NULL;
	PWCHAR type1Vendor = NULL;

	KeyType CheckType = MAX_VAL;

	do
	{

		// TYPE 2
		type2Guid = smbiosDumper.SysUUID();
		if (type2Guid != GUID_NULL)
		{
			CheckType = Type2;
			break;
		}

		// TYPE 3
		type3Id = smbiosDumper.BoardSerialNumber();
		if (type3Id != NULL)
		{
			std::wstring stubChecker(type3Id);
			if (stubChecker.find(L"Filled by O.") == std::wstring::npos)
			{
				CheckType = Type3;
				break;
			}
		}

		// TYPE 1
		type1SecVersion = smbiosDumper.BIOSECVersion();
		type1Version = smbiosDumper.BIOSVersion();
		type1Release = smbiosDumper.BIOSReleaseDate();
		type1Vendor = smbiosDumper.BIOSVendor();

		if (std::wstring(type1Version).find(L"Filled by O.") != std::wstring::npos
			&& std::wstring(type1Release).find(L"Filled by O.") != std::wstring::npos
			&& std::wstring(type1Vendor).find(L"Filled by O.") != std::wstring::npos
			&& type1SecVersion != NULL)
		{
			CheckType = Type1;
			break;
		}

	} while (false);

	Log::DebugFmt("Key type is #", CheckType);

	switch (CheckType)
	{
	case Type1:
	{
		std::wstringstream wideOutKey;
		wideOutKey << type1SecVersion
			<< std::wstring(type1Version)
			<< std::wstring(type1Release)
			<< std::wstring(type1Vendor)
			<< std::endl;

		std::string narrowOutKey = wstring2string(wideOutKey.str());
		std::string outHash = md5(narrowOutKey);
		return outHash;
		break;
	}
	case Type2:
	{
		std::string narrowOutKey = guid2string(&type2Guid);
		std::string outHash = md5(narrowOutKey);
		return outHash;
		break;
	}
	case Type3:
	{
		std::wstring wideOutKey(type3Id);
		std::string outHash(md5(wstring2string(wideOutKey)));
		return outHash;
		break;
	}
	case MAX_VAL:
	default:
	{
		char DllPath[MAX_PATH] = { 0 };
		GetModuleFileName((HINSTANCE)&__ImageBase, DllPath, _countof(DllPath));
		char* pDllPath = &DllPath[0];
		char *p = strrchr(pDllPath, '\\');
		if (p)
		{
			++p;
			p[0] = 0;
		}
		strcat(pDllPath, "COR_UUID.dat");

		if (PathFileExists(pDllPath) == TRUE)
		{
			std::ifstream t(pDllPath);
			std::stringstream buffer;
			buffer << t.rdbuf();
			return md5(buffer.str());
		}
		else
		{
			std::stringstream buffer;
			buffer << std::time(0);
			return md5(buffer.str());
		}

		break;
	}
	}

	return "";
}
