#pragma once
#include "stdafx.h"

//////////////////////////////////////////////////////////////////////////
// CBLModule:: -> Compile
class Hook_Compile
{
public:
	const char* module_name = "blang.dll";
	const char* mangled_func = "?Compile@CBLModule@@QAEHXZ";

	Hook_Compile();

private:

	typedef int(__thiscall *CompileFunc)();
	static CompileFunc orig_Compile;
	static void __stdcall hk_Compile(CBLModule* mod);

};