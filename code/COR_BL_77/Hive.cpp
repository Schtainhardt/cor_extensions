#include "stdafx.h"
#include "Hive.h"
#include "Entry.h"

//////////////////////////////////////////////////////////////////////////
constexpr auto HIVE_RECV = "tcp://127.0.0.1:10400";
constexpr auto HIVE_SEND = "tcp://127.0.0.1:10401";

//////////////////////////////////////////////////////////////////////////
Hive::Hive()
{
	// first 100 ids are reserved for logger, master, exchanger and other services
	ownToken = GetCurrentProcessId() + 100;

	context = new zmq::context_t();
 
 	upstream = new zmq::socket_t(*context, zmq::socket_type::pub);
 	downstream = new zmq::socket_t(*context, zmq::socket_type::sub);
 
 	listenerThread = new std::thread(std::bind(&Hive::Listen, this));
	senderThread = new std::thread(std::bind(&Hive::QueryAndSend, this));

	switch (COR.GetAppMode())
	{
	case ENTERPRISE:
		role = HR_ENTERPRISE;
		break;
	case DESIGNER:
		role = HR_CONFIG;
		break;
	}
}

//////////////////////////////////////////////////////////////////////////
Hive::~Hive()
{
	if (senderThread->joinable()) senderThread->join();
	if (listenerThread->joinable()) listenerThread->join();
}

//////////////////////////////////////////////////////////////////////////
void Hive::Listen()
{
	downstream->connect(HIVE_RECV);
	downstream->setsockopt(ZMQ_SUBSCRIBE, "", 0);
	while (!wantToStop)
	{
		HiveMessage* hms = new HiveMessage(0);
		downstream->recv(hms, HiveMessageHeadSize);

		if (hms->dataLength > 0)
		{
			hms->data = new char[hms->dataLength];
			downstream->recv(&hms->data[0], hms->dataLength);
		}

		ProcessMessage(hms);
	}
}

//////////////////////////////////////////////////////////////////////////
void Hive::QueryAndSend()
{
	upstream->connect(HIVE_SEND);

	while (!wantToStop)
	{
		if (messages.empty())
		{
			std::this_thread::sleep_for(std::chrono::seconds(1));
			continue;
		}

		HiveMessage msg = messages.front();
		upstream->send(msg);
		if (msg.dataLength > 0)
			upstream->send(msg.data, msg.dataLength);

		messages.pop();
	}
}

//////////////////////////////////////////////////////////////////////////
void Hive::PushMessage(HiveMessage hiveMessage)
{
	hiveMessage.senderToken = ownToken;

	messages.push(hiveMessage);
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

HiveRemodeCodeModule::HiveRemodeCodeModule(Hive* hive, std::string code)
{
	hiveHolder = hive;
	remoteCode = code;
}

//////////////////////////////////////////////////////////////////////////
void HiveRemodeCodeModule::Execute()
{
	CBLModule7* mod = new CBLModule7(CBLContext::GetLoadedContext(CBLContext::GetFirstLoadedContextID()), remoteCode.c_str());

	BOOL compiled = mod->Compile();
	if (!compiled)
	{
		nlohmann::json errorReport;
		errorReport["SourceCode"] = remoteCode;

		int errCode = -1; // mod->GetSyntaxErrCode();
		errorReport["ErrorCode"] = errCode;

		char errDescrBuffer[1024];
		CString errDescrString(errDescrBuffer, 1024);
		CBLModule7::GetSyntaxErrDescr(errCode, errDescrString);
		errorReport["ErrorDescription"] = std::string((const char*)errDescrString);

		result = errorReport.dump(4);

		delete mod;
		isDone = true;
		return;
	}

	BOOL executed = mod->Execute();
	if (!executed)
	{
		nlohmann::json errorReport;
		errorReport["SourceCode"] = remoteCode;

		int errCode = mod->GetRuntimeErrCode();
		errorReport["ErrorCode"] = errCode;

		char errDescrBuffer[1024];
		CString errDescrString(errDescrBuffer, 1024);
		mod->GetRuntimeErrDescr(errCode, errDescrString);
		errorReport["ErrorDescription"] = std::string((const char*)errDescrString);

		result = errorReport.dump(4);

		delete mod;
		isDone = true;
		return;
	}

	isSucsess = true;

	nlohmann::json respReport;
	respReport["SourceCode"] = remoteCode;
	respReport["Result"] = std::string("");

	result = respReport.dump(4);

	int varNumber = mod->FindStaticVar("_");
	CValue val;

	BOOL gotReturnValue = mod->GetStaticVarValue(varNumber, val, 0);
	if (!gotReturnValue)
	{
		delete mod;
		isDone = true;
		return;
	}

	char serializationBuffer[32768];
	CString serializationString(serializationBuffer, 32768);
	val.SaveToString(serializationString);

	respReport["Result"] = std::string(serializationBuffer);

	result = respReport.dump(4);

	delete mod;
	isDone = true;
	return;
}