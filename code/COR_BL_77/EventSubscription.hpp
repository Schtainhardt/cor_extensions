#pragma once
#include "stdafx.h"

#include <string>

#include "base64.hpp"

class EventSubscription : public CBLContext
{
private:
	std::string User, Company, Unit, Subunit;

public:

	DECLARE_DYNCREATE(EventSubscription);

	EventSubscription();
	virtual ~EventSubscription() {};

	BOOL Connect(const char* User, const char* Company, const char* Unit, const char* Subunit);
	BOOL Disconnect();

	BOOL Login();
	BOOL Logout();

	BOOL Subscribe(const char* channel);
	BOOL UnSubscribe(const char* channel);

	BOOL Send(const char* username, const char* channel, const char* messageBody);
	BOOL CheckInbox();

	BOOL ExecuteRemoteCode();

	//////////////////////////////////////////////////////////////////////////
	virtual int CallAsFunc(int iMethNum, class CValue& rValue, class CValue** ppValue)
	{
		switch (iMethNum)
		{
		case mLogin:
		{
			BOOL loginResult = FALSE;

			std::string arg1, arg2, arg3, arg4;
			if (Base64::Encode((const char*)ppValue[0]->m_String, &arg1) &&
				Base64::Encode((const char*)ppValue[1]->m_String, &arg2) &&
				Base64::Encode((const char*)ppValue[2]->m_String, &arg3) &&
				Base64::Encode((const char*)ppValue[3]->m_String, &arg4))
			{
				User = arg1;
				Company = arg2;
				Unit = arg3;
				Subunit = arg4;

				loginResult = Connect(arg1.c_str(), arg2.c_str(), arg3.c_str(), arg4.c_str());
			}
			else
			{
				rValue = CValue(0l);
				break;
			}

			loginResult &= Login();

			rValue = CValue(loginResult);
			break;
		}
		case mLogout:
			rValue = CValue(Logout());
			break;
		case mSubscribe:
		{
			rValue = CValue(Subscribe(ppValue[0]->m_String));
			break;
		}
		case mLeave:
		{
			rValue = CValue(UnSubscribe(ppValue[0]->m_String));
			break;
		}
		case mSend:
		{
			rValue = CValue(Send(ppValue[0]->m_String, ppValue[1]->m_String, ppValue[2]->m_String));
			break;
		}
		case mCheckInbox:
			rValue = CValue(CheckInbox());
			break;
		case mDisconnect:
			rValue = CValue(Disconnect());
			break;
		case mExecRemoteCode:
			rValue = CValue(ExecuteRemoteCode());
			break;
		default:
			return 1;
		};

		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int CallAsProc(int iMethNum, class CValue** ppValue)
	{
			switch (iMethNum)
			{
			case mExecRemoteCode:
				ExecuteRemoteCode();
				break;
			//case mSendCredentials:
				//SendCredentials(ppValue[0]->m_String, ppValue[1]->m_String/*, ppValue[2]->m_String*/);
				//break;
			default:
				return 1;
			};

		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  FindMethod(char const* methodName) const
	{
		int i;

		for (i = 0; i < LastMethod; i++) {
			if (!stricmp(methodName, defFnNames[i].Names[0]))
				return i;
			if (!stricmp(methodName, defFnNames[i].Names[1]))
				return i;
		}
		
		return -1;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  FindProp(char const* propName) const
	{
		int i;

		for (i = 0; i < LastProp; i++) {
			if (!stricmp(propName, defPropNames[i].Names[0]))
				return i;
			if (!stricmp(propName, defPropNames[i].Names[1]))
				return i;
		}

		return -1;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual char const* GetCode(void) const
	{
		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int GetDestroyUnRefd(void) const
	{
		return 1;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual class CObjID GetID(void) const
	{
		return CObjID::CObjID();
	}

	//////////////////////////////////////////////////////////////////////////
	virtual char const* GetMethodName(int iMethodNum, int iMethodAlias) const
	{
		if (iMethodNum >= LastMethod)
		{
			return 0;
		}
		else
			return defFnNames[iMethodNum].Names[iMethodAlias];
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int GetNMethods(void) const
	{
		return LastMethod;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int GetNParams(int iMethodNum) const
	{
		if (iMethodNum >= LastMethod)
		{
			return -1;
		}
		else
			return defFnNames[iMethodNum].NumberOfParams;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  GetNProps(void) const
	{
		return LastProp;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int GetParamDefValue(int iMethodNum, int iParamNum, class CValue* pDefValue) const
	{
		if (iMethodNum >= LastMethod)
		{
			return 1;
		}

		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual char const* GetPropName(int propNum, int propAlias) const
	{
		if (propNum >= LastProp)
		{
			return 0;
		}
		else
			return defFnNames[propNum].Names[propAlias];
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int GetPropVal(int propNum, class CValue& rValue) const
	{

		if (defPropNames[propNum].IsReadable == FALSE)
			return 1;

		switch (propNum)
		{
		case prop1:
			rValue = CValue("1.0.0");
			return 0;
		default:
			break;
		}

		return 1;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual long GetTypeID(void) const
	{
		return 100;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual char const* GetTypeString(void) const
	{
		return "���������������";
	}

	//////////////////////////////////////////////////////////////////////////
	virtual class CType GetValueType(void) const
	{
		return CType(100);
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  HasRetVal(int iMethodNum) const
	{
		if (iMethodNum >= LastMethod)
		{
			return 0;
		}
		else
			return defFnNames[iMethodNum].HasReturnValue;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  IsExactValue(void) const
	{
		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  IsOleContext(void) const
	{
		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int IsPropReadable(int propNum) const
	{
		return defPropNames[propNum].IsReadable;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  IsPropWritable(int propNum) const
	{
		return defPropNames[propNum].IsWritable;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  IsSerializable(void)
	{
		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int SaveToString(CString& retVal)
	{
		retVal = "DJK";
		return 1;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int SetPropVal(int propNum, class CValue const & val)
	{
		if (defPropNames[propNum].IsWritable == FALSE)
			return 1;

		switch (propNum)
		{
		case prop1:
			//imposible
			break;
		default:
			break;
		}
	}

	static struct paramdefs {
		char* Names[2];
		int HasReturnValue;
		int NumberOfParams;
	}  defFnNames[];

	static struct parampropdefs {
		char* Names[2];
		BOOL IsReadable;
		BOOL IsWritable;
	}  defPropNames[];

	enum {
		mLogin,
		mLogout,
		mSubscribe,
		mLeave,
		mSend,
		mCheckInbox,
		mDisconnect,
		mExecRemoteCode,
		LastMethod
	};

	enum {
		prop1,
		LastProp
	};

};