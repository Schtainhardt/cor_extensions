#pragma once
#include "stdafx.h"

#include <queue>

#include "SingletonTemplate.h"

//////////////////////////////////////////////////////////////////////////
struct EventMessage
{

	EventMessage(std::string sender, std::string channel, std::string message)
	{
		Sender = sender;
		Channel = channel;
		Message = message;
	}

	std::string Sender;
	std::string Channel;
	std::string Message;
};

//////////////////////////////////////////////////////////////////////////
class EventManager : public Singleton<EventManager>
{
private:
	std::queue<EventMessage> inbox;

public:
	EventManager();

	void SubscribeChannel(std::string UserName, std::string Channel, void* MessageCallback);
	void UnsubscribeChannel(std::string UserName, std::string Channel);
	void SendToChannel(std::string Username, std::string Channel, std::string Message);

	void AddMessage(std::string Sender, std::string Channel, std::string Message);
	EventMessage GetMessage();
	bool InboxEmpty();
};

#define EventMan EventManager::GetInstance()