#include "stdafx.h"
#include "FlagsChecker.hpp"
#include "NetworkingHelper.h"

IMPLEMENT_DYNCREATE(FlagsChecker, CBLContext)

//////////////////////////////////////////////////////////////////////////
//! METHODS DEFINITION
struct FlagsChecker::paramdefs FlagsChecker::defFnNames[] = {

	// name_eng			name_rus				returns val		args count
	{ { "IsConfig",		"�������������������" },	TRUE,			0 },
	{ { "UsersCount",	"��������������������" },	TRUE,			0 },
	{ { "SysKey",		"�������������" },			TRUE,			0 },

	{ { NULL,			NULL },					NULL,			NULL }

};

//////////////////////////////////////////////////////////////////////////
//! FIELDS DEFINITION
struct FlagsChecker::parampropdefs FlagsChecker::defPropNames[] = {

	// name_eng			name_rus				readable		writeable
	{ { "Version",		"������" },				TRUE,			FALSE },
	{ { NULL,			NULL },					NULL,			NULL }

};

//////////////////////////////////////////////////////////////////////////
FlagsChecker::FlagsChecker()
{
	Log::DebugFmt("% @\x0F%@\x07 %", "\t-> FlagsChecker rev.", "1.0", "[29 jan 18]");
}

//////////////////////////////////////////////////////////////////////////
int FlagsChecker::TestMutexImpl()
{
	return CConfigInterface::IsConfigActive();
}

int FlagsChecker::UsersCntImpl()
{
	return CUsersSet::GetUsersCnt();
}

//////////////////////////////////////////////////////////////////////////
const char* FlagsChecker::SysKeyImpl()
{
	std::string systemKey = KeyGenerator::GenerateKey();
	const char* outVal = new char[systemKey.length()+1];
	memcpy((void*)outVal, systemKey.c_str(), systemKey.length());
	return outVal;
}
