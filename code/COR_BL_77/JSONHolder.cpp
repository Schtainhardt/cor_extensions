#include "stdafx.h"
#include "JSONHolder.hpp"
#include "UTF_Tools.h"

IMPLEMENT_DYNCREATE(JSONHolder, CBLContext)

//////////////////////////////////////////////////////////////////////////
//! METHODS DEFINITION
struct JSONHolder::paramdefs JSONHolder::defFnNames[] = {

	// name_eng			name_rus				returns val		args count
	{ { "Load",			"���������" },			TRUE,			1 },
	{ { "Exist",		"����������" },			TRUE,			1 },

	{ { "Length",		"������" },				TRUE,			0 },
	{ { "GetIdx",		"��������" },			TRUE,			1 },

	{ { "Remove",		"�������" },			TRUE,			1 },

	{ { "Dump",			"���������" },			TRUE,			0 },

	{ { "Get",			"����������������" },	TRUE,			1 },
	{ { "Set",			"������������������" },	TRUE,			2 },

	{ { "Select",		"�������" },			TRUE,			0 },
	{ { "Next",			"���������" },			TRUE,			0 },
	{ { "Key",			"����" },				TRUE,			0 },
	{ { "GetVal",		"�������������������������" },	TRUE,	0 },
	{ { "SetVal",		"���������������������������" },TRUE,	1 },

	{ { NULL,			NULL },					NULL,			NULL }

};

//////////////////////////////////////////////////////////////////////////
//! FIELDS DEFINITION
struct JSONHolder::parampropdefs JSONHolder::defPropNames[] = {

	// name_eng			name_rus				readable		writeable
	{ { "Version",		"������" },				TRUE,			FALSE },
	{ { NULL,			NULL },					NULL,			NULL }

};

//////////////////////////////////////////////////////////////////////////
JSONHolder::JSONHolder()
{
	Log::DebugFmt("% @\x0F%@\x07 %", "\t-> JSONHolder rev.", "1.0", "[19 mar 18]");
}

//////////////////////////////////////////////////////////////////////////
JSONHolder::~JSONHolder()
{
}

//////////////////////////////////////////////////////////////////////////
void JSONHolder::__set_container(const nlohmann::json& cont)
{
	jsonHolder = cont;
}

//////////////////////////////////////////////////////////////////////////
BOOL JSONHolder::InitImpl(const char* data)
{
	try
	{
		jsonHolder = nlohmann::json::parse(UTF_Tools::ASCII_To_UTF8(data));
		return TRUE;
	}
	catch (std::exception e)
	{
		Log::ErrorFmt("Error while parsing json data: %", e.what());
		CBLModule::RaiseExtRuntimeError(e.what(), 0);
		return FALSE;
	}
}

//////////////////////////////////////////////////////////////////////////
BOOL JSONHolder::ExistImpl(const char* key)
{
	std::string utfKey = UTF_Tools::ASCII_To_UTF8(key);
	auto foundIt = jsonHolder.find(utfKey);

	return foundIt == jsonHolder.end() ? FALSE : TRUE;
}

//////////////////////////////////////////////////////////////////////////
long JSONHolder::LengthImpl()
{
	if (jsonHolder.is_array() == false)
		return -1;

	return jsonHolder.size();
}

//////////////////////////////////////////////////////////////////////////
void JSONHolder::GetIdxImpl(long idx, CValue& rValue)
{
	if (jsonHolder.is_array() == false)
	{
		rValue = CValue(0l);
		return;
	}

	try
	{

		auto val = jsonHolder.at(idx - 1);
		nlohmann::json::value_t ty = val.type();

		switch (ty)
		{
		case nlohmann::json::value_t::array:
		case nlohmann::json::value_t::object:
		{
			JSONHolder* innerContext = (JSONHolder*)CBLContext::CreateInstance("������JSON");
			innerContext->__set_container(val);
			rValue.AssignContext(innerContext);
			break;
		}
		case nlohmann::json::value_t::string:
		{
			std::string returnValue = UTF_Tools::UTF8_To_ASCII(val.get<std::string>());

			CValue returnStoredVal;
			if (returnValue[0] == (char)BL_STORED_MARKER)
			{
				std::string boxedVal = returnValue.substr(1);
				returnStoredVal.LoadFromString(boxedVal.c_str(), 1);
				rValue = CValue(returnStoredVal);
				break;
			}

			char* retPtr = new char[returnValue.length()];
			memcpy((void*)retPtr, returnValue.c_str(), returnValue.length());
			rValue = CValue(retPtr);
			delete[] retPtr;
			break;
		}
		case nlohmann::json::value_t::boolean:
			rValue = CValue(val == true ? 1 : 0);
			break;
		case nlohmann::json::value_t::number_integer:
		case nlohmann::json::value_t::number_unsigned:
			rValue = CValue((long)val);
			break;
		case nlohmann::json::value_t::number_float:
		{
			CNumeric num((long double)val.get<long double>());
			rValue = CValue(num);
			break;
		}
		case nlohmann::json::value_t::discarded:
		case nlohmann::json::value_t::null:
			rValue = CValue(0l);
			break;
		default:
			break;
		}

		return;
	}
	catch (std::out_of_range& ex)
	{
		CBLModule::RaiseExtRuntimeError("������ ��������� �� ��������� �������", 0);
		rValue = CValue(0l);
		return;
	}
	catch (std::domain_error& ex)
	{
		CBLModule::RaiseExtRuntimeError("������ �� �������� ��������", 0);
		rValue = CValue(0l);
		return;
	}

}

//////////////////////////////////////////////////////////////////////////
BOOL JSONHolder::RemoveImpl(CValue* idx)
{

	if (idx->type > 2 || idx->type < 1)
	{
		CBLModule::RaiseExtRuntimeError("������ �� �������� �������� ��� ��������� ���������", 0);
		return FALSE;
	}

	if (idx->type == 2)
	{
		std::string utfKey = UTF_Tools::ASCII_To_UTF8((const char*)idx->m_String);
		auto foundIt = jsonHolder.find(utfKey);
		if (foundIt == jsonHolder.end())
			return FALSE;

		jsonHolder.erase(foundIt);
		
		return TRUE;
	}

	if (idx->type == 1)
	{
		try
		{
			long targetKey = (long)idx->m_Number;
			jsonHolder.erase(targetKey);
			return TRUE;
		}
		catch (std::out_of_range& ex)
		{
			CBLModule::RaiseExtRuntimeError("������ ��������� �� ��������� �������", 0);
			return FALSE;
		}
		catch (std::domain_error& ex)
		{
			CBLModule::RaiseExtRuntimeError("������ �� �������� ��������", 0);
			return FALSE;
		}
	}

	return FALSE;
}

//////////////////////////////////////////////////////////////////////////
const char* JSONHolder::DumpImpl()
{
	std::string outBuffer = UTF_Tools::UTF8_To_ASCII(jsonHolder.dump(4));

	char* returnValue = new char[outBuffer.length()+1];
	ZeroMemory(returnValue, outBuffer.length()+1);
	memcpy((void*)returnValue, outBuffer.c_str(), outBuffer.length());
	return returnValue;
}

//////////////////////////////////////////////////////////////////////////
void JSONHolder::GetByKeyImpl(const char* idx, CValue& rValue)
{

	std::string utfKey = UTF_Tools::ASCII_To_UTF8(idx);

	try
	{
		auto foundIt = jsonHolder.find(utfKey);
		if (foundIt == jsonHolder.end())
		{
			CBLModule::RaiseExtRuntimeError("���� �� ������", 0);
			rValue = CValue(0l);
			return;
		}

		auto val = foundIt.value();
		nlohmann::json::value_t ty = val.type();

		switch (ty)
		{
		case nlohmann::json::value_t::array:
		case nlohmann::json::value_t::object:
		{
			JSONHolder* innerContext = (JSONHolder*)CBLContext::CreateInstance("������JSON");
			innerContext->__set_container(val);
			rValue.AssignContext(innerContext);
			break;
		}
		case nlohmann::json::value_t::string:
		{
			std::string returnValue = UTF_Tools::UTF8_To_ASCII(val.get<std::string>());

			CValue returnStoredVal;
			if (returnValue[0] == (char)BL_STORED_MARKER)
			{
				std::string boxedVal = returnValue.substr(1);
				returnStoredVal.LoadFromString(boxedVal.c_str(), 1);
				rValue = CValue(returnStoredVal);
				break;
			}

			char* retPtr = new char[returnValue.length()];
			memcpy((void*)retPtr, returnValue.c_str(), returnValue.length());
			rValue = CValue(retPtr);
			delete[] retPtr;
			break;
		}
		case nlohmann::json::value_t::boolean:
			rValue = CValue(val == true ? 1 : 0);
			break;
		case nlohmann::json::value_t::number_integer:
		case nlohmann::json::value_t::number_unsigned:
			rValue = CValue((long)val);
			break;
		case nlohmann::json::value_t::number_float:
		{
			CNumeric num((long double)val.get<long double>());
			rValue = CValue(num);
			break;
		}
		case nlohmann::json::value_t::discarded:
		case nlohmann::json::value_t::null:
			rValue = CValue(0l);
			break;
		default:
			break;
		}

		return;
	}
	catch (std::out_of_range& ex)
	{
		CBLModule::RaiseExtRuntimeError("������ ��������� �� ��������� �������", 0);
		rValue = CValue(0l);
		return;
	}
	catch (std::domain_error& ex)
	{
		CBLModule::RaiseExtRuntimeError("������ �� �������� ��������", 0);
		rValue = CValue(0l);
		return;
	}
}

//////////////////////////////////////////////////////////////////////////
void JSONHolder::SetByKeyImpl(const char* idx, CValue* pVal, CValue& rValue)
{

	CValue val = *pVal;

	std::string utfKey = UTF_Tools::ASCII_To_UTF8(idx);
	auto it = jsonHolder.find(utfKey);
	bool isNewElement = (it == jsonHolder.end());

	// * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	if (val.type == 2)	//string
	{
		if (isNewElement)
		{
			jsonHolder[newElementName] = UTF_Tools::ASCII_To_UTF8((const char*)val.m_String);
			rValue = CValue(1l);
			return;
		}
		else
		{
			it.value() = std::string(UTF_Tools::ASCII_To_UTF8((const char*)val.m_String));
			rValue = CValue(1l);
			return;
		}
	}

	// * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	else if (val.type == 1)	// kind of number
	{
		if (val.m_Number != val.m_Number.GetDouble())	// floating point number
		{
			if (isNewElement)
			{
				jsonHolder[newElementName] = val.m_Number.GetDouble();
				rValue = CValue(1l);
				return;
			}
			else
			{
				it.value() = val.m_Number.GetDouble();
				rValue = CValue(1l);
				return;
			}
		}

		// * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		else	// number
		{
			if (isNewElement)
			{
				jsonHolder[newElementName] = (long)val.m_Number;
				rValue = CValue(1l);
				return;
			}
			else
			{
				it.value() = (long)val.m_Number;
				rValue = CValue(1l);
				return;
			}
		}
	}
	// * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	else if ((val.type == 100) && (strcmp(val.m_Context->GetTypeString(), "������JSON") == 0))
	{
		JSONHolder* innerHolder = (JSONHolder*)val.m_Context;
		if (isNewElement)
		{
			jsonHolder[newElementName] = nlohmann::json(innerHolder->__get_container());
			rValue = CValue(1l);
			return;
		}
		else
		{
			it.value() = nlohmann::json(innerHolder->__get_container());
			rValue = CValue(1l);
			return;
		}
	}
	else
	{
		char serializationBuffer[32768];
		CString serializationString(serializationBuffer, 32768);

		if (isNewElement)
		{
			std::string setVal = std::string("\0");
			setVal += (char)BL_STORED_MARKER;

			CValue* valCopy = new CValue(val);
			valCopy->SaveToString(serializationString);
			setVal += std::string((const char*)serializationString);

			jsonHolder[newElementName] = UTF_Tools::ASCII_To_UTF8(setVal);
			rValue = CValue(1l);
			return;
		}
		else
		{
			std::string setVal = std::string("\0");
			setVal += (char)BL_STORED_MARKER;

			CValue* valCopy = new CValue(val);
			valCopy->SaveToString(serializationString);
			setVal += std::string((const char*)serializationString);

			it.value() = UTF_Tools::ASCII_To_UTF8(setVal);
			rValue = CValue(1l);
			return;
		}
	}

	Log::ErrorFmt("Unreachable place reached at function %", __FUNCTION__);
	DebugBreak();
	return;
}

//////////////////////////////////////////////////////////////////////////
BOOL JSONHolder::InitIteratorImpl()
{
	jsonIterator = jsonHolder.begin();
	if (jsonIterator == jsonHolder.end())
		return FALSE;

	return TRUE;
}

//////////////////////////////////////////////////////////////////////////
BOOL JSONHolder::FetchImpl()
{
	++jsonIterator;
	if (jsonIterator == jsonHolder.end())
		return FALSE;

	return TRUE;
}

//////////////////////////////////////////////////////////////////////////
const char* JSONHolder::IteratorKeyImpl()
{
	if (jsonIterator == jsonHolder.end())
	{
		CBLModule::RaiseExtRuntimeError("�������� ��������������� �� ����� ���������", 0);
		return 0;
	}


	std::string outBuffer = UTF_Tools::UTF8_To_ASCII(jsonIterator.key());
	char* returnValue = new char[outBuffer.length()];
	memcpy((void*)returnValue, outBuffer.c_str(), outBuffer.length());
	return returnValue;
}

//////////////////////////////////////////////////////////////////////////
void JSONHolder::GetIteratorValueImpl(CValue& rValue)
{

	if (jsonIterator == jsonHolder.end())
	{
		CBLModule::RaiseExtRuntimeError("�������� ��������������� �� ����� ���������", 0);
		rValue = CValue(0l);
		return;
	}

	try
	{
		auto foundIt = jsonIterator;

		auto val = foundIt.value();
		nlohmann::json::value_t ty = val.type();

		switch (ty)
		{
		case nlohmann::json::value_t::array:
		case nlohmann::json::value_t::object:
		{
			JSONHolder* innerContext = (JSONHolder*)CBLContext::CreateInstance("������JSON");
			innerContext->__set_container(val);
			rValue.AssignContext(innerContext);
			break;
		}
		case nlohmann::json::value_t::string:
		{
			std::string returnValue(val.get<std::string>());
			returnValue = UTF_Tools::UTF8_To_ASCII(returnValue);

			CValue returnStoredVal;
			if (returnValue[0] == (char)BL_STORED_MARKER)
			{
				std::string boxedVal = returnValue.substr(1);
				returnStoredVal.LoadFromString(boxedVal.c_str(), 1);
				rValue = CValue(returnStoredVal);
				break;
			}

			char* retPtr = new char[returnValue.length()];
			memcpy((void*)retPtr, returnValue.c_str(), returnValue.length());
			rValue = CValue(retPtr);
			delete[] retPtr;
			break;
		}
		case nlohmann::json::value_t::boolean:
			rValue = CValue(val == true ? 1 : 0);
			break;
		case nlohmann::json::value_t::number_integer:
		case nlohmann::json::value_t::number_unsigned:
			rValue = CValue((long)val);
			break;
		case nlohmann::json::value_t::number_float:
		{
			CNumeric num((long double)val.get<long double>());
			rValue = CValue(num);
			break;
		}
		case nlohmann::json::value_t::discarded:
		case nlohmann::json::value_t::null:
			rValue = CValue(0l);
			break;
		default:
			break;
		}

		return;
	}
	catch (std::out_of_range& ex)
	{
		CBLModule::RaiseExtRuntimeError("������ ��������� �� ��������� �������", 0);
		rValue = CValue(0l);
		return;
	}
	catch (std::domain_error& ex)
	{
		CBLModule::RaiseExtRuntimeError("������ �� �������� ��������", 0);
		rValue = CValue(0l);
		return;
	}
}

//////////////////////////////////////////////////////////////////////////
void JSONHolder::SetIteratorValueImpl(CValue* pVal, CValue& rValue)
{
	if (jsonIterator == jsonHolder.end())
	{
		CBLModule::RaiseExtRuntimeError("�������� ��������������� �� ����� ���������", 0);
		rValue = CValue(0l);
		return;
	}

	CValue val = *pVal;
	auto it = jsonIterator;

	// * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	if (val.type == 2)	//string
	{
		it.value() = std::string(UTF_Tools::ASCII_To_UTF8((const char*)val.m_String));
		rValue = CValue(1l);
		return;
	}

	// * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	else if (val.type == 1)	// kind of number
	{
		if (val.m_Number != val.m_Number.GetDouble())	// floating point number
		{
			it.value() = val.m_Number.GetDouble();
			rValue = CValue(1l);
			return;
		}

		// * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		else	// number
		{
			it.value() = (long)val.m_Number;
			rValue = CValue(1l);
			return;
		}
	}
	// * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	else if ((val.type == 100) && (strcmp(val.m_Context->GetTypeString(), "������JSON") == 0))
	{
		JSONHolder* innerHolder = (JSONHolder*)val.m_Context;
		it.value() = nlohmann::json(innerHolder->__get_container());
		rValue = CValue(1l);
		return;
	}
	else
	{
		char serializationBuffer[32768];
		CString serializationString(serializationBuffer, 32768);

		std::string setVal = std::string("");
		setVal += (char)BL_STORED_MARKER;

		CValue* valCopy = new CValue(val);
		valCopy->SaveToString(serializationString);
		setVal += std::string((const char*)serializationString);

		it.value() = UTF_Tools::ASCII_To_UTF8(setVal);
		rValue = CValue(1l);
		return;
	}

	Log::ErrorFmt("Unreachable place reached at function %", __FUNCTION__);
	DebugBreak();
	return;
}
