#pragma once
#include "stdafx.h"

//////////////////////////////////////////////////////////////////////////
// ::GetGlobalModule
class Hook_GetGlobalModule
{
public:
	const char* module_name = "seven.dll";
	const char* mangled_func = "?GetGlobalModule@@YAPAVCBLModule7@@XZ";

	static const char* defaultInitFile;

	Hook_GetGlobalModule();

	void SetGlobalModule(CBLModule7* newGlobalModule);
	void RevertGlobalModule();

	bool IsOriginal(CBLModule7* ptr);

	static Hook_GetGlobalModule* Inst;

	void Start();

	CBLModule7* GetOriginal() const;

private:

	static bool reverted;
	static bool started;

	static CBLModule7* originalGlobalModule;
	static CBLModule7* currentGlobalModule;

	typedef CBLModule7*(__cdecl *GetGlobalModuleFunc)(void);
	static GetGlobalModuleFunc orig_GetGlobalModule;
	static CBLModule7* __cdecl hk_GetGlobalModule();

};
