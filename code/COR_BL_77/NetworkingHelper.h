#pragma once

#include <string>
#include <sstream>
#include <fstream>
#include <codecvt>

#include <Shlwapi.h>

#include "smbios.h"
#include "Log.h"
#include "md5.h"

EXTERN_C IMAGE_DOS_HEADER __ImageBase;

enum KeyType
{
	Type1 = 1,
	Type2,
	Type3,
	MAX_VAL = 0
};

class KeyGenerator
{
private:
	static std::string wstring2string(const std::wstring& wstr);
	static std::string guid2string(GUID *guid);

public:
	static std::string GenerateKey();
};