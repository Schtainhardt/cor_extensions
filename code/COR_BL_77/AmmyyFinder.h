#pragma once
#include "stdafx.h"

#if _WIN32_WINNT > 0x501
class AmmyyFinder
{
private:
	static std::string GetServiceBinaryName(const std::string& ServiceName);

public:
	static std::string GetAmmyyBinary();
};
#endif