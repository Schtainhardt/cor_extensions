#include "stdafx.h"
#include "CApp_ctor.h"

#include "HookTools.h"

#include "Entry.h"

//////////////////////////////////////////////////////////////////////////
// CApp:: -> ctor

Hook_CApp::Hook_CApp()
{
	HOOK_MESSAGE;

	HANDLE processHandle = GetCurrentProcess();
	HMODULE moduleHandle = GetModuleHandleA(module_name);
	LPVOID registrar = GetProcAddress(moduleHandle, mangled_func);

	DWORD OldProtection = 0;
	if (!VirtualProtectEx(processHandle, (LPVOID)((DWORD)registrar-11), 13, PAGE_EXECUTE_READWRITE, &OldProtection))
		Log::Error("Failed");

	char* writer = (char*)registrar - 9;
	*writer++ = 0xE8;
	*((DWORD*)writer) = (DWORD)WriteAppPointerFunc - (DWORD)writer - 4;
	writer += 4;
	*writer++ = 0x6A;
	*writer++ = 0xFF;
	*writer++ = 0xEB;
	*writer++ = 0x02;
	*writer++ = 0xEB;
	*writer++ = 0xF5;
}

Hook_CApp::~Hook_CApp()
{

}

int __stdcall Hook_CApp::WriteAppPointerFunc()
{
	CApp7* ca;
	__asm
	{
		mov ca, ecx
	}
	CCORExtensions::MainApp = ca;
}