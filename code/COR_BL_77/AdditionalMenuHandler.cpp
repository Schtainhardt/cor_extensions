#include "stdafx.h"
#include "AdditionalMenuHandler.h"
#include "Entry.h"
#include "AmmyyFinder.h"
#include "AboutDlgC.h"

enum MenuEntries
{
	ABOUT_EXTENSION = 99990,
	ONLINE_HELP = 99991,
	OPEN_AMMYY = 99992,

	TEST_ZEROMQ = 99993
};

//////////////////////////////////////////////////////////////////////////
CAdditionalMenuHandler::CAdditionalMenuHandler()
{
	AFX_RESOURCE_SCOPE_BEGIN;

	// = COR =
	_rootMenu.CreateMenu();

		_subMenu.CreatePopupMenu();

#if _WIN32_WINNT > 0x501
		// ��������� ���������
		_subMenu.AppendMenu(MF_STRING, OPEN_AMMYY, _T("��������� ���������"));
		std::string res = AmmyyFinder::GetAmmyyBinary();
		if (res.empty()) _subMenu.EnableMenuItem(OPEN_AMMYY, MFS_GRAYED | MF_BYCOMMAND);
#endif

		// zmq
		//_subMenu.AppendMenu(MF_STRING, TEST_ZEROMQ, _T("ZeroMQ"));

		// --
		_subMenu.AppendMenu(MF_SEPARATOR);

		// � �����������...
#ifdef ABOUT_DLG
		_subMenu.AppendMenu(MF_STRING, ABOUT_EXTENSION, _T("� �����������..."));
#endif // ABOUT_DLG

	_rootMenu.AppendMenu(MF_POPUP | MF_STRING, (UINT)_subMenu.m_hMenu, _T("COR"));

	CCORExtensions::MainApp->AddMenu(_rootMenu, 99);
	CCORExtensions::MainApp->UpdateMainFrmMenu();

	AFX_RESOURCE_SCOPE_END;
}

//////////////////////////////////////////////////////////////////////////
void CAdditionalMenuHandler::OnOurMenuCommand()
{
#ifdef ABOUT_DLG
	AFX_RESOURCE_SCOPE_BEGIN;
	CAboutDlgC pDlg(AfxGetApp()->GetMainWnd());
	pDlg.DoModal();
	AFX_RESOURCE_SCOPE_END;
#endif // ABOUT_DLG
	
}

//////////////////////////////////////////////////////////////////////////
void CAdditionalMenuHandler::OnUpdateOurMenuCommand(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(TRUE);
}

//////////////////////////////////////////////////////////////////////////
#if _WIN32_WINNT > 0x501
void CAdditionalMenuHandler::OnOpenAmmyy()
{
	std::string res = AmmyyFinder::GetAmmyyBinary();
	if (res.empty())
	{
		Log::Error("Tried to start Ammyy but executable not found!");
		MessageBoxA(NULL, "������ Ammyy Admin �� �����������!", "COR Extensions", MB_OK | MB_ICONERROR);
		return;
	}

	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	char* appStr = const_cast<char*>(res.c_str());
	if (!CreateProcess(NULL, appStr, NULL, NULL, FALSE, NULL, NULL, NULL, &si, &pi))
	{
		Log::ErrorFmt("Ammyy CreateProcess failed: #", GetLastError());
		return;
	}
}

//////////////////////////////////////////////////////////////////////////
void CAdditionalMenuHandler::OnOpenAmmyyUpdate(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(TRUE);
}
#endif

void shit(void* a, void* b, void* c)
{
	DebugBreak();
}

//////////////////////////////////////////////////////////////////////////
void CAdditionalMenuHandler::OnTest()
{

}

//////////////////////////////////////////////////////////////////////////
void CAdditionalMenuHandler::OnTestUpdate(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(TRUE);
}

//////////////////////////////////////////////////////////////////////////
BEGIN_MESSAGE_MAP(CAdditionalMenuHandler, CModule7)
	//{{AFX_MSG_MAP(CTestApp)
	ON_COMMAND(ABOUT_EXTENSION, OnOurMenuCommand)
	ON_UPDATE_COMMAND_UI(ABOUT_EXTENSION, OnUpdateOurMenuCommand)

#if _WIN32_WINNT > 0x501
	ON_COMMAND(OPEN_AMMYY, OnOpenAmmyy)
	ON_UPDATE_COMMAND_UI(OPEN_AMMYY, OnOpenAmmyyUpdate)
#endif

	ON_COMMAND(TEST_ZEROMQ, OnTest)
	ON_UPDATE_COMMAND_UI(TEST_ZEROMQ, OnTestUpdate)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
CCORExtensionsApp::CCORExtensionsApp()
{
#ifdef BSL_DEV
	m_Module = new CAdditionalMenuHandler();
	m_Module->m_parent = mPWinApp;

	m_Module->Attach((CAppFrame*)mPWinApp);
#endif // BSL_DEV
}

//////////////////////////////////////////////////////////////////////////
CCORExtensionsApp::~CCORExtensionsApp()
{
#ifdef BSL_DEV
	m_Module->Detach((CAppFrame*)mPWinApp);
	delete m_Module;
	m_Module = nullptr;
#endif // BSL_DEV
}