#include "stdafx.h"
#include "CBLModule_Compile.h"

#include "HookTools.h"

#include "Global_GetGlobalModule.h"
#include "SourceConverter.h"


//////////////////////////////////////////////////////////////////////////
// CBLModule:: ->Compile
Hook_Compile::CompileFunc Hook_Compile::orig_Compile;

Hook_Compile::Hook_Compile()
{
	HOOK_MESSAGE;

	HANDLE processHandle = GetCurrentProcess();
	HMODULE moduleHandle = GetModuleHandleA(module_name);
	CompileFunc registrar = (CompileFunc)GetProcAddress(moduleHandle, mangled_func);

	DWORD OldProtection = 0;
	if (!VirtualProtectEx(processHandle, registrar, 16, PAGE_EXECUTE_READWRITE, &OldProtection))
		return;

	// mov ecx, ecx+4 @ 3 bytes
	// jmp ?? @ 5 bytes

	char* writer = (char*)registrar + 4;

	DWORD offset = *(DWORD*)writer;
	offset += 4 + (DWORD)writer;

	orig_Compile = (CompileFunc)offset;

	writer = (char*)registrar;
	
	*writer++ = 0x60;	// pushad
	*writer++ = 0x51;	// push ecx

	*writer++ = 0xE8;	// call...
	*(DWORD*)writer = (DWORD)hk_Compile - (DWORD)writer - 4;
	writer += 4;
	
	*writer++ = 0x61;		// popad
	*writer++ = 0x8B;		// mov ecx, [ecx+4] ; dump of original
	*writer++ = 0x49;
	*writer++ = 0x04;
	*writer++ = 0xE9;		// jmp
	*(DWORD*)writer = (DWORD)orig_Compile - (DWORD)writer - 4;

}

//////////////////////////////////////////////////////////////////////////
void __stdcall Hook_Compile::hk_Compile(CBLModule* mod)
{
	if (mod->pIntInfo->mSource.IsEmpty() == TRUE)
		return;

	Log::DebugFmt("Trying to compile module with ID #...", mod->GetLoadedID());
	return;
}