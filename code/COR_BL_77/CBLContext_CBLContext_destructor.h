#pragma once
#include "stdafx.h"

//////////////////////////////////////////////////////////////////////////
// CBLContext::~CBLContext
class Hook_CBLContext_d
{
public:
	const char* module_name = "bkend.dll";
	const char* mangled_func = "??1CBLContext@@UAE@XZ";

	Hook_CBLContext_d();
	~Hook_CBLContext_d();

private:
	
	char* backupData = nullptr;
	void* registrarBackupAddr = nullptr;

	typedef void(__stdcall *CBLContext_dFunc)();
	static CBLContext_dFunc orig_CBLContext_dFunc;
	static void __stdcall hk_CBLContext_dFunc(CBLContext* context);


};