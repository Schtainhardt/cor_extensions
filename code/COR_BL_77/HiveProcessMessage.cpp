#include "stdafx.h"
#include "Hive.h"
#include "Entry.h"

#define INTERNAL_MESSAGE if (msg->senderToken != ownToken) break
#define TARGET_MESSAGE if (msg->targetToken != ownToken) break

//////////////////////////////////////////////////////////////////////////
void Hive::ProcessMessage(HiveMessage* msg)
{
	switch (msg->type)
	{
	case INTERNAL_SHUTDOWN:
	{
		INTERNAL_MESSAGE;
		wantToStop = true;
		break;
	}
	case TARGET_EXECUTE:
	{
		TARGET_MESSAGE;
		
		HiveMessage hiveMessage;
		std::string responce;
		if (ExecuteRemotely(msg, responce))
		{
			hiveMessage = HiveMessage(responce.c_str(), responce.length());
			hiveMessage.targetToken = msg->senderToken;
			hiveMessage.type = HiveMessageType::TARGET_EXECUTE_RESP;
		}
		else
		{
			hiveMessage = HiveMessage(responce.c_str(), responce.length());
			hiveMessage.targetToken = msg->senderToken;
			hiveMessage.type = HiveMessageType::TARGET_EXECUTE_ERR;

		}
		PushMessage(hiveMessage);
		break;
	}
	case TARGET_EXECUTE_RESP:
	{
		TARGET_MESSAGE;

		Log::DebugFmt("[@\x06HIVE@\x07] TARGET_EXECUTE_RESP:\r\n%", msg->data);
		break;
	}
	case TARGET_EXECUTE_ERR:
	{
		TARGET_MESSAGE;

		Log::WarningFmt("[@\x06HIVE@\x07] TARGET_EXECUTE_ERR:\r\n%", msg->data);
		break;
	}
	case PUBLIC_LIST:
	{
		HiveMessage hiveMessage = HiveMessage(sizeof(HiveMessageAlive));
		hiveMessage.type = HiveMessageType::PNOTIFY_ALIVE;

		HiveMessageAlive* hma = new HiveMessageAlive();
		hma->ProcessID = ownToken-100;
		hma->Role = role;
		memcpy(hiveMessage.data, hma, sizeof(HiveMessageAlive));
		delete hma;

		PushMessage(hiveMessage);

		Log::DebugFmt("[@\x06HIVE@\x07] Pushed PNOTIFY_ALIVE");

		break;
	}
	case NO_ACTION:
		TARGET_MESSAGE;

		Log::DebugFmt("[@\x06HIVE@\x07] Token # requested no action", msg->senderToken);
		break;

	default:
		break;
	}

	if (msg->dataLength > 0)
		delete[] msg->data;

	delete msg;
}