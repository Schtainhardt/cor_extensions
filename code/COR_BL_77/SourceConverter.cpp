#include "stdafx.h"
#include "SourceConverter.h"

SourceConverter::SourceConverter()
{
}


SourceConverter::~SourceConverter()
{
}

bool SourceConverter::LoadSource(const char* source)
{
	version.major = source[7];
	version.minor = source[8];

	const char versionString[] = { version.major, '.', version.minor, 0x00 };

	std::string strSource(source);

	unsigned long totalLength = strSource.length();		// remove \r\n

	UCHAR* hexedSource = (UCHAR*)malloc(totalLength);
	ZeroMemory(hexedSource, totalLength);
	memcpy(hexedSource, source + 10, totalLength - 10);
	
	UCHAR codeSizeBytes[4];
	
	for (int i = 0; i < 4; i++)
	{
		char theByte[3];
		theByte[0] = *hexedSource++;
		theByte[1] = *hexedSource++;
		theByte[2] = 0x00;

		// Fucking endianness!
		codeSizeBytes[3 - i] = StringHexDecode(theByte);
	}

	codeSize = int(codeSizeBytes[0] << 24 | codeSizeBytes[1] << 16 | codeSizeBytes[2] << 8 | codeSizeBytes[3]);

	Log::DebugFmt("Found COR module @\x0bv%@\x07, @\x0E#@\x07 bytes", versionString, codeSize);

	sourceCode = (UCHAR*)malloc(codeSize+1);
	ZeroMemory(sourceCode, codeSize + 1);
	UCHAR* codeWriter = sourceCode;

	const int headerSize = 18;

	if ((totalLength - headerSize) % 2 != 0)
	{
		Log::Error("Got error on stage 1 source decryption. Code size not multiple of two");
		return false;
	}

	for (int i = headerSize; i < totalLength; i += 2)
	{

		if (source[i] == 0x0D || source[i] == 0x0A)
			continue;

		if (!isalnum(source[i]))
		{
			continue;
		}

		char theByte[3];
		theByte[0] = source[i];
		theByte[1] = source[i+1];
		theByte[2] = 0x00;

		// Fucking endianness!
		*codeWriter++ = StringHexDecode(theByte) ^ codeKey[codeKeyReader % 16];
		codeKeyReader++;
	}
	
	codeKeyReader = 0;

	return true;
}

bool SourceConverter::SetSource(CBLModule7* targetModule)
{

	Log::Debug("Assigning...");

	bool result = true;
	try
	{

		CBLModuleInternals* internalModule = ((CBLModule7*)targetModule)->pIntInfo;

		UCHAR** pSourceCode = &sourceCode;
		void** pInternalCall = &internallCall;

		__asm
		{
			mov eax, dword ptr pSourceCode
			push dword ptr [eax]

			mov ecx, [internalModule]

			mov eax, dword ptr pInternalCall
			mov eax, [eax]

			call eax
		}
	}
	catch (CException& mfcException)
	{
		
		TCHAR errorMessage[1024];
		mfcException.GetErrorMessage(errorMessage, 1024);

		Log::ErrorFmt("Error on assigning source:\n%", &errorMessage);
		result = false;
	}
	catch (const std::exception& stdException)
	{

		Log::ErrorFmt("Error on assigning source:\n%", stdException.what());
		result = false;
	}
	return result;
}

const char* SourceConverter::GetSourceCode()
{
	return (char*)sourceCode;
}

bool SourceConverter::IsCrypted(const char* source)
{
	if (source != nullptr && strlen(source) > 0)
	{
		if (source[0] == '/' &&
			source[1] == '/' &&
			source[2] == '#' &&
			source[3] == 'C' &&
			source[4] == 'O' &&
			source[5] == 'R' &&
			source[6] == ' ')
		{
			return true;
		}
	}

	return false;
}

#include <sstream>
unsigned char SourceConverter::StringHexDecode(const char* byteString)
{
	unsigned short byte = 0;
	std::istringstream iss(byteString);
	iss >> std::hex >> byte;
	return byte % 0x100;
}