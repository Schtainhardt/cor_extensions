#pragma once
#include "stdafx.h"

//////////////////////////////////////////////////////////////////////////
// CDBEngDDB7Service:: -> ctor

// NOTICE:
// This hook works only with some kind of DLL loader!

class Hook_CDBEngDDB7Service
{
public:
	const char* module_name = "bkend.dll";
	const char* mangled_func = "??0CDBEngDDB7Service@@QAE@XZ";

	Hook_CDBEngDDB7Service();
	~Hook_CDBEngDDB7Service();

private:

	static int __stdcall WriteAppPointerFunc();

};