#include "stdafx.h"
#include "CBLModule_Execute.h"

#include "HookTools.h"

#include "Global_GetGlobalModule.h"
#include "SourceConverter.h"

//////////////////////////////////////////////////////////////////////////
// CBLModule:: ->LoadSource
Hook_Execute::ExecuteFunc Hook_Execute::orig_Execute;

Hook_Execute::Hook_Execute()
{
	HOOK_MESSAGE;

	HANDLE processHandle = GetCurrentProcess();
	HMODULE moduleHandle = GetModuleHandleA(module_name);
	ExecuteFunc registrar = (ExecuteFunc)GetProcAddress(moduleHandle, mangled_func);

	DWORD OldProtection = 0;
	if (!VirtualProtectEx(processHandle, registrar, 8, PAGE_EXECUTE_READWRITE, &OldProtection))
		return;

	DWORD targetJump = *(DWORD*)((char*)registrar + 4);
	DWORD absJumpAddress = (DWORD)registrar + 8 + targetJump;

	char* DumpAddress = ((char*)registrar);
	char* OriginalFuncAddress = ((char*)registrar) + 8;

	char InstructionDump[8];
	ZeroMemory(InstructionDump, sizeof(InstructionDump));
	memcpy(InstructionDump, DumpAddress, 8);

	LPVOID ReroutedFuncPtr = VirtualAlloc(NULL, 12, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	if (ReroutedFuncPtr == nullptr)
		return;
	ZeroMemory(ReroutedFuncPtr, 12);

	DWORD FinalAddress = (DWORD)hk_Execute - (DWORD)registrar - 5;

	*DumpAddress++ = 0xE9;	// jmp
	*((DWORD*)DumpAddress) = (DWORD)FinalAddress;
	DumpAddress += 4;
	for (size_t i = 0; i < 3; i++)
	{
		*DumpAddress++ = (UCHAR)0x90;
	}

	MemoryBarrier();

	UCHAR* pInstr = (UCHAR*)ReroutedFuncPtr;
	memcpy(pInstr, InstructionDump, 8);

	pInstr += 3;
	*(pInstr) = 0xB8;	// mov eax...
	pInstr++;
	*((DWORD*)pInstr) = absJumpAddress;	// addr
	pInstr += sizeof(DWORD);
	*((WORD*)pInstr) = 0xE0FF;	// jmp eax

	orig_Execute = (ExecuteFunc)ReroutedFuncPtr;

}

//////////////////////////////////////////////////////////////////////////
int __stdcall Hook_Execute::hk_Execute()
{
	CBLModule* thisModule = nullptr;
	// __thiscall hack
	__asm
	{
		mov thisModule, ecx
	}

	Log::Debug("Trying to execute module...");

	__asm
	{
		mov ecx, thisModule
	}

	int originalResult = orig_Execute();

	//if (Hook_GetGlobalModule::Inst->IsOriginal((CBLModule7*)thisModule))
	//{
	//	Hook_GetGlobalModule::Inst->Start();
	//}

	return originalResult;
}