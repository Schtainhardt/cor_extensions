#include "stdafx.h"
#include "Hooker.h"

//////////////////////////////////////////////////////////////////////////
Hooker::Hooker()
{

	// Initialize MinHook.
	//if (MH_Initialize() != MH_OK)
	//{
	//	Log::Error("Error on initializing MinHook!");
	//}
	//else 
	//{
	//	Log::DebugFmt("MinHook initialized, ver. %", "1.33");
	//}

	hkr_RegisterContextClass = new Hook_RegisterContextClass();
	hkr_LoadSource = new Hook_LoadSource();
	hkr_AssignSource = new Hook_AssignSource();
	hkr_GetGlobalModule = new Hook_GetGlobalModule();
	hkr_Execute = new Hook_Execute();
	hkr_Compile = new Hook_Compile();
	
	//hkr_CBLContext = new Hook_CBLContext();

	// BUGGY!
	//hkr_CBLContext_d = new Hook_CBLContext_d();
	
#ifdef BSL_DEV
	hkr_EvalExpr = new Hook_EvalExpr();
	hkr_ExecuteBatch = new Hook_ExecuteBatch();
	hkr_CApp = new Hook_CApp();
#endif // BSL_DEV

	hkr_ProcessAddInEvents = new Hook_ProcessAddInEvents();

}

//////////////////////////////////////////////////////////////////////////
Hooker::~Hooker()
{
	delete hkr_ProcessAddInEvents;

#ifdef BSL_DEV
	delete hkr_CApp;
	delete hkr_ExecuteBatch;
	delete hkr_EvalExpr;
#endif // BSL_DEV

	//delete hkr_CBLContext_d;

	//delete hkr_CBLContext;

	delete hkr_Compile;
	delete hkr_Execute;
	delete hkr_GetGlobalModule;
	delete hkr_AssignSource;
	delete hkr_LoadSource;
	delete hkr_RegisterContextClass;
}