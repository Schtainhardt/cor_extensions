#pragma once

#include <cstdarg>
#include <stdarg.h>

enum MessageColor
{
	WHITE = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE,
	RED = FOREGROUND_RED,
	GREEN = FOREGROUND_GREEN,
	BLUE = FOREGROUND_BLUE,
	YELLOW = FOREGROUND_RED | FOREGROUND_GREEN,
	MAGENTA = FOREGROUND_RED | FOREGROUND_BLUE,
	CYAN = FOREGROUND_GREEN | FOREGROUND_BLUE,

	bWHITE = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY,
	bRED = FOREGROUND_RED | FOREGROUND_INTENSITY,
	bGREEN = FOREGROUND_GREEN | FOREGROUND_INTENSITY,
	bBLUE = FOREGROUND_BLUE | FOREGROUND_INTENSITY,
	bYELLOW = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY,
	bMAGENTA = FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_INTENSITY,
	bCYAN = FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY,		// \x0b

	DEFAULT = 0xff
};

class Log
{
public:
	Log();
	~Log();

private:
	static Log* globalLog;
	CRITICAL_SECTION logProtector;

	static WORD defaultColor;
	void SetColor(MessageColor newColor);
	void RestoreColor();

	void WriteText(const char* message);
	void WriteText(const char* message, va_list args);

public:
	static void Debug(const char* message);
	static void DebugFmt(const char* message, ...);
	static void Info(const char* message);
	static void InfoFmt(const char* message, ...);
	static void Warning(const char* message);
	static void WarningFmt(const char* message, ...);
	static void Error(const char* message);
	static void ErrorFmt(const char* message, ...);
};