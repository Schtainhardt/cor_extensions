#pragma once
#include "stdafx.h"

//#include <minhook/MinHook.h>

//////////////////////////////////////////////////////////////////////////
// CBkEndUI:: -> DoMessageLine
class Hook_CustomDoMessageLine
{
public:
	const char* module_name = "bkend.dll";
	const char* mangled_func = "?DoMessageLine@CBkEndUI@@UAEXPBDW4MessageMarker@@@Z";

	LPVOID battlefield = nullptr;

	Hook_CustomDoMessageLine();
	~Hook_CustomDoMessageLine();

private:

	typedef int(__thiscall *CustomDoMessageLineFunc)(char const * msg, enum MessageMarker mrk);
	static CustomDoMessageLineFunc orig_CustomDoMessageLine;
	static int __stdcall hk_CustomDoMessageLine(char const * msg, enum MessageMarker mrk);

};