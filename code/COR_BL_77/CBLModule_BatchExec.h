#pragma once
#include "stdafx.h"

//////////////////////////////////////////////////////////////////////////
// CBLModule7:: -> TryExecuteBatch
class Hook_ExecuteBatch
{

public:
	const char* module_name = "Seven.dll";
	const char* mangled_func = "?TryExecuteBatch@CBLModule7@@QAEHPBDPAPAVCValue@@HH@Z";

	Hook_ExecuteBatch();

private:
	typedef int(__thiscall *TryExecuteBatchFunc)(const char* code, class CValue** args);
	static TryExecuteBatchFunc orig_TryExecuteBatch;
	static int __stdcall hk_TryExecuteBatch(const char* code, class CValue** args);

};