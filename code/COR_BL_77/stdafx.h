#pragma once

#include "targetver.h"
#include "MemManager.h"
#include "LibraryLinkage.h"

#include "resource.h"

//#define NEW_MFC_ATL

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS
// //#define _AFX_NO_MFC_CONTROLS_IN_DIALOGS
#define _AFXDLL

//////////////////////////////////////////////////////////////////////////
// AFX
#include <afx.h>
#include <afxwin.h>
#include <afxtempl.h>
#include <afxext.h>
#include <afxcmn.h>

#ifdef NEW_MFC_ATL
#include <afxstr.h>
#endif // NEW_MFC_ATL

#include <afxcview.h>
#include <afxctl.h>

//////////////////////////////////////////////////////////////////////////
// std
#include <iostream>

//////////////////////////////////////////////////////////////////////////
// WinAPI
#include <windows.h>

//////////////////////////////////////////////////////////////////////////
// ATL
#include <atlbase.h>

#ifdef NEW_MFC_ATL
#include <atlstr.h>
#endif // NEW_MFC_ATL

//////////////////////////////////////////////////////////////////////////
// General

#ifdef NEW_MFC_ATL
// -- MFC 4.2 emulation --

#undef _DECLARE_DYNAMIC
#undef DECLARE_DYNAMIC

#define DECLARE_DYNAMIC(class_name) \
protected: \
	static CRuntimeClass* PASCAL _GetBaseClass(); \
public: \
	static const CRuntimeClass class##class_name; \
	virtual CRuntimeClass* GetRuntimeClass() const; \

#define _DECLARE_DYNAMIC(class_name) \
protected: \
	static CRuntimeClass* PASCAL _GetBaseClass(); \
public: \
	static CRuntimeClass class##class_name; \
	virtual CRuntimeClass* GetRuntimeClass() const; \

#undef _IMPLEMENT_RUNTIMECLASS
#undef IMPLEMENT_RUNTIMECLASS
#undef IMPLEMENT_DYNCREATE

#define IMPLEMENT_RUNTIMECLASS(class_name, base_class_name, wSchema, pfnNew, class_init) \
	CRuntimeClass* PASCAL class_name::_GetBaseClass() \
		{ return RUNTIME_CLASS(base_class_name); } \
	AFX_COMDAT const CRuntimeClass class_name::class##class_name = { \
		#class_name, sizeof(class class_name), wSchema, pfnNew, \
			&class_name::_GetBaseClass, NULL, class_init }; \
	CRuntimeClass* class_name::GetRuntimeClass() const \
		{ return _RUNTIME_CLASS(class_name); }

#define _IMPLEMENT_RUNTIMECLASS(class_name, base_class_name, wSchema, pfnNew, class_init) \
	CRuntimeClass* PASCAL class_name::_GetBaseClass() \
		{ return RUNTIME_CLASS(base_class_name); } \
	AFX_COMDAT CRuntimeClass class_name::class##class_name = { \
		#class_name, sizeof(class class_name), wSchema, pfnNew, \
			&class_name::_GetBaseClass, NULL, class_init }; \
	CRuntimeClass* class_name::GetRuntimeClass() const \
		{ return _RUNTIME_CLASS(class_name); }

#define IMPLEMENT_DYNCREATE(class_name, base_class_name) \
	CObject* PASCAL class_name::CreateObject() \
		{ return new class_name; } \
	IMPLEMENT_RUNTIMECLASS(class_name, base_class_name, 0xFFFF, \
		class_name::CreateObject, NULL)

// -----------------------
#endif // NEW_MFC_ATL

#define AFX_RESOURCE_SCOPE_BEGIN AFX_MODULE_STATE* ams = AfxGetModuleState(); \
	AFX_MANAGE_STATE(AfxGetStaticModuleState()); \
	AFX_MODULE_STATE* ams2 = AfxGetModuleState(); \
	CWinApp* amsBackup = ams2->m_pCurrentWinApp; \
	ams2->m_pCurrentWinApp = ams->m_pCurrentWinApp

#define AFX_RESOURCE_SCOPE_END ams2->m_pCurrentWinApp = amsBackup

#include <curl/curl.h>
#include "json.hpp"

#include "zmq.hpp"

#include "1cheaders.h"
#include "Log.h"
#include <afxwin.h>
