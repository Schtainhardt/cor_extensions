#pragma once
#include "stdafx.h"

//////////////////////////////////////////////////////////////////////////
// CBLContext::CBLContext
class Hook_CBLContext
{
public:
	const char* module_name = "bkend.dll";
	const char* mangled_func = "??0CBLContext@@QAE@H@Z";

	Hook_CBLContext();
	~Hook_CBLContext();

private:

	char* backupData = nullptr;
	void* registrarBackupAddr = nullptr;

	typedef void*(__stdcall *CBLContextFunc)();
	static void __stdcall hk_CBLContext(CBLContext* thisContext);

};