#include "stdafx.h"

#include "EventSubscription.hpp"
#include <string.h>

#include "Networking.h"
#include "EventManager.h"
#include "base64.hpp"
#include "Entry.h"

IMPLEMENT_DYNCREATE(EventSubscription, CBLContext)

//////////////////////////////////////////////////////////////////////////
//! METHODS DEFINITION
struct EventSubscription::paramdefs EventSubscription::defFnNames[] = {

	// name_eng			name_rus				returns val		args count
	{ { "Login",		"�����" },				TRUE,			4 },
	{ { "Logout",		"�����" },				TRUE,			0 },
	{ { "Join",			"�����������" },		TRUE,			1 },
	{ { "Leave",		"����������" },			TRUE,			1 },
	{ { "Send",			"���������" },			TRUE,			3 },
	{ { "CheckInbox",	"����������" },			TRUE,			0 },
	{ { "Disconnect",	"�����������" },		TRUE,			0 },
	{ { "ExecRemote",	"����������������������" },	TRUE,		0 },
	{ { NULL,			NULL },					NULL,			NULL }

};

//////////////////////////////////////////////////////////////////////////
//! FIELDS DEFINITION
struct EventSubscription::parampropdefs EventSubscription::defPropNames[] = {

// name_eng			name_rus				readable		writeable
	{ { "Version",	"������" },				TRUE,			FALSE },
	{ { NULL,		NULL },					NULL,			NULL }

};

//////////////////////////////////////////////////////////////////////////
EventSubscription::EventSubscription()
{
	Log::DebugFmt("% @\x0F%@\x07 %", "\t-> EventSubscription rev.", "1.0", "[03 dec 17]");
}

//////////////////////////////////////////////////////////////////////////
BOOL EventSubscription::Login()
{
	return NetworkMan.MessagingLogin();
}

//////////////////////////////////////////////////////////////////////////
BOOL EventSubscription::Logout()
{
	return NetworkMan.MessagingLogout();
}

//////////////////////////////////////////////////////////////////////////
BOOL EventSubscription::Subscribe(const char* channel)
{
	std::string arg;
	if (Base64::Encode(channel, &arg))
		return NetworkMan.MessagingSub(arg);
	else
	{
		Log::DebugFmt("Error on base64ing text\n\t%\non %", channel, __FUNCTION__);
		return FALSE;
	}
}

//////////////////////////////////////////////////////////////////////////
BOOL EventSubscription::UnSubscribe(const char* channel)
{
	std::string arg;
	if (Base64::Encode(channel, &arg))
		return NetworkMan.MessagingLeave(arg);
	else
	{
		Log::DebugFmt("Error on base64ing text\n\t%\non %", channel, __FUNCTION__);
		return FALSE;
	}
}

//////////////////////////////////////////////////////////////////////////
BOOL EventSubscription::Send(const char* username, const char* channel, const char* messageBody)
{
	std::string arg1, arg2, arg3;
	if (Base64::Encode(username, &arg1) &&
		Base64::Encode(channel, &arg2) &&
		Base64::Encode(messageBody, &arg3))
	{
		return NetworkMan.MessagingSend(arg1, arg2, arg3);
	}
	else
	{
		Log::DebugFmt("Error on base64ing text\n\t% | % | %\non %", username, channel, messageBody, __FUNCTION__);
		return FALSE;
	}
}

//////////////////////////////////////////////////////////////////////////
BOOL EventSubscription::CheckInbox()
{
	MessagePacket* msg = nullptr;
	BOOL result = NetworkMan.MessagingCheckInbox(&msg);
	if (msg != nullptr)
	{
		int procNum = GetGlobalModule()->FindProc("�����������������������", 0);
		if (procNum == 0)
		{
			Log::Info("CheckInbox => Inbox handler not found in global module!");
			return FALSE;
		}

		CValue** params = new CValue*[3]();

		std::string arg1, arg2, arg3;

		if (Base64::Decode(msg->name, &arg1) &&
			Base64::Decode(msg->channel, &arg2) &&
			Base64::Decode(msg->text, &arg3))
		{
			params[0] = new CValue(arg1.c_str());
			params[1] = new CValue(arg2.c_str());
			params[2] = new CValue(arg3.c_str());

			GetGlobalModule()->CallAsProc(procNum, 3, params);
			
			delete params[2];
			delete params[1];
			delete params[0];
			delete[] params;
			return TRUE;
		}
		else
		{
			Log::ErrorFmt("Error on debas64ing message!\n\t%\n\t\t%\n\t%\n\t\t%\n\t%\n\t\t%", 
				"Name:", msg->name.c_str(), 
				"Channel:",	msg->channel.c_str(), 
				"Text:", msg->text.c_str());

			delete params[2];
			delete params[1];
			delete params[0];
			delete[] params;

			return FALSE;
		}
	}
	return result;
}

//////////////////////////////////////////////////////////////////////////
BOOL EventSubscription::ExecuteRemoteCode()
{
	COR.hive->DoRemote();

	return TRUE;
}

//////////////////////////////////////////////////////////////////////////
BOOL EventSubscription::Connect(const char* User, const char* Company, const char* Unit, const char* Subunit)
{
	return NetworkMan.Connect(User, Company, Unit, Subunit);
}

BOOL EventSubscription::Disconnect()
{
	return NetworkMan.Disconnect();
}