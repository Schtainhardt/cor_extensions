#pragma once

class SourceConverter
{
private:
	int codeKeyReader = 0;
	const char* codeKey = "4vxT7Ae8F9tDq9YW";

	unsigned char* sourceCode = nullptr;

	struct 
	{
		char major;
		char minor;
	} version;

	int codeSize = 0;

public:

	void* internallCall = nullptr;

	SourceConverter();
	~SourceConverter();

	bool LoadSource(const char* source);
	bool SetSource(CBLModule7* targetModule);

	const char* GetSourceCode();

	static bool IsCrypted(const char* source);
	static unsigned char StringHexDecode(const char* byteString);
};

