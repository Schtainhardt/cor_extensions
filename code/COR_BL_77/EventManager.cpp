#include "stdafx.h"
#include "EventManager.h"

//////////////////////////////////////////////////////////////////////////
REGISTER_SINGLETON(EventManager);

//////////////////////////////////////////////////////////////////////////
EventManager::EventManager()
{
	CONSTRUCT_SINGLETON(EventManager);
}

//////////////////////////////////////////////////////////////////////////
void EventManager::SubscribeChannel(std::string UserName, std::string Channel, void* MessageCallback)
{
	//::SubscribeChannel(UserName, Channel, MessageCallback);
}

//////////////////////////////////////////////////////////////////////////
void EventManager::UnsubscribeChannel(std::string UserName, std::string Channel)
{
	//::UnsubscribeChannel(UserName, Channel);
}

//////////////////////////////////////////////////////////////////////////
void EventManager::SendToChannel(std::string Username, std::string Channel, std::string Message)
{
	//::SendToChannel(Username, Channel, Message);
}

//////////////////////////////////////////////////////////////////////////
void EventManager::AddMessage(std::string Sender, std::string Channel, std::string Message)
{
	inbox.push(EventMessage(Sender, Channel, Message));
}

//////////////////////////////////////////////////////////////////////////
EventMessage EventManager::GetMessage()
{
	EventMessage outMessage = inbox.front();
	inbox.pop();
	return outMessage;
}

//////////////////////////////////////////////////////////////////////////
bool EventManager::InboxEmpty()
{
	return inbox.size() == 0;
}
