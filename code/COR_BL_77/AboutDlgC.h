#pragma once
#include "stdafx.h"

//////////////////////////////////////////////////////////////////////////
#ifdef ABOUT_DLG
class CAboutDlgC : public CDialog
{
public:
	CAboutDlgC(CWnd* parent = NULL);

	virtual BOOL OnInitDialog() override;

protected:
	virtual void DoDataExchange(CDataExchange* pDX); 

private:
	int AppendToRich(CRichEditCtrl& m_ctrlLog, CString str, COLORREF color = 0, bool isbold = false, long textsize = 0);

protected:
	DECLARE_MESSAGE_MAP()
};
#endif // ABOUT_DLG