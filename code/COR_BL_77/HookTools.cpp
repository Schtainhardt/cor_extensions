#include "stdafx.h"

#include <algorithm>
#include <functional>
#include <vector>

#include "HookTools.h"
#include "Log.h"

#include "bslLibraryFunctions.h"

//////////////////////////////////////////////////////////////////////////
int HookExplorer::Rebuild(const char* moduleName, const char* functions[])
{
	HMODULE moduleHandle = GetModuleHandleA(moduleName);
	int foundFunctionsCount = 0;
	//int functionsCount = sizeof(functions) / sizeof(functions[0]);
	//for (size_t i = 0; i < functionsCount; i++)
	//{
	//	FunctionDefinition functionDefinition;
	//	functionDefinition.pointer = GetProcAddress(moduleHandle, functions[i]);
	//	functionDefinition.moduleName = moduleName;
	//
	//	vtable.insert_or_assign(functions[i], functionDefinition);
	//	if (functionDefinition.pointer != nullptr)
	//	{
	//		foundFunctionsCount++;
	//	}
	//}

	return foundFunctionsCount;
}

//////////////////////////////////////////////////////////////////////////
HookExplorer::HookExplorer()
{

}

//////////////////////////////////////////////////////////////////////////
void HookExplorer::RebuildVirtualTable()
{

	int functionsFound = 0;

	Log::Debug("Rebuilding virtual table...");
	functionsFound = Rebuild("bkend.dll", bkendFunctions);
	Log::DebugFmt(" -> @\x02 BkEnd.dll@\x07\n\t\t ...found @\x06 # @\x07 functions", functionsFound);
	functionsFound = Rebuild("seven.dll", sevenFunctions);
	Log::DebugFmt(" -> @\x02 Seven.dll@\x07\n\t\t ...found @\x06 # @\x07 functions", functionsFound);
	functionsFound = Rebuild("basic.dll", basicFunctions);
	Log::DebugFmt(" -> @\x02 Basic.dll@\x07\n\t\t ...found @\x06 # @\x07 functions", functionsFound);
	functionsFound = Rebuild("blang.dll", blangFunctions);
	Log::DebugFmt(" -> @\x02 BLang.dll@\x07\n\t\t ...found @\x06 # @\x07 functions", functionsFound);
}

//////////////////////////////////////////////////////////////////////////
// Comparator for by-pointer asc sort
template <typename T1, typename T2>
struct less_second {
	typedef std::pair<T1, T2> type;
	bool operator ()(type const& a, type const& b) const {
		return a.second.pointer < b.second.pointer;
	}
};

//////////////////////////////////////////////////////////////////////////
void HookExplorer::DumpVirtualTable()
{

	Log::Debug("= = = = = = = = = = = =");
	Log::Debug("=     VTABLE DUMP     =");
	Log::Debug("= = = = = = = = = = = =");
	Log::Debug("");

	std::vector<std::pair<const char*, FunctionDefinition> > mapcopy(vtable.begin(), vtable.end());
	std::sort(mapcopy.begin(), mapcopy.end(), less_second<const char*, FunctionDefinition>());

	for (std::vector<std::pair<const char*, FunctionDefinition> >::iterator it = mapcopy.begin(); it != mapcopy.end(); ++it)
	{
		Log::DebugFmt("@\x02{$}@\x07\t@\x06%@\x07\t%", it->second.pointer, it->second.moduleName, it->first);
	}

	Log::Debug("");
	Log::Debug("= = = = = = = = = = = =");
	Log::Debug("");

}
