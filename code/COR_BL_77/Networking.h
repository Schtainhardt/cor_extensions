#pragma once

#include <string>
#include <rpc/client.h>

#define NetworkMan Networking::getInstance()

enum NetworkingConnectState
{
	NCS_FAILED,
	NCS_CONNECTED,
	NCS_PENDING,
	MAXVAL
};
///////////////////////////////////////////////
struct MessagePacket
{
	std::string name;
	std::string channel;
	std::string text;
};

//////////////////////////////////////////////////////////////////////////
class Networking
{
private:
	static Networking * p_instance;
	Networking();
	Networking(const Networking&);
	Networking& operator=(Networking&);

	rpc::client* clientRPC = nullptr;

	std::vector<std::string> serversList;
	
	std::string clientKey;

	std::string token;
	std::vector<MessagePacket> inbox;

	bool connected;

private:
	inline void _CheckConnection();
	bool _Connect();
	bool _GetToken();
	bool _ServerLogin(std::string userName, std::string Company, std::string Unit, std::string Subunit);

public:
	
	void CheckConnection();

	bool Connect(std::string userName, std::string Company, std::string Unit, std::string Subunit);
	bool Disconnect();

	bool MessagingLogin();
	bool MessagingLogout();

	bool MessagingSub(std::string channelName);
	bool MessagingLeave(std::string channelName);

	bool MessagingSend(std::string user, std::string channel, std::string text);
	bool MessagingCheckInbox(MessagePacket** outMessagePacket);

	static Networking& getInstance() {
		if (!p_instance)
			p_instance = new Networking();
		return *p_instance;
	}

	void SendCredentials(std::string companyName, std::string unitName);

	static void __stdcall OnBeforeUpdate(std::string Version);

};