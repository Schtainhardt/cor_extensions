#include "stdafx.h"
#include "CBLModule_AssignSource.h"

#include "HookTools.h"

#include "SourceConverter.h"

#include <fstream>
#include <sstream>

//////////////////////////////////////////////////////////////////////////
// CBLModule:: -> AssignSource
Hook_AssignSource::AssignSourceFunc Hook_AssignSource::orig_AssignSource;
void* Hook_AssignSource::AssignSourceInternalCall;

Hook_AssignSource::Hook_AssignSource()
{
	HOOK_MESSAGE;

	HANDLE processHandle = GetCurrentProcess();
	HMODULE moduleHandle = GetModuleHandleA(module_name);
	AssignSourceFunc registrar = (AssignSourceFunc)GetProcAddress(moduleHandle, mangled_func);

	orig_AssignSource = (AssignSourceFunc)HookFunctionThiscall<8, 5>(processHandle, registrar, hk_AssignSource, &AssignSourceInternalCall);
}


int __stdcall Hook_AssignSource::hk_AssignSource(const char* sourceCode)
{
	CBLModuleInternals* thisModuleInternals = nullptr;
	CBLModule7* thisModule = nullptr;
	// __thiscall hack
	__asm
	{
		mov thisModuleInternals, ecx
	}

	thisModule = (CBLModule7*)thisModuleInternals->pModule;

	if (SourceConverter::IsCrypted(sourceCode))
	{
		SourceConverter* converter = new SourceConverter();
		if (converter->LoadSource(sourceCode) == false)
		{
			Log::Error("Error on loading crypted source code! Source will stay unassigned");
			return 0;
		}

		converter->internallCall = AssignSourceInternalCall;
		return !converter->SetSource(thisModule);
	}

	Log::DebugFmt("Trying to load source: { \n@\x0b%@\x07\n}", sourceCode);

	return 1;
}