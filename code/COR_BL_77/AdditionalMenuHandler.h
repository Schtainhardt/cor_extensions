#pragma once
#include "stdafx.h"

//////////////////////////////////////////////////////////////////////////
class CAdditionalMenuHandler : public CModule7
{
private:
	CMenu _rootMenu;
	CMenu _subMenu;

public:
	CWinApp* m_parent = nullptr;

	CAdditionalMenuHandler();
	virtual ~CAdditionalMenuHandler() {};

	//{{AFX_MSG(CAdditionalMenuHandler)
	afx_msg void OnOurMenuCommand();
	afx_msg void OnUpdateOurMenuCommand(CCmdUI* pCmdUI);
	
#if _WIN32_WINNT > 0x501
	afx_msg void OnOpenAmmyy();
	afx_msg void OnOpenAmmyyUpdate(CCmdUI* pCmdUI);
#endif

	afx_msg void OnTest();
	afx_msg void OnTestUpdate(CCmdUI* pCmdUI);

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//////////////////////////////////////////////////////////////////////////
class CCORExtensionsApp /*: public CWinApp*/
{
private:
	CWinApp* mPWinApp = nullptr;

public:
	CAdditionalMenuHandler* m_Module = nullptr;
	CCORExtensionsApp();
	~CCORExtensionsApp();
};