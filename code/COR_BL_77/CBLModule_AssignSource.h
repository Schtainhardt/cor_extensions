#pragma once
#include "stdafx.h"

//////////////////////////////////////////////////////////////////////////
// CBLModule:: -> AssignSource
class Hook_AssignSource
{

public:
	const char* module_name = "blang.dll";
	const char* mangled_func = "?AssignSource@CBLModule@@QAEXPBD@Z";

	Hook_AssignSource();

private:
	typedef void(__thiscall *AssignSourceFunc)(const char* sourceCode);
	static AssignSourceFunc orig_AssignSource;
	static void* AssignSourceInternalCall;
	static int __stdcall hk_AssignSource(const char* sourceCode);

};