#include "stdafx.h"
#include "StrMatchContext.hpp"

IMPLEMENT_DYNCREATE(StrMatch, CBLContext)

//////////////////////////////////////////////////////////////////////////
//! METHODS DEFINITION
struct StrMatch::paramdefs StrMatch::defFnNames[] = {

	// name_eng			name_rus				returns val		args count
	{ { "AddToCash",	"������������" },		FALSE,			4 },
	{ { "CmpNext",		"���������������" },	FALSE,			3 },
	{ { "GetItemSet",	"�����������������" },	TRUE,			3 },
	{ { "CmpWithCash",	"��������������" },		FALSE,			3 },
	{ { "StrMatch",		"��������" },			TRUE,			2 },
	{ { "OpenSet",		"�������������" },		FALSE,			2 },
	{ { "CreateCash",	"����������" },			TRUE,			1 },
	{ { "ClearCash",	"�����������" },		FALSE,			1 },
	{ { "DeleteCash",	"����������" },			FALSE,			1 },
	{ { NULL,			NULL },					NULL,			NULL }

};

//////////////////////////////////////////////////////////////////////////
//! FIELDS DEFINITION
struct StrMatch::parampropdefs StrMatch::defPropNames[] = {

	// name_eng			name_rus				readable		writeable
	{ { "Version",		"������" },				TRUE,			FALSE },
	{ { NULL,			NULL },					NULL,			NULL }

};