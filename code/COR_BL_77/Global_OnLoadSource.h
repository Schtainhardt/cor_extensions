#pragma once
#include "stdafx.h"

//////////////////////////////////////////////////////////////////////////
// ::OnLoadSource
class Hook_OnLoadSource
{
public:
	const char* module_name = "blang.dll";
	const char* mangled_func = "?OnLoadSource@@YAHPAVCString@@@Z";

	Hook_OnLoadSource();

private:

	typedef int(__cdecl *OnLoadSourceFunc)(CString str1);
	static OnLoadSourceFunc orig_OnLoadSource;
	static int __cdecl hk_OnLoadSource(CString str1);

};