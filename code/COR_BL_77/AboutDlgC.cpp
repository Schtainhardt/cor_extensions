#include "stdafx.h"
#include "AboutDlgC.h"

#ifdef ABOUT_DLG
BEGIN_MESSAGE_MAP(CAboutDlgC, CDialog)
END_MESSAGE_MAP()

//////////////////////////////////////////////////////////////////////////
CAboutDlgC::CAboutDlgC(CWnd* parent) : CDialog(IDD_DIALOG1, parent)
{
}

//////////////////////////////////////////////////////////////////////////
BOOL CAboutDlgC::OnInitDialog()
{
	AfxInitRichEdit();

	CRichEditCtrl* rec = (CRichEditCtrl*)GetDlgItem(IDC_RICHEDIT21);

	AppendToRich(*rec, "\"��������\" 7.7.4.21\r\n", RGB(110, 110, 255), true, 396);
	AppendToRich(*rec, "\r\n", 0, false, 64);
	AppendToRich(*rec, "���������� ����������������\r\n", RGB(128, 128, 255), true, 256);
	AppendToRich(*rec, "������������ ��� �������\r\n", RGB(128, 128, 255), true, 256);
	AppendToRich(*rec, "\r\n", 0, false, 128);
	AppendToRich(*rec, "����������� ��� \"�-�����\" � 2002-2018\r\n", 0, true, 192);
	AppendToRich(*rec, "\r\n", 0, false, 256);
	AppendToRich(*rec, "COR BL Extensions v2.0.527\r\n", RGB(186, 128, 128), true, 256);
	AppendToRich(*rec, "\r\n", 0, false, 64);
	AppendToRich(*rec, "Developed by COR in 2017-2018\r\n", 0, true, 192);
	AppendToRich(*rec, "\r\n");
	AppendToRich(*rec, "���������� ����, ��� �������� �������� ������� � ����������,\r\n � ����� "
		"�������� ����������, ��������������\r\n � ��������� ���������:\r\n");
	AppendToRich(*rec, "\r\nSchtainhardt\r\nTim\r\nGiperion\r\nacidicMercury8\r\n���������� 1cpp.ru � �������� ����, fez, orefkov, Scorp\r\n... � ���, ���� ��� � ���� ������\r\n", RGB(0, 0, 0), false, 0);
	AppendToRich(*rec, "\r\n", RGB(0, 0, 0), false, 0);
	AppendToRich(*rec, "������������ ���������� rpclib, JSON for Modern C++,\r\nFast Base64, MD5 by Frank Thilo, inih by Ben Hoyt", RGB(0, 0, 0), false, 0);
	return TRUE;
}

//////////////////////////////////////////////////////////////////////////
int CAboutDlgC::AppendToRich(CRichEditCtrl& m_ctrlLog, CString str, COLORREF color, bool isbold, long textsize)
{
	long nInsertionPoint = 0;
	CHARRANGE cr;
	CHARFORMAT cf;

	// Initialize character format structure
	cf.cbSize = sizeof(CHARFORMAT);
	cf.dwMask = CFM_COLOR;
	cf.dwEffects = 0;

	if (isbold)
	{
		cf.dwMask |= CFM_BOLD;
		cf.dwEffects = CFE_BOLD;
	}

	if (textsize != 0)
	{
		cf.dwMask |= CFM_SIZE;
		cf.yHeight = textsize;
	}

	cf.crTextColor = color;

	nInsertionPoint = -1;
	m_ctrlLog.SetSel(nInsertionPoint, -1);

	// Set the character format
	m_ctrlLog.SetSelectionCharFormat(cf);
	m_ctrlLog.ReplaceSel(str);

	return 0;
}

//////////////////////////////////////////////////////////////////////////
void CAboutDlgC::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}
#endif // ABOUT_DLG