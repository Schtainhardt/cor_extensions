#pragma once

static class UTF_Tools
{
public:
	static std::string UTF8_To_ASCII(std::string utf8String);
	static std::string ASCII_To_UTF8(std::string asciiString);

	static std::string UTF16_To_ASCII(std::wstring utf16String);
	static std::wstring ASCII_To_UTF16(std::string asciiString);

	static std::wstring UTF8_To_UTF16(std::string utf8String);
	static std::string UTF16_To_UTF8(std::wstring utf16String);

	static std::string UTF8_To_URL(std::string utfString);
};