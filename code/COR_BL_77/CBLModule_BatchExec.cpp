#include "stdafx.h"
#include "CBLModule_BatchExec.h"

#include "HookTools.h"

#include "SourceConverter.h"

//////////////////////////////////////////////////////////////////////////
// CBLModule7:: -> TryExecuteBatch
Hook_ExecuteBatch::TryExecuteBatchFunc Hook_ExecuteBatch::orig_TryExecuteBatch;

Hook_ExecuteBatch::Hook_ExecuteBatch()
{
	HOOK_MESSAGE;

	HANDLE processHandle = GetCurrentProcess();
	HMODULE moduleHandle = GetModuleHandleA(module_name);
	TryExecuteBatchFunc registrar = (TryExecuteBatchFunc)GetProcAddress(moduleHandle, mangled_func);

	DWORD OldProtection = 0;
	if (!VirtualProtectEx(processHandle, (LPVOID)registrar, 0xBE, PAGE_EXECUTE_READWRITE, &OldProtection))
		Log::Error("Failed");

	char* writer = (char*)registrar + 0xBA;
	*((DWORD*)writer) = (DWORD)hk_TryExecuteBatch - (DWORD)writer - 4;
}


int __stdcall Hook_ExecuteBatch::hk_TryExecuteBatch(const char* code, class CValue** args)
{
	CBLModule7* self;
	__asm
	{
		mov self, ecx
	}

	return self->ExecuteBatch(code, args);
}