#pragma once

template <typename T>
class Singleton
{
public:
	static T& GetInstance()
	{
		if (selfInstance == nullptr)
		{
			selfInstance = new T();
		}

		return *selfInstance;
	}

protected:
	static T* selfInstance;
};

#define REGISTER_SINGLETON(x) x* ##x::selfInstance
#define CONSTRUCT_SINGLETON(x) x::selfInstance = this