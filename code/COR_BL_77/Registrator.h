#pragma once
#include "stdafx.h"
#include "Hooker.h"

//////////////////////////////////////////////////////////////////////////
class Registrator
{
private:

	std::vector<void*> factoryList;

public:
	Registrator();

	template <class T> 
	void Register(const char* classNameRU) {
 		T* factory = new T();
 		//factoryList.push_back((void*)factory);
		CBLModule::RegisterContextClass(factory->GetRuntimeClass(), classNameRU, CType(100));
	};

	template <class T> 
	void Register(const char* classNameEN, const char* classNameRU) {
 		T* factory = new T();
 		//factoryList.push_back((void*)factory);
		CBLContext::RegisterContextClass(factory->GetRuntimeClass(), classNameRU, CType(100));
		CBLContext::RegisterContextClass(factory->GetRuntimeClass(), classNameEN, CType(100));
	};

};