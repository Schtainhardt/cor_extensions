#pragma once
#include "stdafx.h"

//////////////////////////////////////////////////////////////////////////
// CBLModule7:: -> TryEvalExpr
class Hook_EvalExpr
{

public:
	const char* module_name = "Seven.dll";
	const char* mangled_func = "?TryEvalExpr@CBLModule7@@QAEHPBDAAVCValue@@PAPAV2@@Z";

	Hook_EvalExpr();

private:
	typedef int(__thiscall *EvalExprFunc)(const char* code, class CValue& retVal, class CValue** args);
	static EvalExprFunc orig_EvalExpr;
	static void* EvalExprInternalCall;
	static int __stdcall hk_EvalExpr(const char* code, class CValue& retVal, class CValue** args);

};