#pragma once
#include "stdafx.h"

//////////////////////////////////////////////////////////////////////////
// WARNING!!!
//////////////////////////////////////////////////////////////////////////
/*
*	BL Engine NOT TESTED with any wide characters behavior
*	so any unexpected output you can get is ok
*
*	IF ANY BUGS: make UTF storing as data buffers and send
*		pointers to BL instead of strings per se
*/
//////////////////////////////////////////////////////////////////////////

class StringCoverter : public CBLContext
{
public:

	DECLARE_DYNCREATE(StringCoverter);

	StringCoverter();
	virtual ~StringCoverter() {};

	const char* Utf8ToAscii(const char* msg);
	const char* AsciiToUtf8(const char* msg);

	const wchar_t* Utf8ToUtf16(const char* msg);
	const char* Utf16ToUtf8(const wchar_t* msg);

	const wchar_t* AsciiToUtf16(const char* msg);
	const char* Utf16ToAscii(const wchar_t* msg);

	const char* AsciiToUri(const char* msg);
	const char* Utf8ToUri(const char* msg);

	long Base64ToInt64(const char* msg);

	//////////////////////////////////////////////////////////////////////////
	virtual int CallAsFunc(int iMethNum, class CValue& rValue, class CValue** ppValue)
	{
		switch (iMethNum)
		{
		case mUtf8ToAscii:
		{
			const char* retPtr = Utf8ToAscii(ppValue[0]->m_String);
			rValue = CValue(retPtr);
			delete[] retPtr;
			break;
		}
		case mAsciiToUtf8:
		{
			const char* retPtr = AsciiToUtf8(ppValue[0]->m_String);
			rValue = CValue(retPtr);
			delete[] retPtr;
			break;
		}
		case mAsciiToUtf16:
		{
			const wchar_t* retPtr = AsciiToUtf16(ppValue[0]->m_String);
			rValue = CValue((const char*)retPtr);
			delete[] retPtr;
			break;
		}
		case mUtf16ToAscii:
		{
			const char* retPtr = Utf16ToAscii(reinterpret_cast<const wchar_t*>((const char*)ppValue[0]->m_String));
			rValue = CValue(retPtr);
			delete[] retPtr;
			break;
		}
		case mUtf8ToUtf16:
		{

			const wchar_t* retPtr = Utf8ToUtf16(ppValue[0]->m_String);
			rValue = CValue((const char*)retPtr);
			delete[] retPtr;
			break;
		}
		case mUtf16ToUtf8:
		{
			const char* retPtr = Utf16ToUtf8(reinterpret_cast<const wchar_t*>((const char*)ppValue[0]->m_String));
			rValue = CValue(retPtr);
			delete[] retPtr;
			break;
		}
		case mUtf8ToUri:
		{
			const char* retPtr = Utf8ToUri(ppValue[0]->m_String);
			rValue = CValue(retPtr);
			delete[] retPtr;
			break;
		}
		case mAsciiToUri:
		{
			const char* retPtr = AsciiToUri(ppValue[0]->m_String);
			rValue = CValue(retPtr);
			delete[] retPtr;
			break;
		}
		case mBase64ToInt64:
		{
			long retPtr = Base64ToInt64(ppValue[0]->m_String);
			CNumeric outVal(retPtr);
			rValue = CValue(outVal);
			break;
		}
		default:
			return 1;
		};

		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int CallAsProc(int iMethNum, class CValue** ppValue)
	{
		switch (iMethNum)
		{

		default:
			return 1;
		};

		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  FindMethod(char const* methodName) const
	{
		int i;

		for (i = 0; i < LastMethod; i++) {
			if (!stricmp(methodName, defFnNames[i].Names[0]))
				return i;
			if (!stricmp(methodName, defFnNames[i].Names[1]))
				return i;
		}

		return -1;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  FindProp(char const* propName) const
	{
		int i;

		for (i = 0; i < LastProp; i++) {
			if (!stricmp(propName, defPropNames[i].Names[0]))
				return i;
			if (!stricmp(propName, defPropNames[i].Names[1]))
				return i;
		}

		return -1;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual char const* GetCode(void) const
	{
		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int GetDestroyUnRefd(void) const
	{
		return 1;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual class CObjID GetID(void) const
	{
		return CObjID::CObjID();
	}

	//////////////////////////////////////////////////////////////////////////
	virtual char const* GetMethodName(int iMethodNum, int iMethodAlias) const
	{
		if (iMethodNum >= LastMethod)
		{
			return 0;
		}
		else
			return defFnNames[iMethodNum].Names[iMethodAlias];
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int GetNMethods(void) const
	{
		return LastMethod;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int GetNParams(int iMethodNum) const
	{
		if (iMethodNum >= LastMethod)
		{
			return -1;
		}
		else
			return defFnNames[iMethodNum].NumberOfParams;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  GetNProps(void) const
	{
		return LastProp;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int GetParamDefValue(int iMethodNum, int iParamNum, class CValue* pDefValue) const
	{
		if (iMethodNum >= LastMethod)
		{
			return 1;
		}

		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual char const* GetPropName(int propNum, int propAlias) const
	{
		if (propNum >= LastProp)
		{
			return 0;
		}
		else
			return defFnNames[propNum].Names[propAlias];
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int GetPropVal(int propNum, class CValue& rValue) const
	{

		if (defPropNames[propNum].IsReadable == FALSE)
			return 1;

		switch (propNum)
		{
		case Version:
			rValue = CValue("1.0.0");
			return 0;
		default:
			break;
		}

		return 1;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual long GetTypeID(void) const
	{
		return 100;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual char const* GetTypeString(void) const
	{
		return "������";
	}

	//////////////////////////////////////////////////////////////////////////
	virtual class CType GetValueType(void) const
	{
		return CType(100);
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  HasRetVal(int iMethodNum) const
	{
		if (iMethodNum >= LastMethod)
		{
			return 0;
		}
		else
			return defFnNames[iMethodNum].HasReturnValue;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  IsExactValue(void) const
	{
		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  IsOleContext(void) const
	{
		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int IsPropReadable(int propNum) const
	{
		return defPropNames[propNum].IsReadable;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  IsPropWritable(int propNum) const
	{
		return defPropNames[propNum].IsWritable;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int  IsSerializable(void)
	{
		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int SaveToString(CString& retVal)
	{
		retVal = "DJK";
		return 1;
	}

	//////////////////////////////////////////////////////////////////////////
	virtual int SetPropVal(int propNum, class CValue const & val)
	{
		if (defPropNames[propNum].IsWritable == FALSE)
			return 1;

		switch (propNum)
		{
		case Version:
			//imposible
			break;
		default:
			break;
		}
	}

	//////////////////////////////////////////////////////////////////////////
	static struct paramdefs {
		char* Names[2];
		int HasReturnValue;
		int NumberOfParams;
	}  defFnNames[];

	//////////////////////////////////////////////////////////////////////////
	static struct parampropdefs {
		char* Names[2];
		BOOL IsReadable;
		BOOL IsWritable;
	}  defPropNames[];

	//////////////////////////////////////////////////////////////////////////
	enum {
		mAsciiToUtf8,
		mUtf8ToAscii,

		mAsciiToUtf16,
		mUtf16ToAscii,

		mUtf8ToUtf16,
		mUtf16ToUtf8,

		mUtf8ToUri,
		mAsciiToUri,

		mBase64ToInt64,

		LastMethod
	};

	//////////////////////////////////////////////////////////////////////////
	enum {
		Version,
		LastProp
	};

};