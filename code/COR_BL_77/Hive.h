#pragma once
#include "stdafx.h"
#include "HiveMessage.h"

#include <thread>
#include <queue>

class HiveRemodeCodeModule;

class Hive
{
private:
	zmq::context_t* context = nullptr;

	zmq::socket_t* upstream = nullptr;
	zmq::socket_t* downstream = nullptr;

	std::thread* listenerThread = nullptr;
	std::thread* senderThread = nullptr;
	bool wantToStop = false;

	unsigned int ownToken = 0;

	std::queue<HiveMessage> messages;

	void Listen();
	void QueryAndSend();

	void ProcessMessage(HiveMessage* msg);

	// specific commands:
	std::queue<HiveRemodeCodeModule*> remoteModules;
	bool ExecuteRemotely(HiveMessage* msg, std::string& result);

	HiveRole role;

protected:
public:
	Hive();
	~Hive();

	void PushMessage(HiveMessage hiveMessage);
	void UpdateCodeExecution() {};

	bool DoRemote();

};

//////////////////////////////////////////////////////////////////////////
class HiveRemodeCodeModule
{
private:
	Hive * hiveHolder;
	std::string result;
	std::string remoteCode;

	bool isDone = false;
	bool isSucsess = false;
public:
	HiveRemodeCodeModule(Hive* hive, std::string code);

	void Execute();

	bool IsDone() { return isDone; };
	bool Sucsess() { return isSucsess; };

	std::string Result() const { return result; };
};