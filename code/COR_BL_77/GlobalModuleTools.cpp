#include "stdafx.h"
#include "GlobalModuleTools.hpp"
#include "Registrator.h"
#include "Log.h"

#include "Entry.h"
#include "SourceConverter.h"

#include <string>
#include <fstream>
#include <sstream>

//EXTERN_C IMAGE_DOS_HEADER __ImageBase;

IMPLEMENT_DYNCREATE(GlobalModuleTools, CBLContext)

//////////////////////////////////////////////////////////////////////////
//! METHODS DEFINITION
struct GlobalModuleTools::paramdefs GlobalModuleTools::defFnNames[] = {

	// name_eng			name_rus				returns val		args count
	{ { "Attach",		"����������" },			TRUE,			2 },
	{ { "Revert",		"��������" },			TRUE,			0 },
	{ { NULL,			NULL },					NULL,			NULL }

};

//////////////////////////////////////////////////////////////////////////
//! FIELDS DEFINITION
struct GlobalModuleTools::parampropdefs GlobalModuleTools::defPropNames[] = {

	// name_eng			name_rus				readable		writeable
	{ { "Version",		"������" },				TRUE,			FALSE },
	{ { NULL,			NULL },					NULL,			NULL }

};

//////////////////////////////////////////////////////////////////////////
CBLContext* GlobalModuleTools::ExtensionContext;
CBLModule7* GlobalModuleTools::ExtensionModule;

//////////////////////////////////////////////////////////////////////////
int GlobalModuleTools::SwapPlainFile(const char* filePath)
{
	Log::DebugFmt("% @\x0F%@\x07", "Swapping global module as file link: ", filePath);

	std::ifstream file(filePath);
	std::stringstream buffer;

	buffer << file.rdbuf();
	std::string str = buffer.str();

	return SwapPlainText(str.c_str());
}

//////////////////////////////////////////////////////////////////////////
int GlobalModuleTools::SwapCryptedFile(const char* filePath)
{
	Log::DebugFmt("% @\x0F%@\x07", "Swapping global module as crypted file link: ", filePath);

	std::ifstream file(filePath);
	std::stringstream buffer;

	buffer << file.rdbuf();
	std::string str = buffer.str();
	
	return SwapCryptedText(str.c_str());
}

//////////////////////////////////////////////////////////////////////////
int GlobalModuleTools::SwapPlainText(const char* sourceCode)
{

	ExtensionContext = new CBLContext();
	ExtensionModule = new CBLModule7(ExtensionContext, "// Global module (COR)");

	ExtensionModule->AssignSource(sourceCode);

	Hook_GetGlobalModule::Inst->SetGlobalModule(ExtensionModule);

	return 1;
}

//////////////////////////////////////////////////////////////////////////
int GlobalModuleTools::SwapCryptedText(const char* sourceCode)
{

	ExtensionContext = new CBLContext();
	ExtensionModule = new CBLModule7(ExtensionContext, "// Global module (COR)");

	ExtensionModule->AssignSource(sourceCode);

	Hook_GetGlobalModule::Inst->SetGlobalModule(ExtensionModule);

	return 1;
}

//////////////////////////////////////////////////////////////////////////
int GlobalModuleTools::SwapImpl(const char* sourceCode, SwapMode mode)
{
	switch (mode)
	{
	case PLAIN_FILE:
		return SwapPlainFile(sourceCode);
		break;
	case CRYPTED_FILE:
		return SwapCryptedFile(sourceCode);
		break;
	case PLAIN_TEXT:
		return SwapPlainText(sourceCode);
		break;
	case CRYPTED_TEXT:
		return SwapCryptedText(sourceCode);
		break;
	case LAST_VAL:
		return 0;
		break;
	default:
		return 0;
		break;

	}

	return 1;
}

//////////////////////////////////////////////////////////////////////////
int GlobalModuleTools::RevertImpl()
{
	Hook_GetGlobalModule::Inst->RevertGlobalModule();
	return 1;
}