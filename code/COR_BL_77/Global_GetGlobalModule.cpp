#include "stdafx.h"
#include "Global_GetGlobalModule.h"

#include <string>
#include <sstream>
#include <fstream>

#include "HookTools.h"

//////////////////////////////////////////////////////////////////////////
bool Hook_GetGlobalModule::reverted;
bool Hook_GetGlobalModule::started = false;

CBLModule7* Hook_GetGlobalModule::originalGlobalModule;
CBLModule7* Hook_GetGlobalModule::currentGlobalModule;

Hook_GetGlobalModule* Hook_GetGlobalModule::Inst;

//////////////////////////////////////////////////////////////////////////
// ::GetGlobalModule
Hook_GetGlobalModule::GetGlobalModuleFunc Hook_GetGlobalModule::orig_GetGlobalModule;

//////////////////////////////////////////////////////////////////////////
const char* Hook_GetGlobalModule::defaultInitFile = "";

//////////////////////////////////////////////////////////////////////////
Hook_GetGlobalModule::Hook_GetGlobalModule()
{

	HOOK_MESSAGE;

	HANDLE processHandle = GetCurrentProcess();
	HMODULE moduleHandle = GetModuleHandleA(module_name);
	GetGlobalModuleFunc registrar = (GetGlobalModuleFunc)GetProcAddress(moduleHandle, mangled_func);
	BYTE* originalGlobalModuleReader = ((BYTE*)registrar)+1;
	
	DWORD oldProtect = 0;
	VirtualProtect(originalGlobalModuleReader, sizeof(void*), PAGE_EXECUTE_READWRITE, &oldProtect);
	originalGlobalModule = (CBLModule7*)(*(DWORD*)originalGlobalModuleReader);

	orig_GetGlobalModule = (GetGlobalModuleFunc)HookFunctionCdecl<5>(processHandle, registrar, hk_GetGlobalModule);

	Inst = this;
	reverted = false;
}

//////////////////////////////////////////////////////////////////////////
CBLModule7* __cdecl Hook_GetGlobalModule::hk_GetGlobalModule()
{

	if (reverted || !started)
	{
		Log::DebugFmt("Global module requested. Send @\x0b%@\x07", originalGlobalModule->m_strModulePath);
		return originalGlobalModule;
	}
	else if (currentGlobalModule != nullptr && !reverted)
	{
		Log::DebugFmt("Global module requested. Send @\x0b%@\x07", currentGlobalModule->m_strModulePath);
		return currentGlobalModule;
	}
	else 
	{
		Log::DebugFmt("Global module requested. Send @\x0b%@\x07", currentGlobalModule->m_strModulePath);
		return currentGlobalModule;
	}

	// impossible
	return nullptr;
}

//////////////////////////////////////////////////////////////////////////
void Hook_GetGlobalModule::SetGlobalModule(CBLModule7* newGlobalModule)
{

	originalGlobalModule->AssignFriendModule(newGlobalModule);
	newGlobalModule->AssignFriendModule(originalGlobalModule);

	if (newGlobalModule->Compile())
	{
		long newGlobalModuleID;
		CString newGlobalModuleName;
		newGlobalModule->GetID(newGlobalModuleName, newGlobalModuleID);
		newGlobalModuleName.TrimRight("\n");
		newGlobalModuleName.TrimRight("\r");
		if (newGlobalModuleName.IsEmpty() == TRUE)
		{
			newGlobalModule->SetID("Global module (COR)", 1);
		}
		reverted = true;
		newGlobalModule->PrepareToLoad();
		newGlobalModule->Load();
		newGlobalModule->Commit();
		newGlobalModule->Execute();
		currentGlobalModule = newGlobalModule;
		started = true;
		reverted = false;
	}
	else
	{
		Log::Error("Global module requested but it can't be compiled!");
	}
}

//////////////////////////////////////////////////////////////////////////
void Hook_GetGlobalModule::RevertGlobalModule()
{
	if (reverted)
		return;

	Log::Debug("Reverting global module...");

	reverted = true;
	currentGlobalModule = nullptr;
}

//////////////////////////////////////////////////////////////////////////
bool Hook_GetGlobalModule::IsOriginal(CBLModule7* ptr)
{
	return ptr == originalGlobalModule;
}

//////////////////////////////////////////////////////////////////////////
void Hook_GetGlobalModule::Start()
{
	started = true;
}

CBLModule7* Hook_GetGlobalModule::GetOriginal() const
{
	return originalGlobalModule;
}
