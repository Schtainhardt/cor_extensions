﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using Newtonsoft.Json;
using ZeroMQ;
using COR_Hive;

namespace COR_UserHost
{

    public class HiveUserHost : HiveSubscriber
    {
        private uint ownToken = (uint)HiveRole.HR_VFSC_BROKER;

        public override bool ProcessMessage(HiveMessageHeader hdr, byte[] data)
        {
            if (hdr.type != HiveMessageType.TARGET_VFSC_REQ || hdr.targetToken != ownToken)
                return true;

            HiveVFSC vfsc = ByteToType<HiveVFSC>(data);
            vfsc.pass = CORUsersBroker.GetPassword(vfsc.user);

            HiveMessageHeader outhdr = new HiveMessageHeader();
            outhdr.senderToken = ownToken;
            outhdr.targetToken = hdr.senderToken;
            outhdr.type = HiveMessageType.TARGET_VFSC_RESP;
            outhdr.dataLength = 0;
            byte[] outdata = TypeToByte(vfsc);
            serverSocketSender.Send(new ZFrame(TypeToByte(outhdr)));
            serverSocketSender.Send(new ZFrame(outdata));

            return true;
        }
    }

    public partial class CORUsersBroker : ServiceBase
    {
        [DllImport("netapi32.dll", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall, 
            SetLastError = true)]
        static extern uint NetUserChangePassword(
                [MarshalAs(UnmanagedType.LPWStr)] string domainname,
                [MarshalAs(UnmanagedType.LPWStr)] string username,
                [MarshalAs(UnmanagedType.LPWStr)] string oldpassword,
                [MarshalAs(UnmanagedType.LPWStr)] string newpassword
            );

        private const uint UPDATE_DELAY = 60 /* secs */ * 60 /* mins */ * 6 /* hrs */;
        private uint updateETA = 0;
        private bool wantStop = false;
        private Thread updateThread = null;

        static Dictionary<string, string> usersCreds = new Dictionary<string, string>();

        public static string GetPassword(string key)
        {
            if (!usersCreds.ContainsKey(key))
                return "";

            return usersCreds[key];
        }

        public CORUsersBroker()
        {
            InitializeComponent();
        }

        public static string GenerateStrongPassword(int length, IEnumerable<char> characterSet)
        {
            var characterArray = characterSet.Distinct().ToArray();
            var bytes = new byte[length * 8];
            new RNGCryptoServiceProvider().GetBytes(bytes);
            var result = new char[length];
            for (int i = 0; i < length; i++)
            {
                ulong value = BitConverter.ToUInt64(bytes, i * 8);
                result[i] = characterArray[value % (uint)characterArray.Length];
            }
            return new string(result);
        }

        protected override void OnStart(string[] args)
        {
            updateThread = new Thread(() =>
            {
                if (File.Exists("accounts.dat"))
                {
                    string data = File.ReadAllText("accounts.dat");
                    usersCreds = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                }

                while (!wantStop)
                {
                    for (; updateETA > 0; --updateETA)
                    {
                        Thread.Sleep(1000);
                    }

                    foreach (var pair in usersCreds)
                    {
                        string newPassword = GenerateStrongPassword(12, "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm1234567890");
                        NetUserChangePassword(null, pair.Key, pair.Value, newPassword);
                        usersCreds[pair.Key] = newPassword;
                    }

                    string data = JsonConvert.SerializeObject(usersCreds, Formatting.Indented);
                    File.WriteAllText("accounts.dat", data);
                }
            });
        }

        protected override void OnStop()
        {
            wantStop = true;
            updateETA = 0;
            if (updateThread.IsAlive) updateThread.Join(10000);
        }
    }
}
