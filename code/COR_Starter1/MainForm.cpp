#include "MainForm.h"

using namespace System;
using namespace COR_Launcher;

void main(array<String^>^ argv)
{
	System::Windows::Forms::Application::EnableVisualStyles();
	System::Windows::Forms::Application::Run(gcnew MainForm());
	return;
}