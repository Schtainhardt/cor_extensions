#include "NativeLauncher.h"

NativeLauncher::NativeLauncher()
{
}

NativeLauncher::~NativeLauncher()
{
}

void createShellcode(int ret, int str, unsigned char** shellcode, int* shellcodeSize)
{
	unsigned char* retChar = (unsigned char*)&ret;
	unsigned char* strChar = (unsigned char*)&str;

	int api = (int)GetProcAddress(LoadLibraryA("kernel32.dll"), "LoadLibraryA");

	unsigned char* apiChar = (unsigned char*)&api;
	unsigned char sc[] = {
		0x68, retChar[0], retChar[1], retChar[2], retChar[3],
		0x9C,
		0x60,
		0x68, strChar[0], strChar[1], strChar[2], strChar[3],
		0xB8, apiChar[0], apiChar[1], apiChar[2], apiChar[3],
		0xFF, 0xD0,
		0x61,
		0x9D,
		0xC3
	};

	*shellcodeSize = 22;
	*shellcode = (unsigned char*)malloc(22);
	memcpy(*shellcode, sc, 22);
}

void NativeLauncher::Launch()
{

	LPCSTR filePath = "D:\\Programs\\1Cv77\\BIN\\1cv7s.exe";
	LPCSTR currentDirectory = "D:\\Programs\\1Cv77\\BIN\\";
	LPCSTR dllPath = "D:\\BSL_CORExtensionsD.dll";

	STARTUPINFOA StartupInfo = { 0 };
	PROCESS_INFORMATION ProcessInformation;

	StartupInfo.cb = sizeof(StartupInfo);

	HANDLE launcherMutex = CreateMutexA(0, FALSE, "Global\\CORBSL_Extensions");

	int result = CreateProcessA(
		filePath,
		NULL,
		NULL,
		NULL,
		FALSE,
		CREATE_SUSPENDED,
		NULL,
		currentDirectory,
		&StartupInfo,
		&ProcessInformation
	);

	if (result == 0)
		return;

	HANDLE hProcess(ProcessInformation.hProcess);
	HANDLE hThread(ProcessInformation.hThread);

	char* remote_dllString = (char*)VirtualAllocEx(hProcess, NULL, strlen(dllPath) + 1, MEM_COMMIT, PAGE_READWRITE);

	CONTEXT context = {0};

	context.ContextFlags = CONTEXT_CONTROL;
	GetThreadContext(hThread, &context);

	unsigned char* shellcode;
	int shellcodeLen;
	createShellcode(context.Eip, (int)remote_dllString, &shellcode, &shellcodeLen);
	
	char* remote_shellcode = (char*)VirtualAllocEx(hProcess, NULL, shellcodeLen, MEM_COMMIT, PAGE_EXECUTE_READWRITE);

	WriteProcessMemory(hProcess, remote_dllString, dllPath, strlen(dllPath) + 1, NULL);
	WriteProcessMemory(hProcess, remote_shellcode, shellcode, shellcodeLen, NULL);

	context.Eip = (DWORD)remote_shellcode;
	context.ContextFlags = CONTEXT_CONTROL;
	SetThreadContext(hThread, &context);

	ResumeThread(hThread);

	//Sleep(30000);

	//VirtualFreeEx(hProcess, remote_dllString, strlen(dllPath) + 1, MEM_DECOMMIT);
	//VirtualFreeEx(hProcess, remote_shellcode, shellcodeLen, MEM_DECOMMIT);

}
