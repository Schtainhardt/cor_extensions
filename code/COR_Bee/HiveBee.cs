﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using ZeroMQ;

using COR_Hive;

namespace COR_Bee
{
    public class HiveBee : HiveSubscriber
    {
        public void SendStop()
        {
            HiveMessageHeader hdr = new HiveMessageHeader();
            hdr.dataLength = 0;
            hdr.senderToken = 0x73706563;
            hdr.targetToken = 0x73706563;
            hdr.type = HiveMessageType.INTERNAL_SHUTDOWN;
            hdr.totalSize = (uint)Marshal.SizeOf(typeof(HiveMessageHeader));

            serverSocketSender.Send(new ZFrame(TypeToByte(hdr)));
        }

        public override bool ProcessMessage(HiveMessageHeader hdr, byte[] data)
        {
            switch (hdr.type)
            {
                case HiveMessageType.INTERNAL_SHUTDOWN:
                    if (hdr.targetToken != 0x73706563 /*HR_SPECTATOR*/)
                        break;
                    return false;

                case HiveMessageType.TARGET_EXECUTE:
                    Program.mainForm.AddMessage("Token " + hdr.senderToken.ToString() + " requested remote code execution at " + hdr.targetToken.ToString());
                    string strData = Encoding.GetEncoding(1251).GetString(data);
                    Program.mainForm.AddMessage(strData);
                    break;
                case HiveMessageType.PUBLIC_LIST:
                    Program.mainForm.AddMessage("Token " + hdr.senderToken.ToString() + " requested network heartbeat (but targeted to " + hdr.targetToken.ToString() + ")");

                    HiveMessageHeader ownHdr = new HiveMessageHeader();
                    ownHdr.targetToken = hdr.senderToken;
                    ownHdr.type = HiveMessageType.PNOTIFY_ALIVE;
                    ownHdr.dataLength = (uint)Marshal.SizeOf<HiveClient>();
                    ownHdr.totalSize = ownHdr.dataLength + (uint)Marshal.SizeOf<HiveMessageHeader>();

                    HiveClient ownClient = new HiveClient();
                    ownClient.processPid = (uint)HiveRole.HR_LOGGER;
                    ownClient.role = HiveRole.HR_LOGGER;

                    List<byte> rawMessage = new List<byte>();
                    rawMessage.AddRange(TypeToByte(ownHdr));
                    rawMessage.AddRange(TypeToByte(ownClient));

                    serverSocketSender.Send(new ZFrame(rawMessage.ToArray()));
                    break;
                case HiveMessageType.PNOTIFY_ALIVE:
                    HiveClient hc = ByteToType<HiveClient>(data);
                    Program.mainForm.AddMessage($"Token {hdr.senderToken.ToString()} is alive! " +
                        $"{Environment.NewLine}" +
                        $"\t With PID: {hc.processPid.ToString()}" +
                        $"{Environment.NewLine}" +
                        $"\t With role: {hc.role.ToString()}" +
                        $"{Environment.NewLine}" +
                        $"\t Targeted to {hdr.targetToken}");
                    break;
                case HiveMessageType.NO_ACTION:
                    Program.mainForm.AddMessage("Token "+hdr.senderToken.ToString()+" requested no action from token "+hdr.targetToken.ToString());
                    break;
                default:
                    Program.mainForm.AddMessage("Unknown action processed");
                    break;
            }

            return true;
        }
    }
}
