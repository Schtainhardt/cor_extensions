﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZeroMQ;

namespace COR_Bee
{
    public partial class Form1 : Form
    {
        public delegate void AddTextDelegate(string text);

        HiveBee bee = new HiveBee();

        public Form1()
        {
            InitializeComponent();
        }

        public void AddMessage(string text)
        {
            if (this.textBox1.InvokeRequired)
            {
                AddTextDelegate d = new AddTextDelegate(AddMessage);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.textBox1.Text += text + Environment.NewLine;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            bee.Start();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            bee.SendStop();
        }
    }
}
