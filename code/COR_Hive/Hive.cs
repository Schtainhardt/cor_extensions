﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace COR_Hive
{
    public class Hive
    {
        private List<HiveModule> modules = new List<HiveModule>();

        public Hive()
        {
            modules.Add(new HiveProxy());
            modules.Add(new HiveHeartbeat());
            modules.Add(new HiveHeartbeatInvoker());
            modules.Add(new HiveExchanger());
            modules.Add(new HiveRFS());
        }

        public void Start()
        {
            foreach (HiveModule module in modules)
            {
                module.Start();
            }
        }

        public void Stop()
        {
            foreach (HiveModule module in modules)
            {
                module.Stop();
            }
        }
    }
}
