﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace COR_Hive
{
    public partial class HiveService : ServiceBase
    {

        Hive hive = new Hive();

        public HiveService()
        {
            InitializeComponent();
        }

        public void Start()
        {
            OnStart(new string[] { });
        }

        protected override void OnStart(string[] args)
        {
            hive.Start();
        }

        protected override void OnStop()
        {
            hive.Stop();
        }
    }
}
