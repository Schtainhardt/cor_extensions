﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using ZeroMQ;

namespace COR_Hive
{
    //////////////////////////////////////////////////////////////////////////
    class HiveExchanger : HiveSubscriber
    {
        private List<HiveClient> clients = new List<HiveClient>();

        public override bool ProcessMessage(HiveMessageHeader hdr, byte[] data)
        {
            switch (hdr.type)
            {
                case HiveMessageType.PUBLIC_RESET_LIST:
                    {
                        clients.Clear();
                        return true;
                    }
                case HiveMessageType.PNOTIFY_ALIVE:
                    {
                        HiveClient client = ByteToType<HiveClient>(data);
                        clients.Add(client);
                        return true;
                    }
                default:
                    return true;
            }
        }
    }
}
