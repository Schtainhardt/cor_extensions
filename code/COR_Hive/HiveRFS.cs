﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using ZeroMQ;

namespace COR_Hive
{
    //////////////////////////////////////////////////////////////////////////
    [StructLayout(LayoutKind.Sequential)]
    public struct NETRESOURCE
    {
        public int dwScope;
        public int dwType;
        public int dwDisplayType;
        public int dwUsage;
        public string LocalName;
        public string RemoteName;
        public string Comment;
        public string Provider;
    }

    //////////////////////////////////////////////////////////////////////////
    public class HiveRFS : HiveSubscriber
    {
        private string connectedShare = "";

        //////////////////////////////////////////////////////////////////////////
        [DllImport("mpr.dll")]
        static extern int WNetAddConnection2(ref NETRESOURCE netResource, string password, string username, int flags);

        [DllImport("mpr.dll")]
        static extern int WNetCancelConnection2(string lpName, int dwFlags, int fForce);

        //////////////////////////////////////////////////////////////////////////
        private NETRESOURCE GetNetresource(string remoteFolder)
        {
            if (!String.IsNullOrEmpty(connectedShare))
                throw new Exception("Share is already connected, disconnect first!");

            NETRESOURCE rc = new NETRESOURCE();
            rc.dwType = 0;
            rc.RemoteName = $"\\\\cor.com.ua\\{remoteFolder}";
            connectedShare = $"\\\\cor.com.ua\\{remoteFolder}";
            rc.LocalName = "Z:";
            rc.Provider = null;

            return rc;
        }

        //////////////////////////////////////////////////////////////////////////
        public override bool Start()
        {
            if (!base.Start())
                return false;

            return true;
        }

        //////////////////////////////////////////////////////////////////////////
        public override bool Stop()
        {
            if (!base.Stop())
                return false;

            if (!String.IsNullOrEmpty(connectedShare))
            {
                int cancelRet = WNetCancelConnection2(connectedShare, 1, 1);
            }

            return true;
        }

        //////////////////////////////////////////////////////////////////////////
        public override bool ProcessMessage(HiveMessageHeader hdr, byte[] data)
        {
            switch (hdr.type)
            {
                case HiveMessageType.TARGET_VFS_MOUNT:
                    {
                        HiveVFSCredentials creds = ByteToType<HiveVFSCredentials>(data);

                        NETRESOURCE netResource;
                        try
                        {
                            netResource = GetNetresource("ich");
                        }
                        catch (Exception e)
                        {
                            int cancelRet = WNetCancelConnection2(connectedShare, 1, 1);
                            netResource = GetNetresource("ich");
                        }

                        int addRet = WNetAddConnection2(ref netResource, "A3VrRmMgjC8rzbd2", "Interchem", 4);

                        return true;
                    }
                default:
                    return true;
            }
        }
    }
}
