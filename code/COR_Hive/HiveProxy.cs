﻿using ZeroMQ;

namespace COR_Hive
{
    public class HiveProxy : HiveModule
    {
        private const string PUB_ENDPOINT = "tcp://127.0.0.1:10400";
        private const string SUB_ENDPOINT = "tcp://127.0.0.1:10401";

        ZContext serverContext = new ZContext();

        ZSocket serverSocketPublisher = null;
        ZSocket serverSocketSubscriber = null;

        public HiveProxy()
        {
            serverSocketPublisher = new ZSocket(serverContext, ZSocketType.XPUB);
            serverSocketSubscriber = new ZSocket(serverContext, ZSocketType.XSUB);
        }

        public override bool Start()
        {
            bool result = base.Start();

            serverSocketPublisher.Bind(PUB_ENDPOINT);
            serverSocketSubscriber.Bind(SUB_ENDPOINT);

            return true;
        }

        public override bool Update()
        {
            ZContext.Proxy(serverSocketSubscriber, serverSocketPublisher);
            return false;
        }

        public override bool Stop()
        {
            serverSocketSubscriber.Unbind(SUB_ENDPOINT);
            serverSocketPublisher.Unbind(PUB_ENDPOINT);

            serverSocketSubscriber.Close();
            serverSocketPublisher.Close();

            serverContext.Shutdown();

            return true;
        }
    }
}
