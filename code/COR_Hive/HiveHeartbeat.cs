﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using ZeroMQ;

namespace COR_Hive
{
    //////////////////////////////////////////////////////////////////////////
    class HiveHeartbeat : HiveSubscriber
    {
        public override bool ProcessMessage(HiveMessageHeader hdr, byte[] data)
        {
            switch (hdr.type)
            {
                case HiveMessageType.PUBLIC_LIST:
                    {
                        HiveMessageHeader msgHead = new HiveMessageHeader();
                        msgHead.targetToken = hdr.senderToken;
                        msgHead.senderToken = (uint)HiveRole.HR_MASTER;
                        msgHead.type = HiveMessageType.PNOTIFY_ALIVE;
                        msgHead.dataLength = (uint)Marshal.SizeOf(typeof(HiveClient));
                        msgHead.totalSize = msgHead.dataLength + (uint)Marshal.SizeOf(typeof(HiveMessageHeader));

                        HiveClient hiveClientResp = new HiveClient();
                        hiveClientResp.role = HiveRole.HR_MASTER;
                        hiveClientResp.processPid = (uint)HiveRole.HR_MASTER;

                        byte[] rawMessage = TypeToByte(msgHead);
                        rawMessage = rawMessage.Concat(TypeToByte(hiveClientResp)).ToArray();

                        ZFrame frm = new ZFrame(rawMessage);
                        serverSocketSender.Send(frm);

                        return true;
                    }
                default:
                    return true;
            }
        }
    }
}
