﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using ZeroMQ;

namespace COR_Hive
{
    public class HiveHeartbeatInvoker : HiveModule
    {
        private const string ENDPOINT_SEND = "tcp://127.0.0.1:10401";

        ZContext serverContext = new ZContext();
        public ZSocket serverSocketSender = null;

        //////////////////////////////////////////////////////////////////////////
        public HiveHeartbeatInvoker()
        {
            serverSocketSender = new ZSocket(serverContext, ZSocketType.PUB);
        }

        //////////////////////////////////////////////////////////////////////////
        public override bool Start()
        {
            bool result = base.Start();

            if (!result)
                return false;

            serverSocketSender.Connect(ENDPOINT_SEND);

            return true;
        }

        //////////////////////////////////////////////////////////////////////////
        public override bool Update()
        {
            Thread.Sleep(5000);

            // reset all remote lists
            HiveMessageHeader header = new HiveMessageHeader();
            header.senderToken = (uint)HiveRole.HR_HB_INVOKER;
            header.dataLength = 0;
            header.type = HiveMessageType.PUBLIC_RESET_LIST;
            header.targetToken = 0;
            header.totalSize = (uint)Marshal.SizeOf(typeof(HiveMessageHeader));
            serverSocketSender.Send(new ZFrame(TypeToByte(header)));

            Thread.Sleep(50);

            // invoke
            header = new HiveMessageHeader();
            header.senderToken = (uint)HiveRole.HR_HB_INVOKER;
            header.dataLength = 0;
            header.type = HiveMessageType.PUBLIC_LIST;
            header.targetToken = 0;
            header.totalSize = (uint)Marshal.SizeOf(typeof(HiveMessageHeader));
            serverSocketSender.Send(new ZFrame(TypeToByte(header)));

            return true;
        }

        //////////////////////////////////////////////////////////////////////////
        public override bool Stop()
        {
            bool result = base.Stop();

            if (!result)
                return false;

            serverSocketSender.Close();
            serverContext.Shutdown();

            return true;
        }
    }
}
