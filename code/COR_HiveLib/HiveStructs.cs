﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace COR_Hive
{
    //////////////////////////////////////////////////////////////////////////
    public enum HiveRole
    {
        // BL instances
        HR_ENTERPRISE,
        HR_CONFIG,

        // bee
        HR_LOGGER,

        // server modules
        HR_MASTER,
        HR_EXCHANGER,
        HR_HB_INVOKER,
        HR_VFSC_BROKER,

        // client module
        HR_LAUNCHER
    }

    //////////////////////////////////////////////////////////////////////////
    [StructLayout(LayoutKind.Sequential)]
    public struct HiveMessageHeader
    {
        [MarshalAs(UnmanagedType.U4)] public UInt32 totalSize;
        [MarshalAs(UnmanagedType.U4)] public UInt32 senderToken;
        [MarshalAs(UnmanagedType.U4)] public UInt32 targetToken;
        [MarshalAs(UnmanagedType.U4)] public HiveMessageType type;
        [MarshalAs(UnmanagedType.U4)] public UInt32 dataLength;
        [MarshalAs(UnmanagedType.U4)] public UInt32 unusedPointer;
    }

    //////////////////////////////////////////////////////////////////////////
    // Heartbeat
    [StructLayout(LayoutKind.Sequential)]
    public struct HiveClient
    {
        [MarshalAs(UnmanagedType.U4)] public UInt32 processPid;
        [MarshalAs(UnmanagedType.U4)] public HiveRole role;
    }

    //////////////////////////////////////////////////////////////////////////
    // FS Mount
    //
    [StructLayout(LayoutKind.Sequential)]
    public struct HiveVFSCredentials
    {
        [MarshalAs(UnmanagedType.LPStr)] public string share;
        [MarshalAs(UnmanagedType.LPStr)] public string user;
        [MarshalAs(UnmanagedType.LPStr)] public string pass;
    };
    
    //////////////////////////////////////////////////////////////////////////
    // Hive share creds
    //
    [StructLayout(LayoutKind.Sequential)]
    public struct HiveVFSC
    {
        [MarshalAs(UnmanagedType.LPStr)] public string user;
        [MarshalAs(UnmanagedType.LPStr)] public string pass;
    };

    //////////////////////////////////////////////////////////////////////////
    public enum HiveMessageType
    {
	    //+========================+===========================+=====================================+
	    //| Request		           | Response			       | Description		                 |
	    //+========================+===========================+=====================================+
	        TARGET_EXECUTE,			TARGET_EXECUTE_RESP,		// Remote code execution
								    TARGET_EXECUTE_ERR,
	    //+------------------------+---------------------------+-------------------------------------+
            TARGET_VFS_MOUNT,		TARGET_VFS_MOUNT_RESP,		// Mount remote folder
								    TARGET_VFS_MOUNT_ERR,
        //+------------------------+---------------------------+-------------------------------------+
            TARGET_VFSC_REQ,        TARGET_VFSC_RESP,           // VFS credentials
        //+------------------------+---------------------------+-------------------------------------+
            PUBLIC_LIST,			PNOTIFY_ALIVE,				// Heartbeat
	    //+------------------------+---------------------------+-------------------------------------+
		    PUBLIC_RESET_LIST,									// List reset request for HB trackers
	    //+------------------------+---------------------------+-------------------------------------+
		    INTERNAL_SHUTDOWN,									// Internal shutdown loopback
	    //+------------------------+---------------------------+-------------------------------------+
		    NO_ACTION											// Default no action request
	    //+------------------------+---------------------------+-------------------------------------+
    };
}
