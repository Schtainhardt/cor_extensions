﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace COR_Hive
{
    public abstract class HiveModule
    {
        private Thread updateThread = null;

        public HiveModule()
        {
            updateThread = new Thread(() =>
            {
                while (Update());
            });
        }

        public virtual bool Start()
        {
            updateThread.Start();
            return true;
        }

        public abstract bool Update();

        public virtual bool Stop()
        {
            if (updateThread.IsAlive)
                updateThread.Join();

            return true;
        }

        public static T ByteToType<T>(BinaryReader reader)
        {
            byte[] bytes = reader.ReadBytes(Marshal.SizeOf(typeof(T)));

            GCHandle handle = GCHandle.Alloc(bytes, GCHandleType.Pinned);
            T theStructure = (T)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(T));
            handle.Free();

            return theStructure;
        }

        public static T ByteToType<T>(byte[] bytes)
        {
            int targetSize = Marshal.SizeOf(typeof(T));

            if (bytes.Length < targetSize)
                throw new Exception("Too few bytes given to unwrap the struct");

            if (bytes.Length > targetSize)
                bytes = bytes.Take(targetSize).ToArray();

            GCHandle handle = GCHandle.Alloc(bytes, GCHandleType.Pinned);
            T theStructure = (T)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(T));
            handle.Free();

            return theStructure;
        }

        public static byte[] TypeToByte<T>(T str)
        {
            int size = Marshal.SizeOf(str);
            byte[] arr = new byte[size];

            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(str, ptr, true);
            Marshal.Copy(ptr, arr, 0, size);
            Marshal.FreeHGlobal(ptr);
            return arr;
        }
    }
}
