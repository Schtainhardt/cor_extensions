﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using ZeroMQ;

namespace COR_Hive
{
    public abstract class HiveSubscriber : HiveModule
    {
        private string ENDPOINT_RECV = "tcp://127.0.0.1:10400";
        private string ENDPOINT_SEND = "tcp://127.0.0.1:10401";

        ZContext serverContext = new ZContext();
        protected ZSocket serverSocketReceiver = null;
        protected ZSocket serverSocketSender = null;

        //////////////////////////////////////////////////////////////////////////
        public HiveSubscriber(string recv = "tcp://127.0.0.1:10400", string send = "tcp://127.0.0.1:10401")
        {
            ENDPOINT_RECV = recv;
            ENDPOINT_SEND = send;

            serverSocketReceiver = new ZSocket(serverContext, ZSocketType.SUB);
            serverSocketSender = new ZSocket(serverContext, ZSocketType.PUB);
        }

        //////////////////////////////////////////////////////////////////////////
        public override bool Start()
        {
            bool result = base.Start();

            if (!result)
                return false;

            serverSocketReceiver.Connect(ENDPOINT_RECV);
            serverSocketSender.Connect(ENDPOINT_SEND);

            serverSocketReceiver.SetOption(ZSocketOption.SUBSCRIBE, "");

            return true;
        }

        //////////////////////////////////////////////////////////////////////////
        public abstract bool ProcessMessage(HiveMessageHeader hdr, byte[] data);

        //////////////////////////////////////////////////////////////////////////
        public override bool Update()
        {
            ZFrame frame = serverSocketReceiver.ReceiveFrame();
            if (frame.Length < Marshal.SizeOf(typeof(HiveMessageHeader)))
                return true;

            byte[] header = frame.Read(Marshal.SizeOf(typeof(HiveMessageHeader)));
            byte[] data = { };
            HiveMessageHeader hdr = ByteToType<HiveMessageHeader>(header);

            if (hdr.dataLength > 0)
            {
                data = frame.Read((int)hdr.dataLength);
            }

            return ProcessMessage(hdr, data);
        }

        //////////////////////////////////////////////////////////////////////////
        public override bool Stop()
        {
            bool result = base.Stop();

            if (!result)
                return false;

            serverSocketReceiver.Close();
            serverSocketSender.Close();
            serverContext.Shutdown();

            return true;
        }
    }
}
