#pragma once

#include "windows.h"

namespace CORStarter {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Configuration;
	using namespace MetroFramework::Forms;
	using namespace MetroFramework::Controls;
	using namespace MetroFramework::Components;

	public ref class Widget : public MetroFramework::Forms::MetroForm
	{
	public:
		Widget(void)
		{
			InitializeComponent();
		}

	protected:
		~Widget()
		{
			if (components)
			{
				delete components;
			}
		}

	private:
		MetroFramework::Components::MetroStyleManager^  metroStyleManager1;
		MetroFramework::Components::MetroStyleExtender^  metroStyleExtender1;
		MetroFramework::Controls::MetroTile^  mtOptions;
		MetroFramework::Controls::MetroTile^  mtExit;
		MetroFramework::Controls::MetroProgressSpinner^  msNetworkStatus;
		MetroFramework::Controls::MetroTile^  mtLaunchCitramon;
		MetroFramework::Controls::MetroTile^  mtMonitor;
		MetroFramework::Controls::MetroTile^  mtLaunchConfig;
		System::Windows::Forms::NotifyIcon^  trayIcon;
		MetroFramework::Controls::MetroTile^  mtLaunchCitramonEx;
		MetroFramework::Components::MetroToolTip^  metroToolTip;
		MetroFramework::Controls::MetroContextMenu^  trayContextMenu;
		System::Windows::Forms::ToolStripMenuItem^  tttStatus;
		System::Windows::Forms::ToolStripSeparator^  tttSeparator1;
		System::Windows::Forms::ToolStripMenuItem^  tttCitramonEx;
		System::Windows::Forms::ToolStripMenuItem^  tttCitramon;
		System::Windows::Forms::ToolStripMenuItem^  tttDesigner;
		System::Windows::Forms::ToolStripMenuItem^  tttMonitor;
		System::Windows::Forms::ToolStripSeparator^  toolStripSeparator2;
		System::Windows::Forms::ToolStripMenuItem^  tttQuit;
		System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem1;
		System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem2;
		System::Windows::Forms::Timer^ timerPoll;
		MetroFramework::Controls::MetroLabel^  metroLabel1;
		System::ComponentModel::IContainer^  components;

		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Widget::typeid));
			this->metroStyleManager1 = (gcnew MetroFramework::Components::MetroStyleManager(this->components));
			this->metroStyleExtender1 = (gcnew MetroFramework::Components::MetroStyleExtender(this->components));
			this->mtOptions = (gcnew MetroFramework::Controls::MetroTile());
			this->mtExit = (gcnew MetroFramework::Controls::MetroTile());
			this->msNetworkStatus = (gcnew MetroFramework::Controls::MetroProgressSpinner());
			this->mtLaunchCitramon = (gcnew MetroFramework::Controls::MetroTile());
			this->mtMonitor = (gcnew MetroFramework::Controls::MetroTile());
			this->mtLaunchConfig = (gcnew MetroFramework::Controls::MetroTile());
			this->trayIcon = (gcnew System::Windows::Forms::NotifyIcon(this->components));
			this->trayContextMenu = (gcnew MetroFramework::Controls::MetroContextMenu(this->components));
			this->tttStatus = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->tttSeparator1 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->tttCitramonEx = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->tttCitramon = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->tttDesigner = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->tttMonitor = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator2 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->tttQuit = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->mtLaunchCitramonEx = (gcnew MetroFramework::Controls::MetroTile());
			this->metroToolTip = (gcnew MetroFramework::Components::MetroToolTip());
			this->timerPoll = (gcnew System::Windows::Forms::Timer(this->components));
			this->metroLabel1 = (gcnew MetroFramework::Controls::MetroLabel());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->metroStyleManager1))->BeginInit();
			this->trayContextMenu->SuspendLayout();
			this->SuspendLayout();
			// 
			// metroStyleManager1
			// 
			this->metroStyleManager1->Owner = this;
			this->metroStyleManager1->Style = MetroFramework::MetroColorStyle::Teal;
			// 
			// metroStyleExtender1
			// 
			this->metroStyleExtender1->Theme = MetroFramework::MetroThemeStyle::Light;
			// 
			// mtOptions
			// 
			this->mtOptions->ActiveControl = nullptr;
			this->mtOptions->Location = System::Drawing::Point(61, 277);
			this->mtOptions->Name = L"mtOptions";
			this->mtOptions->Size = System::Drawing::Size(90, 90);
			this->mtOptions->TabIndex = 0;
			this->mtOptions->Text = L"���������";
			this->mtOptions->TileImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"mtOptions.TileImage")));
			this->mtOptions->TileImageAlign = System::Drawing::ContentAlignment::TopRight;
			this->mtOptions->UseSelectable = true;
			this->mtOptions->UseStyleColors = true;
			this->mtOptions->UseTileImage = true;
			// 
			// mtExit
			// 
			this->mtExit->ActiveControl = nullptr;
			this->mtExit->Location = System::Drawing::Point(157, 277);
			this->mtExit->Name = L"mtExit";
			this->mtExit->Size = System::Drawing::Size(90, 90);
			this->mtExit->Style = MetroFramework::MetroColorStyle::Orange;
			this->mtExit->TabIndex = 1;
			this->mtExit->Text = L"�����";
			this->mtExit->TileImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"mtExit.TileImage")));
			this->mtExit->TileImageAlign = System::Drawing::ContentAlignment::TopRight;
			this->mtExit->UseSelectable = true;
			this->mtExit->UseStyleColors = true;
			this->mtExit->UseTileImage = true;
			this->mtExit->Click += gcnew System::EventHandler(this, &Widget::mtExit_Click);
			// 
			// msNetworkStatus
			// 
			this->msNetworkStatus->Location = System::Drawing::Point(223, 134);
			this->msNetworkStatus->Maximum = 100;
			this->msNetworkStatus->Name = L"msNetworkStatus";
			this->msNetworkStatus->Size = System::Drawing::Size(24, 23);
			this->msNetworkStatus->TabIndex = 4;
			this->metroToolTip->SetToolTip(this->msNetworkStatus, L"��������� ����������");
			this->msNetworkStatus->UseSelectable = true;
			this->msNetworkStatus->Value = 25;
			// 
			// mtLaunchCitramon
			// 
			this->mtLaunchCitramon->ActiveControl = nullptr;
			this->mtLaunchCitramon->Location = System::Drawing::Point(6, 213);
			this->mtLaunchCitramon->Name = L"mtLaunchCitramon";
			this->mtLaunchCitramon->Size = System::Drawing::Size(145, 58);
			this->mtLaunchCitramon->TabIndex = 6;
			this->mtLaunchCitramon->Text = L"��������";
			this->mtLaunchCitramon->TileImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"mtLaunchCitramon.TileImage")));
			this->mtLaunchCitramon->TileImageAlign = System::Drawing::ContentAlignment::BottomLeft;
			this->mtLaunchCitramon->TileTextFontSize = MetroFramework::MetroTileTextSize::Tall;
			this->mtLaunchCitramon->TileTextFontWeight = MetroFramework::MetroTileTextWeight::Regular;
			this->mtLaunchCitramon->UseSelectable = true;
			this->mtLaunchCitramon->UseStyleColors = true;
			this->mtLaunchCitramon->UseTileImage = true;
			this->mtLaunchCitramon->Click += gcnew System::EventHandler(this, &Widget::mtLaunchCitramon_Click);
			// 
			// mtMonitor
			// 
			this->mtMonitor->ActiveControl = nullptr;
			this->mtMonitor->Location = System::Drawing::Point(157, 213);
			this->mtMonitor->Name = L"mtMonitor";
			this->mtMonitor->Size = System::Drawing::Size(90, 58);
			this->mtMonitor->TabIndex = 7;
			this->mtMonitor->Text = L"�������";
			this->mtMonitor->TileImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"mtMonitor.TileImage")));
			this->mtMonitor->TileTextFontWeight = MetroFramework::MetroTileTextWeight::Regular;
			this->mtMonitor->UseSelectable = true;
			this->mtMonitor->UseTileImage = true;
			this->mtMonitor->Click += gcnew System::EventHandler(this, &Widget::mtMonitor_Click);
			// 
			// mtLaunchConfig
			// 
			this->mtLaunchConfig->ActiveControl = nullptr;
			this->mtLaunchConfig->Location = System::Drawing::Point(6, 277);
			this->mtLaunchConfig->Name = L"mtLaunchConfig";
			this->mtLaunchConfig->Size = System::Drawing::Size(49, 50);
			this->mtLaunchConfig->TabIndex = 8;
			this->mtLaunchConfig->TileImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"mtLaunchConfig.TileImage")));
			this->metroToolTip->SetToolTip(this->mtLaunchConfig, L"������������");
			this->mtLaunchConfig->UseSelectable = true;
			this->mtLaunchConfig->UseTileImage = true;
			this->mtLaunchConfig->Click += gcnew System::EventHandler(this, &Widget::mtLaunchConfig_Click);
			// 
			// trayIcon
			// 
			this->trayIcon->ContextMenuStrip = this->trayContextMenu;
			this->trayIcon->Text = L"COR";
			this->trayIcon->Visible = true;
			this->trayIcon->DoubleClick += gcnew System::EventHandler(this, &Widget::trayIcon_DoubleClick);
			// 
			// trayContextMenu
			// 
			this->trayContextMenu->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(8) {
				this->tttStatus, this->tttSeparator1,
					this->tttCitramonEx, this->tttCitramon, this->tttDesigner, this->tttMonitor, this->toolStripSeparator2, this->tttQuit
			});
			this->trayContextMenu->Name = L"trayContextMenu";
			this->trayContextMenu->Size = System::Drawing::Size(214, 148);
			// 
			// tttStatus
			// 
			this->tttStatus->Font = (gcnew System::Drawing::Font(L"Segoe UI", 9, System::Drawing::FontStyle::Bold));
			this->tttStatus->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"tttStatus.Image")));
			this->tttStatus->Name = L"tttStatus";
			this->tttStatus->Size = System::Drawing::Size(213, 22);
			this->tttStatus->Text = L"���������";
			this->tttStatus->Click += gcnew System::EventHandler(this, &Widget::trayIcon_DoubleClick);
			// 
			// tttSeparator1
			// 
			this->tttSeparator1->Name = L"tttSeparator1";
			this->tttSeparator1->Size = System::Drawing::Size(210, 6);
			// 
			// tttCitramonEx
			// 
			this->tttCitramonEx->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"tttCitramonEx.Image")));
			this->tttCitramonEx->ImageScaling = System::Windows::Forms::ToolStripItemImageScaling::None;
			this->tttCitramonEx->Name = L"tttCitramonEx";
			this->tttCitramonEx->Size = System::Drawing::Size(213, 22);
			this->tttCitramonEx->Text = L"�������� (����������)";
			this->tttCitramonEx->Click += gcnew System::EventHandler(this, &Widget::mtLaunchCitramonEx_Click);
			// 
			// tttCitramon
			// 
			this->tttCitramon->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"tttCitramon.Image")));
			this->tttCitramon->ImageScaling = System::Windows::Forms::ToolStripItemImageScaling::None;
			this->tttCitramon->Name = L"tttCitramon";
			this->tttCitramon->Size = System::Drawing::Size(213, 22);
			this->tttCitramon->Text = L"��������";
			this->tttCitramon->Click += gcnew System::EventHandler(this, &Widget::mtLaunchCitramon_Click);
			// 
			// tttDesigner
			// 
			this->tttDesigner->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"tttDesigner.Image")));
			this->tttDesigner->ImageScaling = System::Windows::Forms::ToolStripItemImageScaling::None;
			this->tttDesigner->Name = L"tttDesigner";
			this->tttDesigner->Size = System::Drawing::Size(213, 22);
			this->tttDesigner->Text = L"������������";
			this->tttDesigner->Click += gcnew System::EventHandler(this, &Widget::mtLaunchConfig_Click);
			// 
			// tttMonitor
			// 
			this->tttMonitor->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"tttMonitor.Image")));
			this->tttMonitor->Name = L"tttMonitor";
			this->tttMonitor->Size = System::Drawing::Size(213, 22);
			this->tttMonitor->Text = L"�������";
			// 
			// toolStripSeparator2
			// 
			this->toolStripSeparator2->Name = L"toolStripSeparator2";
			this->toolStripSeparator2->Size = System::Drawing::Size(210, 6);
			// 
			// tttQuit
			// 
			this->tttQuit->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"tttQuit.Image")));
			this->tttQuit->Name = L"tttQuit";
			this->tttQuit->Size = System::Drawing::Size(213, 22);
			this->tttQuit->Text = L"�����";
			this->tttQuit->Click += gcnew System::EventHandler(this, &Widget::tttQuit_Click);
			// 
			// mtLaunchCitramonEx
			// 
			this->mtLaunchCitramonEx->ActiveControl = nullptr;
			this->mtLaunchCitramonEx->Location = System::Drawing::Point(6, 163);
			this->mtLaunchCitramonEx->Name = L"mtLaunchCitramonEx";
			this->mtLaunchCitramonEx->Size = System::Drawing::Size(241, 44);
			this->mtLaunchCitramonEx->TabIndex = 9;
			this->mtLaunchCitramonEx->Text = L"�������� (����������)";
			this->mtLaunchCitramonEx->TileImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"mtLaunchCitramonEx.TileImage")));
			this->mtLaunchCitramonEx->TileTextFontSize = MetroFramework::MetroTileTextSize::Tall;
			this->mtLaunchCitramonEx->TileTextFontWeight = MetroFramework::MetroTileTextWeight::Regular;
			this->mtLaunchCitramonEx->UseSelectable = true;
			this->mtLaunchCitramonEx->UseStyleColors = true;
			this->mtLaunchCitramonEx->UseTileImage = true;
			this->mtLaunchCitramonEx->Click += gcnew System::EventHandler(this, &Widget::mtLaunchCitramonEx_Click);
			// 
			// metroToolTip
			// 
			this->metroToolTip->Style = MetroFramework::MetroColorStyle::Teal;
			this->metroToolTip->StyleManager = nullptr;
			this->metroToolTip->Theme = MetroFramework::MetroThemeStyle::Light;
			// 
			// timerPoll
			// 
			this->timerPoll->Enabled = true;
			this->timerPoll->Interval = 125;
			this->timerPoll->Tick += gcnew System::EventHandler(this, &Widget::timerPoll_Tick);
			// 
			// metroLabel1
			// 
			this->metroLabel1->AutoSize = true;
			this->metroLabel1->FontWeight = MetroFramework::MetroLabelWeight::Bold;
			this->metroLabel1->Location = System::Drawing::Point(6, 134);
			this->metroLabel1->Name = L"metroLabel1";
			this->metroLabel1->Size = System::Drawing::Size(121, 19);
			this->metroLabel1->TabIndex = 11;
			this->metroLabel1->Text = L"�������������";
			// 
			// Widget
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BorderStyle = MetroFramework::Forms::MetroFormBorderStyle::FixedSingle;
			this->ClientSize = System::Drawing::Size(250, 370);
			this->ControlBox = false;
			this->Controls->Add(this->metroLabel1);
			this->Controls->Add(this->mtLaunchCitramonEx);
			this->Controls->Add(this->mtLaunchConfig);
			this->Controls->Add(this->mtMonitor);
			this->Controls->Add(this->mtLaunchCitramon);
			this->Controls->Add(this->msNetworkStatus);
			this->Controls->Add(this->mtExit);
			this->Controls->Add(this->mtOptions);
			this->DisplayHeader = false;
			this->MaximizeBox = false;
			this->MinimizeBox = false;
			this->Movable = false;
			this->Name = L"Widget";
			this->Padding = System::Windows::Forms::Padding(0, 30, 0, 0);
			this->Resizable = false;
			this->ShowIcon = false;
			this->ShowInTaskbar = false;
			this->StartPosition = System::Windows::Forms::FormStartPosition::Manual;
			this->Style = MetroFramework::MetroColorStyle::Teal;
			this->Text = L"COR";
			this->TopMost = true;
			this->Deactivate += gcnew System::EventHandler(this, &Widget::Widget_Deactivate);
			this->Load += gcnew System::EventHandler(this, &Widget::Widget_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->metroStyleManager1))->EndInit();
			this->trayContextMenu->ResumeLayout(false);
			this->ResumeLayout(false);
			this->PerformLayout();
		}

	private:

		bool modalWindow = false;

		void Widget_Load(System::Object^  sender, System::EventArgs^  e);
		void Widget_Deactivate(System::Object^  sender, System::EventArgs^  e);
		void trayIcon_DoubleClick(System::Object^  sender, System::EventArgs^  e);
		void tttQuit_Click(System::Object^  sender, System::EventArgs^  e);
		void mtExit_Click(System::Object^  sender, System::EventArgs^  e);
		void mtLaunchConfig_Click(System::Object^  sender, System::EventArgs^  e);
		void mtLaunchCitramon_Click(System::Object^  sender, System::EventArgs^  e);
		void mtLaunchCitramonEx_Click(System::Object^  sender, System::EventArgs^  e);
		void timerPoll_Tick(System::Object^  sender, System::EventArgs^  e);
		void mtMonitor_Click(System::Object^  sender, System::EventArgs^  e);

		void Poll();
	};
}
