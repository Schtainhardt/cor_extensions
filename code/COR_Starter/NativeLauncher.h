#pragma once
#include <windows.h>

ref class NativeLauncher
{
public:
	NativeLauncher();
	~NativeLauncher();
	void Launch(const char* exeFile, const char* workingDir, char* arguments, const char* payload, HANDLE eventHandle);

	bool RegisterConfig(HANDLE& hProcess);
	bool RegisterEnterprise(HANDLE& hProcess);
	bool RegisterEnterpriseExclusive(HANDLE& hProcess);
	bool RegisterMonitor(HANDLE& hProcess);
};

ref class HandleFreeOnRelease
{
private:
	HANDLE processHandle = 0;
	HANDLE eventHandle = 0;
public:
	HandleFreeOnRelease(HANDLE pHandle, HANDLE eHandle)
	{
		processHandle = pHandle;
		eventHandle = eHandle;
	};

	void FreeHandle()
	{
		WaitForSingleObject(processHandle, INFINITE);
		CloseHandle(eventHandle);
		CloseHandle(processHandle);
	};
};