#pragma once
#include "NativeLauncher.h"

namespace COR_Launcher {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;


	/// <summary>
	/// ������ ��� MainForm
	/// </summary>
	public ref class MainForm : public System::Windows::Forms::Form
	{

		public:
			NativeLauncher^ nativeLauncher = nullptr;

			MainForm(void)
			{
				InitializeComponent();
				nativeLauncher = gcnew NativeLauncher();
			}

		protected:
			~MainForm()
			{
				if (components)
				{
					delete components;
				}
				delete nativeLauncher;
			}

		private: 
		
			System::Windows::Forms::Button^  btnLogin;
			System::ComponentModel::Container ^components;

		void InitializeComponent(void)
		{
			this->btnLogin = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// btnLogin
			// 
			this->btnLogin->Location = System::Drawing::Point(429, 306);
			this->btnLogin->Name = L"btnLogin";
			this->btnLogin->Size = System::Drawing::Size(75, 23);
			this->btnLogin->TabIndex = 0;
			this->btnLogin->Text = L"�����";
			this->btnLogin->UseVisualStyleBackColor = true;
			this->btnLogin->Click += gcnew System::EventHandler(this, &MainForm::btnLogin_Click);
			// 
			// MainForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(516, 341);
			this->Controls->Add(this->btnLogin);
			this->Name = L"MainForm";
			this->Text = L"MainForm";
			this->Load += gcnew System::EventHandler(this, &MainForm::MainForm_Load);
			this->ResumeLayout(false);

		}

		private: System::Void btnLogin_Click(System::Object^  sender, System::EventArgs^  e) 
		{
			nativeLauncher->Launch();
		}

	private: System::Void MainForm_Load(System::Object^  sender, System::EventArgs^  e) 
	{
		array<String^>^ arguments = Environment::GetCommandLineArgs();

		for each(String^ key in arguments)
		{
			if (key == "/debug")
			{
				nativeLauncher->Launch();
				this->WindowState = Windows::Forms::FormWindowState::Minimized;
				break;
			}
		}
	}
	};
}
