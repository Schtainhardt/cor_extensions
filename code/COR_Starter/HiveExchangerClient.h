#pragma once

using namespace COR_Hive;

public enum HEC_State
{
	DISCONNECTED,
	WAITING_RESP,
	ESTABLISHED,
	DISCONNECTING
};

ref class HiveExchangerClient : public HiveSubscriber
{
private:
	HEC_State state = DISCONNECTED;
public:
	void SendStop();

	System::String^ PollState(HEC_State& outState);

	virtual bool Start() override;
	virtual bool ProcessMessage(HiveMessageHeader hdr, array<unsigned char, 1>^ data) override;

	HiveExchangerClient() : HiveSubscriber("tcp://127.0.0.1:10400", "tcp://127.0.0.1:10401") {};
};