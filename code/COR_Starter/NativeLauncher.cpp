#include "NativeLauncher.h"

using namespace System::Threading;

NativeLauncher::NativeLauncher()
{
}

NativeLauncher::~NativeLauncher()
{
}

void createShellcode(int ret, int str, unsigned char** shellcode, int* shellcodeSize)
{
	unsigned char* retChar = (unsigned char*)&ret;
	unsigned char* strChar = (unsigned char*)&str;

	int api = (int)GetProcAddress(LoadLibraryA("kernel32.dll"), "LoadLibraryA");

	unsigned char* apiChar = (unsigned char*)&api;
	unsigned char sc[] = {
		0x68, retChar[0], retChar[1], retChar[2], retChar[3],
		0x9C,
		0x60,
		0x68, strChar[0], strChar[1], strChar[2], strChar[3],
		0xB8, apiChar[0], apiChar[1], apiChar[2], apiChar[3],
		0xFF, 0xD0,
		0x61,
		0x9D,
		0xC3
	};

	*shellcodeSize = 22;
	*shellcode = (unsigned char*)malloc(22);
	memcpy(*shellcode, sc, 22);
}

void NativeLauncher::Launch(const char* exeFile, const char* workingDir, char* arguments, const char* payload, HANDLE eventHandle)
{
	STARTUPINFOA StartupInfo = { 0 };
	PROCESS_INFORMATION ProcessInformation;

	StartupInfo.cb = sizeof(StartupInfo);

	int result = CreateProcessA(exeFile, arguments,
		NULL,
		NULL,
		FALSE,
		CREATE_SUSPENDED | DETACHED_PROCESS,
		NULL,
		workingDir,
		&StartupInfo,
		&ProcessInformation
	);

	if (result == 0)
		return;

	HANDLE hProcess(ProcessInformation.hProcess);
	HANDLE hThread(ProcessInformation.hThread);

	char* remote_dllString = (char*)VirtualAllocEx(hProcess, NULL, strlen(payload) + 1, MEM_COMMIT, PAGE_READWRITE);

	CONTEXT context = {0};

	context.ContextFlags = CONTEXT_CONTROL;
	GetThreadContext(hThread, &context);

	unsigned char* shellcode;
	int shellcodeLen;
	createShellcode(context.Eip, (int)remote_dllString, &shellcode, &shellcodeLen);
	
	char* remote_shellcode = (char*)VirtualAllocEx(hProcess, NULL, shellcodeLen, MEM_COMMIT, PAGE_EXECUTE_READWRITE);

	WriteProcessMemory(hProcess, remote_dllString, payload, strlen(payload) + 1, NULL);
	WriteProcessMemory(hProcess, remote_shellcode, shellcode, shellcodeLen, NULL);

	context.Eip = (DWORD)remote_shellcode;
	context.ContextFlags = CONTEXT_CONTROL;
	SetThreadContext(hThread, &context);

	if (IsDebuggerPresent())
		MessageBoxA(0, "����� ������� Attach", "�������", 0);

	ResumeThread(hThread);

	HandleFreeOnRelease^ releaseManager = gcnew HandleFreeOnRelease(hProcess, eventHandle);
	ThreadStart^ actionDelegate = gcnew ThreadStart(releaseManager, &HandleFreeOnRelease::FreeHandle);
	Thread^ freeOnRelease = gcnew Thread(actionDelegate);
	freeOnRelease->Start();
}

bool NativeLauncher::RegisterConfig(HANDLE& hProcess)
{
	HANDLE  m_hStartEvent = CreateEventA(NULL, FALSE, FALSE, "Global\\COR_BL_Config");
	hProcess = 0;

	if (m_hStartEvent == NULL)
	{
		CloseHandle(m_hStartEvent);
		return true;
	}

	if (GetLastError() == ERROR_ALREADY_EXISTS) 
	{

		CloseHandle(m_hStartEvent);
		m_hStartEvent = NULL;
		return true;
	}

	hProcess = m_hStartEvent;

	return false;
}

bool NativeLauncher::RegisterEnterprise(HANDLE& hProcess)
{
	HANDLE m_hStartEvent = CreateEventA(NULL, FALSE, FALSE, "Local\\COR_BL_Enterprise");
	hProcess = 0;

	if (m_hStartEvent == NULL)
	{
		CloseHandle(m_hStartEvent);
		return true;
	}

	if (GetLastError() == ERROR_ALREADY_EXISTS) 
	{
		CloseHandle(m_hStartEvent);
		m_hStartEvent = NULL;
		return true;
	}

	hProcess = m_hStartEvent;

	return false;
}

bool NativeLauncher::RegisterEnterpriseExclusive(HANDLE& hProcess)
{
	HANDLE  m_hStartEvent = CreateEventA(NULL, FALSE, FALSE, "Global\\COR_BL_EnterpriseEx");
	hProcess = 0;

	if (m_hStartEvent == NULL)
	{
		CloseHandle(m_hStartEvent);
		return true;
	}

	if (GetLastError() == ERROR_ALREADY_EXISTS)
	{

		CloseHandle(m_hStartEvent);
		m_hStartEvent = NULL;
		return true;
	}

	hProcess = m_hStartEvent;

	return false;
}

bool NativeLauncher::RegisterMonitor(HANDLE& hProcess)
{
	HANDLE m_hStartEvent = CreateEventA(NULL, FALSE, FALSE, "Local\\COR_BL_Monitor");
	hProcess = 0;

	if (m_hStartEvent == NULL)
	{
		CloseHandle(m_hStartEvent);
		return true;
	}

	if (GetLastError() == ERROR_ALREADY_EXISTS)
	{
		CloseHandle(m_hStartEvent);
		m_hStartEvent = NULL;
		return true;
	}

	hProcess = m_hStartEvent;

	return false;
}