#pragma once

#define HOLD_STRING(name, defaultValue) static System::String^ name = gcnew System::String(defaultValue)

static ref class SettingsHolder
{
public:
	static System::Configuration::Configuration^ config = nullptr;

	static void Init();
	static void Save();

	HOLD_STRING(BL_BIN, "C:\\Program files (x86)\\1Cv77\\BIN\\");
	HOLD_STRING(BL_EXE, "1cv7s.exe");

	HOLD_STRING(BL_DB_PATH, "D:\\1C_Bases\\Citramon\\");

	HOLD_STRING(COR_DLL_PATH, "C:\\Program files (x86)\\1Cv77\\BIN\\");
	HOLD_STRING(COR_DLL, "COR_BL_77.dll");

	HOLD_STRING(COR_USERNAME, "Username");
	HOLD_STRING(COR_PASSWORD, "Password");
};

#undef HOLD_STRING