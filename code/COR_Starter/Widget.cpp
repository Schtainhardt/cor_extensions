#include "Widget.h"
#include "NativeLauncher.h"
#include "SettingsHolder.h"

#include <msclr/marshal.h>

using namespace System;
using namespace CORStarter;

//////////////////////////////////////////////////////////////////////////
void __clrcall WinMainClr(array<String^>^ argv)
{
	SettingsHolder::Init();

	System::Windows::Forms::Application::EnableVisualStyles();
	System::Windows::Forms::Application::Run(gcnew Widget());
	return;
}

//////////////////////////////////////////////////////////////////////////
void CORStarter::Widget::Widget_Load(System::Object^ sender, System::EventArgs^ e)
{
	HWND hDesktop = GetDesktopWindow();
	int screenHeight = GetSystemMetrics(SM_CYFULLSCREEN);
	int screenWidth = GetSystemMetrics(SM_CXFULLSCREEN);

	// our brand new cool metro ui window have problems with position, so...
	int heightFix = GetSystemMetrics(SM_CYCAPTION);

	int realX = screenWidth - this->Width;
	int realY = screenHeight - this->Height + heightFix;

	this->SetDesktopBounds(realX, realY, this->Width, this->Height);

	Poll();
}

//////////////////////////////////////////////////////////////////////////,
void CORStarter::Widget::mtExit_Click(System::Object^ sender, System::EventArgs^ e)
{
	modalWindow = true;
	auto result = MetroFramework::MetroMessageBox::Show(this, "�� ������������� ������ �������� COR?",
		"�����", MessageBoxButtons::YesNo, MessageBoxIcon::Question);
	modalWindow = false;

	if (result != Windows::Forms::DialogResult::Yes)
		return;

	Poll();

	Application::Exit();
}

//////////////////////////////////////////////////////////////////////////
void CORStarter::Widget::mtLaunchCitramon_Click(System::Object^ sender, System::EventArgs^ e)
{
	NativeLauncher^ launcher = gcnew NativeLauncher();
	HANDLE eHandle = 0;
	if (!launcher->RegisterEnterpriseExclusive(eHandle))
	{
		CloseHandle(eHandle);
	}
	else
	{
		modalWindow = true;
		MetroFramework::MetroMessageBox::Show(this, "�� ������ ���������� ������� �������� � ����������� ������! ������ � ����������� ������ ����������!", "COR Launcher", MessageBoxButtons::OK, MessageBoxIcon::Warning);
		modalWindow = false;
		return;
	}

	if (!launcher->RegisterEnterprise(eHandle))
	{
		msclr::interop::marshal_context ctx;
		const char* exePath = ctx.marshal_as<const char*>(SettingsHolder::BL_BIN + SettingsHolder::BL_EXE);
		const char* workingDir = ctx.marshal_as<const char*>(SettingsHolder::BL_BIN);
		char* args = const_cast<char*>(ctx.marshal_as<const char*>(SettingsHolder::BL_EXE + " enterprise /D\""+SettingsHolder::BL_DB_PATH+"\""));
		const char* payloadPath = ctx.marshal_as<const char*>(SettingsHolder::COR_DLL_PATH + SettingsHolder::COR_DLL);

		launcher->Launch(exePath, workingDir, args, payloadPath, eHandle);
	}
	else
	{
		modalWindow = true;
		MetroFramework::MetroMessageBox::Show(this, "�������� ��� ������� ��� ������� ������������!", "COR Launcher", MessageBoxButtons::OK, MessageBoxIcon::Warning);
		modalWindow = false;
	}
}

//////////////////////////////////////////////////////////////////////////
void CORStarter::Widget::mtLaunchCitramonEx_Click(System::Object^ sender, System::EventArgs^ e)
{
	NativeLauncher^ launcher = gcnew NativeLauncher();
	HANDLE eHandle = 0;
	if (!launcher->RegisterEnterpriseExclusive(eHandle))
	{
		msclr::interop::marshal_context ctx;
		const char* exePath = ctx.marshal_as<const char*>(SettingsHolder::BL_BIN + SettingsHolder::BL_EXE);
		const char* workingDir = ctx.marshal_as<const char*>(SettingsHolder::BL_BIN);
		char* args = const_cast<char*>(ctx.marshal_as<const char*>(SettingsHolder::BL_EXE + " enterprise /M /D\"" + SettingsHolder::BL_DB_PATH + "\""));
		const char* payloadPath = ctx.marshal_as<const char*>(SettingsHolder::COR_DLL_PATH + SettingsHolder::COR_DLL);

		launcher->Launch(exePath, workingDir, args, payloadPath, eHandle);
	}
	else
	{
		modalWindow = true;
		MetroFramework::MetroMessageBox::Show(this, "�������� (����������) ��� ������� �� ������ ����������!", "COR Launcher", MessageBoxButtons::OK, MessageBoxIcon::Warning);
		modalWindow = false;
	}
}

//////////////////////////////////////////////////////////////////////////
void CORStarter::Widget::timerPoll_Tick(System::Object^ sender, System::EventArgs^ e)
{
	Poll();
}

//////////////////////////////////////////////////////////////////////////
void CORStarter::Widget::mtMonitor_Click(System::Object ^ sender, System::EventArgs ^ e)
{
	NativeLauncher^ launcher = gcnew NativeLauncher();
	HANDLE eHandle = 0;
	if (!launcher->RegisterMonitor(eHandle))
	{
		msclr::interop::marshal_context ctx;
		const char* exePath = ctx.marshal_as<const char*>(SettingsHolder::BL_BIN + SettingsHolder::BL_EXE);
		const char* workingDir = ctx.marshal_as<const char*>(SettingsHolder::BL_BIN);
		char* args = const_cast<char*>(ctx.marshal_as<const char*>(SettingsHolder::BL_EXE + " monitor /D\"" + SettingsHolder::BL_DB_PATH + "\""));
		const char* payloadPath = ctx.marshal_as<const char*>(SettingsHolder::COR_DLL_PATH + SettingsHolder::COR_DLL);

		launcher->Launch(exePath, workingDir, args, payloadPath, eHandle);
	}
	else
	{
		modalWindow = true;
		MetroFramework::MetroMessageBox::Show(this, "������� ������������� ��� ������� ��� ������� ������������!", "COR Launcher", MessageBoxButtons::OK, MessageBoxIcon::Warning);
		modalWindow = false;
		return;
	}
}

//////////////////////////////////////////////////////////////////////////
void CORStarter::Widget::Poll()
{

}

//////////////////////////////////////////////////////////////////////////
void CORStarter::Widget::Widget_Deactivate(System::Object^ sender, System::EventArgs^ e)
{
	System::Threading::Thread::Sleep(125);

	if (modalWindow)
		return;

	this->Hide();
}

//////////////////////////////////////////////////////////////////////////
void CORStarter::Widget::trayIcon_DoubleClick(System::Object^ sender, System::EventArgs^ e)
{
	if (!this->Visible)
	{
		Poll();
		this->Show();
	}
	else
	{
		this->Hide();
	}
}

//////////////////////////////////////////////////////////////////////////
void CORStarter::Widget::tttQuit_Click(System::Object^ sender, System::EventArgs^ e)
{
	if (!this->Visible)
		this->Show();

	mtExit_Click(sender, e);
}

//////////////////////////////////////////////////////////////////////////
void CORStarter::Widget::mtLaunchConfig_Click(System::Object^ sender, System::EventArgs^ e)
{
	NativeLauncher^ launcher = gcnew NativeLauncher();
	HANDLE eHandle = 0;
	if (!launcher->RegisterConfig(eHandle))
	{
		msclr::interop::marshal_context ctx;
		const char* exePath = ctx.marshal_as<const char*>(SettingsHolder::BL_BIN + SettingsHolder::BL_EXE);
		const char* workingDir = ctx.marshal_as<const char*>(SettingsHolder::BL_BIN);
		char* args = const_cast<char*>(ctx.marshal_as<const char*>(SettingsHolder::BL_EXE + " config /D\"" + SettingsHolder::BL_DB_PATH + "\""));
		const char* payloadPath = ctx.marshal_as<const char*>(SettingsHolder::COR_DLL_PATH + SettingsHolder::COR_DLL);

		launcher->Launch(exePath, workingDir, args, payloadPath, eHandle);
	}
	else
	{
		modalWindow = true;
		MetroFramework::MetroMessageBox::Show(this, "������������ ��� ������� �� ������ ����������!", "COR Launcher", MessageBoxButtons::OK, MessageBoxIcon::Warning);
		modalWindow = false;
	}
}
