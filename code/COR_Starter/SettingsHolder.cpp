#include "SettingsHolder.h"

#define READ_STRING(name) if (!String::IsNullOrEmpty(checker = ConfigurationSettings::AppSettings[#name])) name = checker

using namespace System;
using namespace System::Configuration;
using namespace System::Windows::Forms;

void SettingsHolder::Init()
{
	config = ConfigurationManager::OpenExeConfiguration(ConfigurationUserLevel::None);

	System::String^ checker;

	READ_STRING(BL_BIN);
	READ_STRING(BL_EXE);

	READ_STRING(BL_DB_PATH);

	READ_STRING(COR_DLL_PATH);
	READ_STRING(COR_DLL);


	READ_STRING(COR_USERNAME);
}

//////////////////////////////////////////////////////////////////////////
void SettingsHolder::Save()
{
	config->Save(ConfigurationSaveMode::Modified);
	ConfigurationManager::RefreshSection("appSettings");
}

#undef READ_STRING