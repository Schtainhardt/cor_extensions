#include "HiveExchangerClient.h"

#include <windows.h>

using namespace System::Collections::Generic;
using namespace COR_Hive;
using namespace ZeroMQ;

bool HiveExchangerClient::ProcessMessage(HiveMessageHeader hdr, array<unsigned char, 1>^ data)
{

	state = ESTABLISHED;

	switch (hdr.type)
	{
	case HiveMessageType::PUBLIC_LIST:
	{
		HiveMessageHeader ownHdr;
		ownHdr.targetToken = hdr.senderToken;
		ownHdr.type = HiveMessageType::PNOTIFY_ALIVE;
		ownHdr.dataLength = sizeof(HiveClient);
		ownHdr.totalSize = ownHdr.dataLength + sizeof(HiveMessageHeader);

		HiveClient ownClient;
		ownClient.processPid = GetCurrentProcessId();
		ownClient.role = HiveRole::HR_LAUNCHER;

		List<unsigned char>^ rawMessage = gcnew List<unsigned char>();
		rawMessage->AddRange(TypeToByte(ownHdr));
		rawMessage->AddRange(TypeToByte(ownClient));

		serverSocketSender->Send(gcnew ZFrame(rawMessage->ToArray()));
		break;
	}
	case HiveMessageType::INTERNAL_SHUTDOWN:
	{
		DWORD pid = GetCurrentProcessId();
		if (hdr.targetToken == pid)
		{
			state = DISCONNECTED;
			return false;
		}
		break;
	}
	default:
		break;
	}

	return true;
}

void HiveExchangerClient::SendStop()
{
	HiveMessageHeader hdr;
	hdr.dataLength = 0;
	hdr.senderToken = GetCurrentProcessId();
	hdr.targetToken = GetCurrentProcessId();
	hdr.type = HiveMessageType::INTERNAL_SHUTDOWN;
	hdr.totalSize = sizeof(HiveMessageHeader);

	serverSocketSender->Send(gcnew ZFrame(TypeToByte(hdr)));
	state = DISCONNECTING;
}

System::String^ HiveExchangerClient::PollState(HEC_State& outState)
{
	switch (state)
	{
	case DISCONNECTED:
		return "��������";
	case WAITING_RESP:
		return "�������� �����������...";
	case ESTABLISHED:
		return "���������";
	case DISCONNECTING:
		return "����������...";
	default:
		return "";
	}
}

bool HiveExchangerClient::Start()
{
	state = WAITING_RESP;
	return __super::Start();
}
